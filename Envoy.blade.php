@servers(['production' => 'deploybot@85.90.246.239'])

@task('deploy', ['on' => 'production'])
cd /home/boost
git reset --hard HEAD
git pull origin master
composer dump-autoload
php artisan migrate --force
@endtask

@task('resetDB', ['on' => 'production'])
mysql -u boost --password=boost
drop database boostdb;
create database boostdb;
exit
cd /home/boost
php artisan migrate --seed --force
@endtask

@task('seed', ['on' => 'production'])
cd /home/boost
php artisan migrate --seed --force
@endtask

@task('permissions', ['on' => 'production'])
cd /home/boost
chmod 770 public -R
chmod 770 resources -R
chmod 770 storage -R
@endtask

@task('betadeploy', ['on' => 'production'])
cd /home/betaboost
git reset --hard HEAD
git pull origin master
composer dump-autoload
php artisan migrate --force
@endtask
