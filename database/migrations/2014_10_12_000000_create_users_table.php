<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('email')->unique();
            $table->string('opleiding', 140);
            $table->string('portfolio', 60);
            $table->string('facebook', 60);
            $table->string('twitter', 60);
            $table->string('instagram', 60);
            $table->string('locatie', 60);
            $table->string('functie', 60);
            $table->string('refer');
            $table->text('bio', 600);
            $table->string('password', 60);
            $table->string('image');
            $table->boolean('emails')->default(true);
            $table->boolean('mailsTime')->default(true);
            $table->integer('experience');
            $table->integer('views')->default(1);
            $table->integer('admin')->default(0)->unsigned();
            $table->boolean('confirmed')->default(false);
            $table->string('confirmation_code')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
