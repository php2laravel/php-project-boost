<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class VotesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('votes')->delete();
        
        DB::table('votes')->insert([
            'userId' => '1',
            'projectId' => '3',
            'created_at' => '2015-11-16 10:00:00',
        ]);
        DB::table('votes')->insert(
        [
            'userId' => '1',
            'projectId' => '2',
            'created_at' => '2015-11-16 10:10:00',
        ]);
        DB::table('votes')->insert(
        [
            'userId' => '2',
            'projectId' => '5',
            'created_at' => '2015-11-16 11:00:00',
        ]);
        DB::table('votes')->insert([
            'userId' => '2',
            'projectId' => '3',
            'created_at' => '2015-11-16 10:00:00',
        ]);
        DB::table('votes')->insert(
        [
            'userId' => '3',
            'projectId' => '3',
            'created_at' => '2015-11-16 10:10:00',
        ]);
        DB::table('votes')->insert(
        [
            'userId' => '4',
            'projectId' => '1',
            'created_at' => '2015-11-16 11:00:00',
        ]);
    }
}
