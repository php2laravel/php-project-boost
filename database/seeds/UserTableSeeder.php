<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
            'id' => 1,
            'name' => 'Niels Meulders',
            'image' => '1.jpg',
            'email' => 'niels.meulders@gmail.com',
            'password' => bcrypt('nielsmeulders'),
            'opleiding' => 'Interactive Multimedia Design',
            'portfolio' => 'http://www.nielsmeulders.be',
            'facebook' => 'https://www.facebook.com/niels.meulders',
            'twitter' => 'https://www.facebook.com/niels.meulders',
            'instagram' => 'https://www.facebook.com/niels.meulders',
            'locatie' => 'Keerbergen',
            'functie' => 'Developer',
            'bio' => 'Ik ben Niels Meulders, een 23 jarige gedreven developer',
            'experience' => 50,
            'admin' => 1,
            'confirmed' => 1,
        ]);
        DB::table('users')->insert(
            [
                'id' => 2,
                'name' => 'Joren Polfliet',
                'image' => '2.jpg',
                'email' => 'polfliet.joren@gmail.com',
                'password' => bcrypt('jorenpolfliet'),
                'opleiding' => 'Interactive Multimedia Design',
                'portfolio' => '',
                'facebook' => '',
                'twitter' => '',
                'instagram' => '',
                'locatie' => '',
                'functie' => 'Scrum master',
                'bio' => '',
                'experience' => 50,
                'admin' => 1,
                'confirmed' => 1,
            ]);
        DB::table('users')->insert(
            [
                'id' => 3,
                'name' => 'Thais Lenglez',
                'image' => '3.jpg',
                'email' => 'thaislenglez@gmail.com',
                'password' => bcrypt('thaislenglez'),
                'opleiding' => 'Interactive Multimedia Design',
                'portfolio' => '',
                'facebook' => '',
                'twitter' => '',
                'instagram' => '',
                'locatie' => '',
                'functie' => 'Designer',
                'bio' => '',
                'experience' => 50,
                'admin' => 1,
                'confirmed' => 1,
            ]);
        DB::table('users')->insert(
            [
                'id' => 4,
                'name' => 'Veerle Devos',
                'image' => '4.jpg',
                'email' => 'veerle_devos@hotmail.be',
                'password' => bcrypt('veerledevos'),
                'opleiding' => 'Interactive Multimedia Design',
                'portfolio' => '',
                'facebook' => '',
                'twitter' => '',
                'instagram' => '',
                'locatie' => '',
                'functie' => 'Designer',
                'bio' => '',
                'experience' => 50,
                'admin' => 1,
                'confirmed' => 1,
            ]);
        DB::table('users')->insert(
            [
                'id' => 5,
                'name' => 'Jens Ivens',
                'image' => '5.jpg',
                'email' => 'jens.ivens@hotmail.com',
                'password' => bcrypt('jensivens'),
                'opleiding' => 'Interactive Multimedia Design',
                'portfolio' => '',
                'facebook' => '',
                'twitter' => '',
                'instagram' => '',
                'locatie' => '',
                'functie' => 'Developer',
                'bio' => '',
                'experience' => 0,
                'admin' => 1,
                'confirmed' => 1,
            ]);
    }
}
