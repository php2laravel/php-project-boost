<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;


class ProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projects')->delete();
        
        DB::table('projects')->insert([
            'id' => '1',
            'title' => 'Movember',
            'desc' => 'A beautiful calender for the month November',
            'image' => '1.jpg',
            'user_id' => '1',
            'competition' => false,
            'countlikes' => "1",
            'created_at' => Carbon::now()
        ]);
        DB::table('projects')->insert(
        [
            'id' => '2',
            'title' => 'Wanderaid app',
            'desc' => 'Volunteering app',
            'image' => '2.png',
            'user_id' => '3',
            'competition' => false,
            'countlikes' => "1",
            'created_at' => Carbon::now()
        ]);
        DB::table('projects')->insert(
        [
            'id' => '3',
            'title' => 'Find me fit!',
            'desc' => 'A beautiful, modern app to check your health status with features such as step-counter and daily water consumption.',
            'image' => '3.png',
            'user_id' => '2',
            'competition' => false,
            'countlikes' => "3",
            'created_at' => Carbon::now()
        ]);
        DB::table('projects')->insert(
        [
            'id' => '4',
            'title' => 'Weerapp',
            'desc' => 'A beautiful, simple app design to check the time in a matter of seconds.',
            'image' => '4.jpg',
            'user_id' => '4',
            'competition' => false,
            'created_at' => Carbon::now()
        ]);

        /*Contests*/

        DB::table('projects')->insert(
        [
            'id' => '5',
            'title' => 'Kinderstad',
            'desc' => 'Typography based on flowers!',
            'image' => '5.png',
            'user_id' => '4',
            'competition' => true,
            'competition_id' => 1,
            'countlikes' => "3",
            'created_at' => '2015-10-15 09:00:00'
        ]);  
        DB::table('projects')->insert([
            'id' => '6',
            'title' => 'Mockup',
            'desc' => 'rebranding',
            'image' => '6.png',
            'user_id' => '1',
            'competition_id' => 2,
            'competition' => true,
            'countlikes' => "2",
            'created_at' => '2015-11-21 09:00:00'
        ]);
        DB::table('projects')->insert(
        [
            'id' => '7',
            'title' => 'T-rain',
            'desc' => 'Free typo if you like!',
            'image' => '7.png',
            'user_id' => '2',
            'competition_id' => 1,
            'competition' => true,
            'countlikes' => "8",
            'created_at' => '2015-10-25 09:00:00'
        ]); 
        DB::table('projects')->insert(
        [
            'id' => '8',
            'title' => 'Iphone cover',
            'desc' => 'Contest application for contest November',
            'image' => '8.png',
            'user_id' => '3',
            'competition' => true,
            'competition_id' => 2,
            'countlikes' => "10",
            'created_at' => '2015-12-11 09:00:00'
        ]);                 

    }
}
