<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->delete();
        
        DB::table('tags')->insert([
            'id' => '1',
            'name' => 'webdesign',
            'created_at' => '2015-11-16 10:00:00',
        ]);
        DB::table('tags')->insert(
        [
            'id' => '2',
            'name' => 'illustration',
            'created_at' => '2015-11-16 10:10:00',
        ]);
        DB::table('tags')->insert(
        [
            'id' => '3',
            'name' => 'UI',
            'created_at' => '2015-11-16 11:00:00',
        ]);
        DB::table('tags')->insert([
            'id' => '4',
            'name' => 'mobile',
            'created_at' => '2015-11-16 10:00:00',
        ]);
        DB::table('tags')->insert(
        [
            'id' => '5',
            'name' => 'UX',
            'created_at' => '2015-11-16 10:10:00',
        ]);
        DB::table('tags')->insert(
        [
            'id' => '6',
            'name' => 'typography',
            'created_at' => '2015-11-16 11:00:00',
        ]);
        DB::table('tags')->insert([
            'id' => '7',
            'name' => 'flat design',
            'created_at' => '2015-11-16 10:00:00',
        ]);
        DB::table('tags')->insert(
        [
            'id' => '8',
            'name' => 'trends',
            'created_at' => '2015-11-16 10:10:00',
        ]);
        DB::table('tags')->insert(
        [
            'id' => '9',
            'name' => '3D',
            'created_at' => '2015-11-16 11:00:00',
        ]);
        DB::table('tags')->insert([
            'id' => '10',
            'name' => 'Sketch App',
            'created_at' => '2015-11-16 10:00:00',
        ]);
        DB::table('tags')->insert(
        [
            'id' => '11',
            'name' => 'Illustrator',
            'created_at' => '2015-11-16 10:10:00',
        ]);
        DB::table('tags')->insert(
        [
            'id' => '12',
            'name' => 'Photoshop',
            'created_at' => '2015-11-16 11:00:00',
        ]);
        DB::table('tags')->insert([
            'id' => '13',
            'name' => 'sketch',
            'created_at' => '2015-11-16 10:00:00',
        ]);
        DB::table('tags')->insert(
        [
            'id' => '14',
            'name' => 'colours',
            'created_at' => '2015-11-16 10:10:00',
        ]);
        DB::table('tags')->insert(
        [
            'id' => '15',
            'name' => 'logo',
            'created_at' => '2015-11-16 11:00:00',
        ]);
    }
}
