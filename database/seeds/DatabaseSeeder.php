<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /*
        uncomment this for deploy seeders

        $this->call('BadgesTableSeeder');
        $this->command->info('Badges table seeded!');
        $this->call('TagsTableSeeder');
        $this->command->info('Tags table seeded!');

         */

        /*
        uncomment this for local seeders

        $this->call('UserTableSeeder');
        $this->command->info('User table seeded!');
        $this->call('ProjectTableSeeder');
        $this->command->info('Project table seeded!');
        $this->call('VotesTableSeeder');
        $this->command->info('Votes table seeded!');
        $this->call('BadgesTableSeeder');
        $this->command->info('Badges table seeded!');
        $this->call('TagsTableSeeder');
        $this->command->info('Tags table seeded!');
        $this->call('ProjectTagTableSeeder');
        $this->command->info('Project_tag pivot table seeded!');
         */

        $this->call('UserTableSeeder');
        $this->command->info('User table seeded!');
        $this->call('ProjectTableSeeder');
        $this->command->info('Project table seeded!');
        $this->call('VotesTableSeeder');
        $this->command->info('Votes table seeded!');
        $this->call('BadgesTableSeeder');
        $this->command->info('Badges table seeded!');
        $this->call('TagsTableSeeder');
        $this->command->info('Tags table seeded!');
        $this->call('ProjectTagTableSeeder');
        $this->command->info('Project_tag pivot table seeded!');
        $this->call('ContestsTableSeeder');
        $this->command->info('Contests table seeded!'); 
        $this->call('AdsTableSeeder');
        $this->command->info('Ads table seeded!');
        $this->call('CommentTableSeeder');
        $this->command->info('Comments table seeded!');
    }
}