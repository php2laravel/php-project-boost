<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class ProjectTagTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('project_tag')->delete();
        
        DB::table('project_tag')->insert([
            'project_id' => '1',
            'tag_id' => '1',
        ]);
        DB::table('project_tag')->insert(
        [
            'project_id' => '1',
            'tag_id' => '2',
        ]);
        DB::table('project_tag')->insert(
        [
            'project_id' => '1',
            'tag_id' => '3',
        ]);
        DB::table('project_tag')->insert([
            'project_id' => '2',
            'tag_id' => '1',
        ]);
        DB::table('project_tag')->insert(
        [
            'project_id' => '3',
            'tag_id' => '2',
        ]);
        DB::table('project_tag')->insert(
        [
            'project_id' => '4',
            'tag_id' => '3',
        ]);
    }
}
