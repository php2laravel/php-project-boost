<?php

use Illuminate\Database\Seeder;

class ContestsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contests')->delete();
        
        DB::table('contests')->insert([
            'id' => '1',
            'title' => 'October',
            'desc' => 'Design a beautiful calender for the month October',
            'startdate' => '2015-10-01 09:00:00',
            'enddate' => '2015-10-31 23:59:00'
        ]);

        DB::table('contests')->insert([
            'id' => '2',
            'title' => 'November',
            'desc' => 'Design an IMD logo for the month November',
            'startdate' => '2015-11-01 09:00:00',
            'enddate' => '2015-11-30 23:59:00'
        ]);  

        DB::table('contests')->insert([
            'id' => '3',
            'title' => 'December: #IMDBeer',
            'desc' => 'IMD beer, branding!',
            'startdate' => '2015-12-01 09:00:00',
            'enddate' => '2015-12-31 23:59:00'
        ]);                
    }
}
