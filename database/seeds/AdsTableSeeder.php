<?php

use Illuminate\Database\Seeder;

class AdsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ads')->delete();
        
        DB::table('ads')->insert([
            'id' => '0',
            'user_id' => '4',
            'companyname' => 'Designosource',
            'website' => 'www.designosource.be',
            'image' => '2.png',
            'enddate' => '12-29-2015'
        ]); 

        DB::table('ads')->insert([
            'id' => '2',
            'user_id' => '2',
            'companyname' => 'Designosource',
            'website' => 'www.designosource.be',
            'image' => '1.jpg',
            'enddate' => '12-30-2015'
        ]); 

        DB::table('ads')->insert([
            'id' => '3',
            'user_id' => '5',
            'companyname' => 'Designosource',
            'website' => 'www.designosource.be',
            'image' => '3.png',
            'enddate' => '12-31-2015'
        ]);                          
    }
}
