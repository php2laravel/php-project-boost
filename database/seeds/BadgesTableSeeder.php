<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;


class BadgesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('badges')->delete();
        DB::table('badges_users')->delete();

        DB::table('badges')->insert([
            'id' => '1',
            'name' => 'Founding Member',
            'image' => '/images/badges/founding-member.png',
        ]);
        DB::table('badges')->insert([
            'id' => '2',
            'name' => 'First Upload',
            'image' => '/images/badges/first-work.png',
        ]);
        DB::table('badges')->insert([
            'id' => '3',
            'name' => 'Referance Badge',
            'image' => '/images/badges/referance-badge.png',
        ]);

        //delete from production website
        /*DB::table('badges_users')->insert([
            'id' => '1',
            'badge_id' => '1',
            'user_id' => '1',
        ]);
        DB::table('badges_users')->insert([
            'id' => '2',
            'badge_id' => '2',
            'user_id' => '1',
        ]);
        DB::table('badges_users')->insert([
            'id' => '3',
            'badge_id' => '1',
            'user_id' => '2',
        ]);
        DB::table('badges_users')->insert([
            'id' => '4',
            'badge_id' => '2',
            'user_id' => '2',
        ]);
        DB::table('badges_users')->insert([
            'id' => '5',
            'badge_id' => '1',
            'user_id' => '3',
        ]);
        DB::table('badges_users')->insert([
            'id' => '6',
            'badge_id' => '2',
            'user_id' => '3',
        ]);
        DB::table('badges_users')->insert([
            'id' => '7',
            'badge_id' => '1',
            'user_id' => '4',
        ]);
        DB::table('badges_users')->insert([
            'id' => '8',
            'badge_id' => '2',
            'user_id' => '4',
        ]);
        DB::table('badges_users')->insert([
            'id' => '9',
            'badge_id' => '1',
            'user_id' => '5',
        ]);*/
    }
}
