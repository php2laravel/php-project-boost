<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CommentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('comments')->delete();

        DB::table('comments')->insert([
            'id' => '1',
            'project_id' => '1',
            'user_id' => '2',
            'body' => 'Mooi werkje!', 
            'created_at' => Carbon::now()
        ]);

        DB::table('comments')->insert([
            'id' => '2',
            'project_id' => '1',
            'user_id' => '3',
            'body' => 'Fantastische afwerking! Doe zo verder.',
            'created_at' => Carbon::now()
        ]);

        DB::table('comments')->insert([
            'id' => '3',
            'project_id' => '2',
            'user_id' => '1',
            'body' => 'Knap!',
            'created_at' => Carbon::now()
        ]);

        DB::table('comments')->insert([
            'id' => '4',
            'project_id' => '3',
            'user_id' => '4',
            'body' => 'Cool gevonden!',
            'created_at' => Carbon::now()
        ]);

        DB::table('comments')->insert([
            'id' => '5',
            'project_id' => '4',
            'user_id' => '5',
            'body' => 'Echt mooi.',
            'created_at' => Carbon::now()
        ]);
    }
}
