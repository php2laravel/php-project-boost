@extends('app')
<?php 
	if (Auth::check()) {
	    $user = Auth::user();
	}
?>

	@section('content')
	<a name="picture"></a>
	<div class="profile-info_edit panel panel-default">
		@if ($user->image != "")
			<img src="{{URL::to('/')}}/uploads/users/{{ $user->image }}" id="preview-old" class="profile-picture">
		@else
			<img src="http://jbryner.me/_images/placeholder_small.gif" alt="placeholder user picture" id="preview-old" class="profile-picture">
		@endif
        
        <div id="preview" class="profile-picture"></div>
        <br/>

		<div class="profile-info_form ">
			{!! Form::model($user, ['method' => 'POST', 'url' => ['my-profile/edit', $user->id], 'files' => true, 'class'=>'edit-profile_form', 'runat'=>'server' ]) !!}
				<br>
		            {!! Form::label('image', 'Change your profile picture') !!}
		            {!! form::file('image', null, ['class' => 'edit-profile_form-input'])!!}
	            <a name="basic"></a>
	            <br>
			  	<br>
					{!! Form::label('name', 'naam:', ['class' => 'edit-profile_form-label'])!!}
					{!! Form::text('name', null, ['class' => 'form-control edit-profile_form-input'])!!}
				<br>
				<br>
					{!! Form::label('opleiding', 'Opleiding:', ['class' => 'edit-profile_form-label'])!!}
					{!! Form::text('opleiding', null, ['class' => 'form-control edit-profile_form-input', 'placeholder' => '2IMD - Development'])!!}
				<br>
				<br>
					{!! Form::label('functie', 'Functie:', ['class' => 'edit-profile_form-label'])!!}
					{!! Form::text('functie', null, ['class' => 'form-control edit-profile_form-input', 'placeholder' => 'UX Designer'])!!}
				<br>
				<br>
					{!! Form::label('portfolio', 'Portfolio url: (without http://)', ['class' => 'edit-profile_form-label'])!!}
					{!! Form::text('portfolio', null, ['class' => 'form-control edit-profile_form-input', 'placeholder' => 'www.myportfolio.com'])!!}
				<br>
				<br>
					{!! Form::label('locatie', 'Locatie:', ['class' => 'edit-profile_form-label'])!!}
					{!! Form::text('locatie', null, ['class' => 'form-control edit-profile_form-input', 'placeholder' => 'Mechelen'])!!}
				<br>
				<a name="socialmedia"></a>
				<hr>
				<h4>Social media</h4>
				<br>
					{!! Form::label('facebook', 'Facebook url: (without http://)', ['class' => 'edit-profile_form-label'])!!}
					{!! Form::text('facebook', null, ['class' => 'form-control edit-profile_form-input', 'placeholder' => 'www.facebook.com/myportfolio'])!!}
				<br>
				<br>
					{!! Form::label('twitter', 'Twitter url: (without http://)', ['class' => 'edit-profile_form-label'])!!}
					{!! Form::text('twitter', null, ['class' => 'form-control edit-profile_form-input', 'placeholder' => 'www.twitter.com/myportfolio'])!!}
				<br>
				<!--<br>
					{!! Form::label('instagram', 'Instagram url: (without http://)', ['class' => 'edit-profile_form-label'])!!}
					{!! Form::text('instagram', null, ['class' => 'form-control edit-profile_form-input', 'placeholder' => 'www.instagram.com/myportfolio'])!!}
				<br>
				-->
				<br>
					{!! Form::label('emails', 'Would you like to receive emails?')!!}
					{!! Form::select('emails', array('1' =>'Yes', '0' => 'No')) !!}
				<br>

					{!! Form::label('mailsTime', 'When would you like to receive our overview mail?')!!}
					{!! Form::select('mailsTime', array('1' =>'Weekly', '0' => 'Monthly')) !!}
				<br>
				<a name="more"></a>
				<hr>
				<h4>More information</h4>
				<br>
					{!! Form::label('bio', 'Bio:')!!}
					{!! Form::textarea('bio', null, ['class' => 'form-control', 'placeholder' => 'UX Designer from Mechelen. Fueled by iced coffee, photography and good conversation.'])!!}
				<br>

	           	<div "profile-info-toggle">
				<h4>Account settings</h4>
					<p> Email address: </p>
					<p>{{ $user->email }}</p>

					<a href="#" class="text-muted credit">reset password</a>
						
				</div>
	            
				<br class="clear" />
				{!! Form::submit('Save changes', ["class" => 'btn form btn-center btn-default profile-info_edit-btn']) !!}
				<a href="{{URL::previous()}}" class="center subtle-link">Or go back without saving</a>
			{!! Form::close() !!}
		</div>
		
		<br class="clear" />
	</div>
	@stop


@section('scripts')
    <script>
	    jQuery(document).ready(function() {
            $('#preview').hide();
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    console.log("changies!");

                    reader.onload = function (e) {
                        $('#preview-old').slideUp();
                        $('#preview').slideDown();
                        $('#preview').css('background', 'transparent url('+e.target.result +') center center no-repeat');
                         $('#preview').css('background-size', 'cover');     
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#image").change(function(){
                readURL(this);
            });
        });
    </script>
@stop

