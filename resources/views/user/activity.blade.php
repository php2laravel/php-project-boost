@extends('app')

@section('content')


<h2>Your recent activity</h2>
		<button class="btn btn-default likes">Likes</button>
		<button class="btn btn-default comments">Comments</button>

<div class="likes-overview">
    @foreach ($likes as $like)


		<div class="row">
        	<div class="col-md-4">{{ $like->created_at }}</div>
        	<div class="col-md-8">Your friend {{ $like->name }} likes your project <a href="{{ action('ProjectsController@show', [$like->projectId]) }}">{{ $like->title }}</a>!</div>
		</div>
    


    @endforeach

</div>
<div class="comments-overview">
    @foreach ($comments as $comment)


		<div class="row">
        	<div class="col-md-4">{{ $comment->created_at }}</div>
        	<div class="col-md-8">Your friend {{ $comment->name }} commented on your project <a href="{{ action('ProjectsController@show', [$like->projectId]) }}">{{ $like->title }}</a>: {{ $comment->body}}!</div>
		</div>
    


    @endforeach

</div>
@stop