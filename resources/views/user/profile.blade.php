@extends('app')
<?php
	if (Auth::check()) {
	    $user = Auth::user();
	}
?>
@section('message')
        @if (Session::has('flash_notification.message'))
            <p class="alert alert-admin alert-{{ Session::get('flash_notification.level') }} alert-top">
             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('flash_notification.message') }}
            </p>
        @endif 
@stop
@section('left-sidebar')
	<aside class="profile-top">
		<div class="profile-top-left">
			<a href="/my-profile/edit#picture">
				@if ($user->image != "")
					<img src="{{URL::to('/')}}/uploads/users/{{ $user->image }}" class="profile-picture">
				@else
					<img src="http://jbryner.me/_images/placeholder_small.gif" alt="placeholder user picture" class="profile-picture">
				@endif
			</a>
			<ul class="profile-info_statistics">
				<li>
	                <img src="<?php echo asset('images/icons/vote.png')?>" alt="heart icon" class="icon-normal">
	                <br>
	                <span> {{ $totallikes }} </span>
				</li>
				<li>
					<img src="<?php echo asset('images/icons/iris.png')?>" alt="heart icon" class="icon-normal">
					<br>
					<span> {{ $user->views }} </span>
				</li>
			</ul>
			<a href="/my-profile/edit" class="btn-small profile-top-btn">edit profile</a>

		</div>
		<div class="profile-top-right">
			<h3>
				{{ $user->name }} <span class="profile-info-exp"> {{ $user->xp()[0] }} | {{ $user->xp()[1] }} </span>		
			</h3>
			<div class="profile-text">
			<div>
				@if($user->bio != "")
					<p class="profile-info_bio">
						{{ $user->bio }}
					</p>
				@else
					<a class="profile-info_bio" href="/my-profile/edit#more">
							add a bio
					</a>
				@endif
			</div>
			<div>
				<ul class="profile-info-basic">
					<li>
						@if($user->locatie != "")
							<p class="profile-info-basic-item">{{$user->locatie}}</p>
						@else
							<a href="/my-profile/edit#basic">
									add your location
							</a>
						@endif
					</li>
					<li>
						@if($user->opleiding != "")
							<p class="profile-info-basic-item">{{ $user->opleiding}}</p>
						@else
							<a class="profile-info-basic-item" href="/my-profile/edit#basic">
									add your education
							</a>
						@endif	
					</li>
					<li>
						@if($user->functie != "")
							<p class="profile-info-basic-item">{{ $user->functie}}</p>
						@else
							<a class="profile-info-basic-item" href="/my-profile/edit#basic">
									add your function
							</a>
						@endif	
					</li>
				</ul>
			</div>


			<div class="profile-info-extra">
				<div "profile-info-toggle">
					<a href="#" class="account-info-showmore">Account settings</a>
						<ul class="profile-info-more">
							<li>{{ $user->email }}</li>
							<li>
								<a href="#" class="text-muted credit">reset password</a>
							</li>
						</ul>
				</div>

				<div class="profile-info_list-links">
					<p class="profile-info_list-links-item">
						@if($user->portfolio == "")
							<a href="/my-profile/edit#basic">
										add your portfolio website
							</a>
						@else
							<a href="http://{{ $user->portfolio }}" target="_blank"><i class="fa glyphicon glyphicon-link"></i></a>
						@endif
					</p>	
					<p class="profile-info_list-links-item">
						@if($user->twitter != "")
							<a href="http://{{ $user->twitter }}" target="_blank"><i class="fa fa-twitter"></i></a>
						@else
							<a href="/my-profile/edit#socialmedia">
								add Twitter account
							</a>
							
						@endif
					</p>
					<p class="profile-info_list-links-item">
						@if($user->facebook != "")
							<a href="http://{{ $user->facebook }}" target="_blank"><i class="fa fa-facebook-official"></i></a>
						@else
							<a href="/my-profile/edit#socialmedia">
								add Facebook account
							</a>
						@endif
					</p>
				</div>
			</div>
		</div>		
	</aside>
@stop

@section('second-nav')
	<nav class="second-nav">
		<ul class="second-navbar">
			<li class="second-navbar-item">
				<a href="#" title="" id="boosts" class="second-navbar-item-link">
					Boosts ({{count($user->projects)}})
				</a>
			</li>
			<li class="second-navbar-item">
				<a href="#" title="" id="likes" class="second-navbar-item-link">
					Favourites ({{count($likes)}})
				</a>
			</li>
			<li class="second-navbar-item">
				<a href="#" title="" id="followers" class="second-navbar-item-link">
					Followers ({{count($followers)}})
				</a>
			</li>
			<li class="second-navbar-item">
				<a href="#" title="" id="follows" class="second-navbar-item-link">
					Following ({{count($follows)}})
				</a>
			</li>
			<li class="second-navbar-item">
				<a href="#" title="" id="activity" class="second-navbar-item-link">
					Activity ({{count($activityLikes) + count($activityComments)}})
				</a>
			</li>
			<li class="second-navbar-item">
				<a href="#" title="" id="badges" class="second-navbar-item-link">
					Badges ({{count($badges)}}/{{count($totalBadges)}})
				</a>
			</li>
		</ul>
	</nav>
@stop

        
@section('second-content')
	<div class="sec-container">
	    <main class="container">
			<section id="sec-content">
				@include('includes.myboost')
				@include('includes.favourites')
				@include('includes.follows')
				@include('includes.followers')
				@include('includes.badges')
				@include('includes.activity')
				<div class="spacebetween"></div>
			</section>
		</main>
    </div>
@stop

@section('scripts')
    <script>
		$(document).ready(function() {

			$(document).ready(function() {
				$(".alert").delay(1500).slideUp();
			});

			$(".profile-info-more").hide();
				
			$(".account-info-showmore").click(function() {
				console.log('toggle!');
				event.preventDefault();
			    $(".profile-info-more").slideToggle();
			});

			$(".container-show-badges").hide();
			$(".container-show-favourites").hide();
			$(".container-show-follows").hide();
			$(".container-show-followers").hide();
			$(".container-show-activity").hide();
			$("#boosts").addClass('bubble-tip');

			$("#badges").click(function() {
				event.preventDefault();
				$('.second-navbar-item-link').removeClass('bubble-tip');
				$(this).addClass('bubble-tip');
				$(".container-show-myboosts").slideUp();
				$(".container-show-followers").slideUp();
				$(".container-show-favourites").slideUp();
				$(".container-show-follows").slideUp();
				$(".container-show-activity").slideUp();
				$(".container-show-badges").slideDown();
			});

			$("#activity").click(function() {
				event.preventDefault();
				$('.second-navbar-item-link').removeClass('bubble-tip');
				$(this).addClass('bubble-tip');
				$(".container-show-myboosts").slideUp();
				$(".container-show-followers").slideUp();
				$(".container-show-favourites").slideUp();
				$(".container-show-follows").slideUp();
				$(".container-show-badges").slideUp();
				$(".container-show-activity").slideDown();
			});

			$("#likes").click(function() {
				event.preventDefault();
				$('.second-navbar-item-link').removeClass('bubble-tip');
				$(this).addClass('bubble-tip');
				$(".container-show-badges").slideUp();
				$(".container-show-myboosts").slideUp();
				$(".container-show-follows").slideUp();
				$(".container-show-followers").slideUp();
				$(".container-show-activity").slideUp();
			    $(".container-show-favourites").slideDown();
			});

			$("#follows").click(function() {
				event.preventDefault();
				$('.second-navbar-item-link').removeClass('bubble-tip');
				$(this).addClass('bubble-tip');
				$(".container-show-badges").slideUp();
				$(".container-show-favourites").slideUp();
				$(".container-show-myboosts").slideUp();
				$(".container-show-followers").slideUp();
				$(".container-show-activity").slideUp();
			    $(".container-show-follows").slideDown();
			});

			$("#boosts").click(function() {
				event.preventDefault();
				$('.second-navbar-item-link').removeClass('bubble-tip');
				$(this).addClass('bubble-tip');
				$(".container-show-badges").slideUp();
				$(".container-show-favourites").slideUp();
				$(".container-show-followers").slideUp();
			    $(".container-show-follows").slideUp();
				$(".container-show-activity").slideUp();
				$(".container-show-myboosts").slideDown();
			});

			$("#followers").click(function() {
				event.preventDefault();
				$('.second-navbar-item-link').removeClass('bubble-tip');
				$(this).addClass('bubble-tip');
				$(".container-show-myboosts").slideUp();
				$(".container-show-badges").slideUp();
				$(".container-show-favourites").slideUp();
				$(".container-show-follows").slideUp();
				$(".container-show-activity").slideUp();
			    $(".container-show-followers").slideDown();
			});
		});
    </script>
@stop 