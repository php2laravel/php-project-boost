@extends('app')
@section('message')
    @if (Session::has('flash_notification.message'))
        <p class="alert alert-admin alert-{{ Session::get('flash_notification.level') }}">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ Session::get('flash_notification.message') }}
        </p>
    @endif 
@stop
@section('content')
	<div class="content-spotlight">
		<br class="clear"/>
		<h3 class="section-title">All projects</h3>
	    <div class="section-line"></div>
	    <br class="clear"/>
	    
	    @if (Auth::check())
	    	 @include('includes.cta')
	    @endif

	    @foreach ($projects as $project)
	    	<div class="content-spotlight-item">
                <a class="content-spotlight-link" href="/projects/{!! $project->id !!}">
                    <img class="content-spotlight-item_image" src="{{URL::to('/')}}/uploads/projects/{!! $project->image !!}" alt="imageproject">
                    <div class="content-spotlight-item_info">
                        <p class="content-spotlight-item_info-namedesign">{!! $project->title !!}</p>
                </a>
                <?php $designerName = strtolower(str_replace(' ', '-',  $project->name)); ?>
                <a class="content-spotlight-link" href="/<?php echo $designerName ?>">
                    <p class="content-spotlight-item_info-author">{!! $project->name !!}</p>
                </a>
                        <ul class="content-spotlight-item_info-tools">
                            <li class="cmmnt">
                            	<img src="<?php echo asset('images/icons/comment.png')?>" alt="heart icon" class="icon-small">
								<span> {!! $project->countcomments !!} </span>
                            </li>
                            <li class="fav">
                            	<img src="<?php echo asset('images/icons/heart.png')?>" alt="heart icon" class="icon-small">
								<span> {!! $project->countlikes !!} </span>
                            </li>
                            <br class="clearfix"/>
                        </ul>
                    </div>
            </div>
		@endforeach
        <br>
        {!! $projects->render() !!}
		</div>
	</div>
@stop
@section('scripts')
    <script>
        $(document).ready(function() {
            $(document).ready(function() {
                $(".alert").delay(1500).slideUp();
            });
        });
    </script>
@stop 
