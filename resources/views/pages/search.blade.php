@extends('app')
<?php 
    if (Auth::check()) {
        $user = Auth::user();
    }
    if (!isset($designers)) { $designers = array(); }
    if (!isset($tag)) { $tag = array(); }
    if (!isset($tags)) { $tags = array(); }
    if (!isset($desc)) { $desc = array(); }
    if (!isset($colors)) { $colors = array(); }
    if (!isset($palette)) { $palette = array(); }
    if (!isset($projects)) { $projects = array(); }
    if (!isset($options)) { $options = false; }

?>
@section('content')
        @if($options)
            <div>
                <h1> You searched for <span class="search-query">{{ $query }}</span></h1>
                <a href="#" class="search-options">Show more options</a>
                <img src="<?php echo asset('images/icons/search.png')?>" alt="search icon" class="icon-small search-options-icon">
                {!! Form::open( array('class'=>'form-search-options', 'role' => 'search', 'id'=>'form-search-options')) !!}
                    <h4>Show all results with <span class="muted">{{ $query }}</span> in the </h4>
                    <label class="checkbox-inline">
                      <input type="checkbox" id="inlineCheckbox1" value="title" checked> title
                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox" id="inlineCheckbox2" value="description" checked> description
                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox" id="inlineCheckbox3" value="tag" checked> tags
                    </label>
                    <label class="checkbox-inline">
                      <input type="checkbox" id="inlineCheckbox3" value="designer" checked> username
                    </label>
    
                    {!! Form::submit('Filter results', array('class'=>'btn btn-default btn-filter'))!!}
                {!! Form::close() !!}
            </div>
        @endif

        @if(count($projects) == 0 && count($designers) == 0 && count($tags) == 0 && count($colors) == 0)

            <h3 class="show-titles show-descriptions">No Results Found</h3>

            @endif

        <br class="clear"/>
        <div class="search-results">

            @if (count($projects) >= 1 || count($desc) >= 1)
                <h3 class="show-titles show-descriptions"> Projects: {{count($projects)+count($desc)}}  </h3>
                @if (count($projects) >= 1)
                    <div class="search-container show-titles">
                        @foreach($projects as $p)
                            <div class="search-container-result shadow-box">
                                <a href="/projects/{{$p->id}}">
                                    <img src="{{URL::to('/')}}/uploads/projects/{{ $p->image }}" class="search-container-result-image"> 
                                
                                <div class="search-container-result-info">
                                    <a href="/projects/{{$p->id}}">
                                        <h4 class="search-container-result-title">{{ $p->title }}</h4>
                                    </a>
                                    <p>{{ $p->desc }}</p>
                                </div>
                            </div>
                         @endforeach
                    </div>
                @endif
                @if (count($desc) >= 1)
                    @foreach($desc as $d)
                        <div class="search-container search-container-result-big show-descriptions shadow-box">
                            <a href="/projects/{{$d->id}}">
                                <img src="{{URL::to('/')}}/uploads/projects/{{ $d->id }}/{{ $d->image }}" class="search-container-result-big-image"> 
                                <div class="search-container-result-info">
                                <h4 class="search-container-result-title">{{ $d->title }}</h4>
                            </a>
                            <p>{{ $d->desc }}</p>
                                </div>
                        </div>
                    @endforeach
                @endif
            @endif
            <br class="clear"/>

            @if (count($colors) >= 1)
                <h3 class="show-titles show-colors"> Projects with colour <span class="colour-preview">{{$query}}</span>: {{count($colors)}}  </h3>
                    @if (count($colors) >= 1)
                        @foreach($colors as $c)
                            <div class="search-container-result show-colors">
                                <a href="/projects/{{$c->id}}">
                                    <img src="{{URL::to('/')}}/uploads/projects/{{ $c->image }}" class="search-container-result-image"> 
                                    <div class="search-container-result-info">
                                    <h4>{{ $c->title }}</h4>
                                </a>
                                <p>
                                    <div class="colour-palette">
                                        @foreach($palette as $pal)
                                            <a href="colour?search={{ $pal }}"><div class="colour-palette-item" style="background-color:#{{ $pal }}"></div></a>
                                        @endforeach
                                    </div>
                                </p>
                            </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    @endif
            @endif
            <br class="clear"/>


            <br class="clear"/>
            @if (count($tags) >= 1)
                <h3 class="show-tags"> Projects tagged as 
                    @foreach ($tag as $tg)
                        @if($tg == $tag[0])
                            <span class="result-tag"> {{ $tg->name }}</span>
                        @else
                            <span class="result-tag">or {{ $tg->name }}</span>
                        @endif   
                    @endforeach
                    : {{count($tags)}}
                </h3>
                <div class="search-result-container">
                    @foreach($tags as $t)
                        <div class="search-container-result-big show-tags shadow-box">
                            <a href="/projects/{{$t->id}}">
                                <img src="{{URL::to('/')}}/uploads/projects/{{ $t->image }}" class="search-container-result-tag-image"> 
                                <div class="search-container-result-info">
                                <h4 class="search-container-result-title">{{ $t->title }}</h4>
                                    </a>
                                    <div class="results-tags">
                                        @foreach ($t->tags as $tag)
                                            <span class="tag">
                                                <a href="../tag?search={{ $tag->name }}" title="all project tagged as {{ $tag->name }}">
                                                    {{ $tag->name }}
                                                </a>
                                            </span>
                                        @endforeach  
                                    </div>  
                                </div>
                        </div>
                    @endforeach
                </div>
            @endif
            <br class="clear"/>
            @if (count($designers) >= 1)
                <h3 class="show-designers"> Designers: {{count($designers)}}  </h3>
                <div class="search-result-container">
                    @foreach($designers as $d)
                        <?php $designershort = strtolower(str_replace(' ', '-',  $d->name)); ?>
                        <div class="search-container-result-user show-designers shadow-box">
                            <a href="/{{$designershort}}">
                                @if ($d->image != "")
                                    <img src="{{URL::to('/')}}/uploads/users/{{ $d->image }}" class="search-container-result-image">
                                @else
                                    <img src="http://jbryner.me/_images/placeholder_small.gif" alt="placeholder user picture" class="search-container-result-image">
                                @endif 
                                <div class="search-container-result-info">
                                <h4 class="search-container-result-title">{{ $d->name }}</h4>
                                </a>
                                </div>
                        </div>
                    @endforeach
                </div>
            @endif
        </div>
        @if (count($projects) === 0 && count($designers)=== 0 && count($desc) ===0 && count($tags) === 0 && count($palette) === 0)  
        <p> There are no results for "{{ $query }}".</p>
        @endif
    @stop
    
@section('scripts')
    <script>
        $(document).ready(function() {
            
            var query = $('.search-query').html();
            $('.form-search-options').hide();
            $("#search-field").val(query);

            $(".colour-preview").css('border-bottom', "<?php echo $query ?> solid 4px");
            $(".colour-preview").css('padding', '4px 4px 2px 4px');
            $('.search-container').highlight(query);

            $('.search-options').click(function() {
                event.preventDefault();
                $('.form-search-options').slideToggle();
                console.log($(this).text());
                if ($(this).text()== "Show more options") {
                    $(this).text("x")
                } else {
                    $(this).text("Show more options")
                };
            });

            $('.btn-filter').click(function() {
                event.preventDefault();
                var data = $("#login_form :input").serializeArray();
                var data = data.concat(
                jQuery('.form-search-options input[type=checkbox]:checked').map(
                        function() {
                            return {"name": this.name, "value": this.value}
                        }).get())
                $('.show-titles').slideUp();
                $('.show-designers').slideUp();
                $('.show-descriptions').slideUp();
                $('.show-projects').slideUp();
                $('.show-tags').slideUp();
                //console.log(data);
                for (i = 0; i < data.length; i++) { 
                    $('.show-'+data[i]['value']+'s').slideDown();
                };
            });
        });
    </script>
@stop
