@extends('app')
<?php 
    if (Auth::check()) {
        $user = Auth::user();
    }
?>
@section('content')
    <div class="container-show-notifications">
        <h2>Notifications</h2>

        <div class="likes-overview">
            <h3>Likes ({{count($notificationLikes)}})</h3>
            @foreach ($notificationLikes as $like)
        		<div class="row">
                	<div class="col-md-4">
                        <img src="<?php echo asset('images/icons/voted.png')?>" alt="heart icon" class="icon-small">
                        {{ \Carbon\Carbon::createFromTimeStamp(strtotime($like->created_at))->diffForHumans() }}
                    </div>
                	<div class="col-md-8">{{ $like->name }} likes your project <a href="{{ action('ProjectsController@show', [$like->projectId]) }}">{{ $like->title }}</a>!</div>
        		</div>
            @endforeach
            {!! $notificationLikes->render() !!}

        </div>
        <div class="comments-overview">
            <h3>Comments ({{count($notificationLikes)}})</h3>
            @foreach ($notificationComments as $comment)
        		<div class="row">
                	<div class="col-md-4">
                        <img src="<?php echo asset('images/icons/comment.png')?>" alt="heart icon" class="icon-small">
                        {{ \Carbon\Carbon::createFromTimeStamp(strtotime($comment->created_at))->diffForHumans() }}
                    </div>
                    <div class="col-md-8">

                    @if ($comment->user_id == $user->id)
                        {{ $comment->user_id }}
                        {{ $user->id }}
                        You
                    @else
                        {{ $comment->name }}
                    @endif
                	 commented on your project <a href="{{ action('ProjectsController@show', [$comment->project_id]) }}">{{ $comment->title }}</a>: {{ $comment->body}}!
                    </div>
        		</div>
            @endforeach
            {!! $notificationComments->render() !!}
        </div>
    </div>
@stop