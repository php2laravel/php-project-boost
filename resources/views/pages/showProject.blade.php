@extends('app')
@section('head')
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="csrf-token"           content="{{ csrf_token() }}">        
        <meta property="og:url"           content="boost.dev/projects/{{ $project->id }}" />
        <meta property="og:type"          content="boost.dev" />
        <meta property="og:title"         content="{{ $project->title }}" />
        <meta property="og:description"   content="{{ $project->desc }}" />
        <meta property="og:image"         content="{{URL::to('/')}}/uploads/projects/{{ $project->image }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <title>Boost | {{ $project->title }} </title>

        <link rel="shortcut icon" href="<?php echo asset('favicon.ico')?>" type="image/x-icon">
        <link rel="icon" href="<?php echo asset('favicon.ico')?>" type="image/x-icon">

        <link rel="stylesheet" href="<?php echo asset('css/datepicker.css')?>" type="text/css">
        <link rel="stylesheet" href="<?php echo asset('sass/modules/fonts/webfontkit/stylesheet.css')?>" type="text/css">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.css">
        <link rel="stylesheet" href="<?php echo asset('css/style.css')?>" type="text/css">
       
    
        <script src="https://code.jquery.com/jquery-2.1.4.js"></script>
        <script src="<?php echo asset('js/bootstrap-datepicker.js')?>"></script>
        <script src="https://cdn.jsdelivr.net/select2/4.0.1-rc.1/js/select2.min.js">
        </script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script src="<?php echo asset('js/base.js')?>"></script>

    </head>
@stop
<?php 
    if (Auth::check()) {
        $user = Auth::user();
    }
    if (!isset($contest)) { $contest = false; }

    $date = date("d-m-Y", strtotime($project->created_at)); 
?>
@section('second-message')
     <div class="project-detail-page">
        <main class="container">
            @if (Session::has('flash_notification.message'))
                <p class="alert alert-admin alert-{{ Session::get('flash_notification.level') }} alert-top alert-second">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
                </p>
            @endif 
        </main>
     </div>
@stop
    @section('second-content')
        <div class="project-detail-page">
            <aside class="shadow-box project-block">
                <article>
                    <div class="project-top">
                        <div class="project-title-block">
                            <h1>
                                {{ $project->title }}
                            </h1>
                            <p class="muted">Posted by <a href="/{{ $designershort }}" "designer">{{ $designer->name }}</a> on {{ $date }}.</p>
                        </div>

                        <div class="project-form-block">

                            @if ($project->user_id === Auth::user() || Auth::user()->admin == 1)
                            <div class="form-adm">
                                <!--<div class="form-vote">
                                    <label class="btn-user btn-user-vote"></label>
                                    <span class="btn-user-vote_count">{{ $countVotes }}</span>
                                </div>
                                -->
                                <a href="../projects/edit/{{ $project->id }}" class="btn btn-adm btn-adm-edit">edit</a>
                                <button class="btn btn-adm btn-adm-delete" type="button" data-toggle="collapse" data-target="#collapseDeleteForm" aria-expanded="false" aria-controls="collapseExample">
                                  delete
                                </button>
                            </div> 
                            <br class="clear"></br> 
                            <div class="collapse" id="collapseDeleteForm">
                                <p>Are you sure you want to delete this project?</p>
                                <a href="../projects/delete/{{ $project->id }}" class="btn btn-small btn-delete-confirm">Yes, delete it.</a>
                                <span class="spacebetween"></span>
                                <button  class="btn btn-default btn-primary" data-toggle="collapse" data-target="#collapseDeleteForm" aria-controls="collapseExample">No</button>
                            </div>
                            @endif
                            <!-- if current user ( $user->id) == author project ($project->userid)-->
                            @unless ($project->user_id === Auth::user())
                                <div class="form-user">
                                    <div class="loading"></div>
                                        {!! Form::open( array('class'=>'form-user deletelike', 'id'=>'project-vote', 'action' => 'VoteController@delete')) !!}
                                            {!! Form::hidden('userId', $user->id) !!}
                                            {!! Form::hidden('projectId', $project->id) !!}
                                            {!! Form::submit('Like ', array('class'=>'btn-user btn-user-vote hearted'))!!}
                                            {!! Form::label('Count', $countVotes, array('class'=>'btn-user-vote_count'))!!}
                                            <!--{!! Form::hidden('_token', "{{ csrf_token() }}") !!}-->
                                        {!! Form::close() !!}  
                                         
                                        {!! Form::open( array('class'=>'form-user postlike', 'id'=>'project-vote', 'action' => 'VoteController@store')) !!}
                                            {!! Form::hidden('userId', $user->id) !!}
                                            {!! Form::hidden('projectId', $project->id) !!}
                                            {!! Form::submit('Like ', array('class'=>'btn-user btn-user-vote'))!!}
                                            {!! Form::label('Count', $countVotes, array('class'=>'btn-user-vote_count'))!!}
                                            <!--{!! Form::hidden('_token', "{{ csrf_token() }}") !!}-->
                                        {!! Form::close() !!}               
                                        
                                </div> 
                            @endunless
                        </div>
                    </div>
                            
                    <img src="{{URL::to('/')}}/uploads/projects/{{ $project->image }}" class="project-image">
                </article>
                <h2>Feedback
                        @if(count($comments) > 1)
                            <small>
                                ({{count($comments)}}<span class="comment-title-span"> comments</span>)
                            </small>
                        @elseif(count($comments) == 1)
                            <small>
                                ({{count($comments)}}<span class="comment-title-span"> comment</span>)
                            </small>
                        @endif
                </h2>
                <article> <!--Add comment -->
                        <br/>
                        {!! Form::open(array('class'=>'form-inline comment_form', 'id'=>'comment-form', 'action' => 'CommentController@store')) !!}
                        {!! form::text('body', null, ['class' => 'form-control form-inline comment_form-input', 'placeholder'=>'Give '.$designer->name.' some feedback']) !!}
                        {!! Form::Submit('Post Comment', ['class' => 'btn btn-small form-inline comment_form-btn']) !!}
                        {!! Form::hidden('project_id', $project->id) !!}
                        <!--{!! Form::hidden('_token', "{{ csrf_token() }}") !!}-->

                        <br class="clear"/>
                        {!! Form::close() !!}
                        <br/>

                    </article>

                    <article>
                        @if (count($comments) < 1)
                            @if ($project->user_id === $user->id)
                                <p class="center">At the moment, there are no comments for this project.</p>
                            @else
                                <p class="center">Add a comment and be the first to give {{$designer->name}} feedback for this project.</p>
                            @endif
                        @endif
                       
                        <br class="clear"></br>
                        <?php $i=0; ?>
                        <ul class="comments-render"></ul>

                            @foreach ($comments as $comment)
                            <li class="comments-render-item">
                                @if($i<=2)
                                    <article class="comment-div">
                                        <a href="/{{ strtolower(str_replace(' ', '-',  $comment->name)) }}">
                                            <div class="comment-picture">
                                                 @if ($comment->image != "")
                                                    <img src="{{URL::to('/')}}/uploads/users/thumbnails/{{ $comment->image }}" class="comment-picture-img">
                                                @else
                                                    <img src="http://jbryner.me/_images/placeholder_small.gif" alt="placeholder user picture" class="comment-picture-img">
                                                @endif
                                            </div>
                                            <div class="comment-body">
                                                <span class="comment-body-name">{{ $comment->name }}</span>
                                        </a>
                                           <span class="comment-date"><?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($comment->created_at))->diffForHumans() ?></span>
                                             <p class="comment-body-body">{{ $comment->body }}</p>

                                        </div>
                                        <br class="clear"/>
                                    </article>
                                @endif
                                <?php 
                                    $i++; 
                                ?>
                            </li>
                            @endforeach
                        {!! $comments->render()!!}

                        @if (count($comments) > 3)
                            <a href="#" class="subtle-link center toggle-comments-container"><br>show all comments</a>
                        @endif
                        <div class="comments-container">
                            <?php $i=0; ?>
                            @foreach ($comments as $comment)
                                @if ($i>2)
                                    <article class="comment-div">
                                        <div class="comment-picture">
                                             @if ($comment->image != "")
                                                <img src="{{URL::to('/')}}/uploads/users/thumbnails/{{ $comment->image }}" class="comment-picture-img">
                                            @else
                                                <img src="http://jbryner.me/_images/placeholder_small.gif" alt="placeholder user picture" class="comment-picture-img">
                                            @endif
                                        </div>
                                        <div class="comment-body">
                                            <span class="comment-body-name">{{ $comment->name }}</span>
                                           <span class="comment-date"><?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($comment->created_at))->diffForHumans() ?></span>
                                        
                                            <p class="comment-body-body">{{ $comment->body }}</p>
                                        </div>
                                        <br class="clear"/>
                                    </article>
                                @endif
                                <?php 
                                    $i++;
                                ?>
                            @endforeach
                            {!! $comments->render()!!}

                        </div>
                    </article>
            </aside>
            <aside class="project-sidebar">
                <h2>Info</h2>
                <article>
                    <p>{{ $project->desc }}</p>
                    <div class="colour-palette">
                        @if(isset ($palette[0]))
                        <a href="../colour?search={{ $palette[0] }}"><div class="colour-palette-item" style="background-color:#{{ $palette[0] }}"></div></a>
                        @endif
                        @if(isset ($palette[1]))
                        <a href="../colour?search={{ $palette[1] }}"><div class="colour-palette-item" style="background-color:#{{ $palette[1] }}"></div></a>
                        @endif
                        @if(isset ($palette[2]))
                        <a href="../colour?search={{ $palette[2] }}"><div class="colour-palette-item" style="background-color:#{{ $palette[2] }}"></div></a>                
                        @endif
                    </div>
                </article>
               
                <br class="clear"/>
                <article class="showvotes">
                    <img src="<?php echo asset('images/icons/voted.png')?>" alt="heart icon" class="icon-small">
                     <!-- display likes:
                        * 0 likes = messag
                            -> different message if you created this project
                        * 1 like = "X likes this project"
                        * 2 or 3 likes = "X and Y like this project"
                        * more than 3 likes = "X, Y, Z and i others like this project"
                    -->
                    @if ($countVotes >= 1)
                        <span>
                            @foreach ($topVotes as $vote)
                                @if ($countVotes === 1)
                                    <span>{{$vote->name}}</span>
                                @elseif ($countVotes === 2)
                                    <span>
                                        @if ($vote == $votes[0])
                                            {{$vote->name}}
                                        @else
                                            and {{$vote->name}}
                                        @endif
                                    </span>
                                @elseif ($countVotes === 3)
                                    <span>
                                        @if ($vote == $votes[0])
                                            {{$vote->name}},
                                        @elseif ($vote == $votes[1])
                                            {{$vote->name}}
                                        @else
                                            and {{$vote->name}}
                                        @endif
                                    </span>
                                @elseif ($countVotes >= 3)
                                    <span>
                                        @if ($vote != $votes[0])
                                        , {{$vote->name}}
                                        @else
                                            {{$vote->name}}
                                        @endif
                                    </span>
                                @endif 
                            @endforeach
                            @if ($countVotes === 4)
                                <span> and 1 other </span>
                            @elseif ($countVotes > 4)
                                <span> and {{ $countVotes-3 }} others </span>
                            @endif
                            boosted this project
                        </span>
                        @elseif($countVotes === 0 && $project->user_id != $user->id)
                            <span>Be the first to like this project.</span>
                        @elseif($countVotes === 0 && $project->user_id === $user->id)
                            <span>At the moment, there are no votes for this project.</span>
                    @endif
                    @if ($countVotes >= 4)
                        <br>
                        <a href="#">See all</a>
                    @endif
                </article>

                <article class="showallvotes">
                    <img src="<?php echo asset('images/icons/voted.png')?>" alt="heart icon" class="icon-small">
                    @foreach ($votes as $vote)
                        <span>{{ $vote->name }}, </span>
                    @endforeach
                    <span> boosted this project.</span>
                </article>
                <br class="clear"/>
                <!-- Your share button code -->
                    <div class="fb-share-button" data-href="http://boost.weareimd.be/projects/{{ $project->id }}" data-layout="button_count">
                    </div>
                <article>
                @unless($project->tags->isEmpty())
                        <h3>Tags</h3>
                            @foreach ($project->tags as $tag)
                                <span class="tag">
                                    <a href="../tag?search={{ $tag->name }}" title="all project tagged as {{ $tag->name }}">
                                        {{ $tag->name }}
                                    </a>
                                </span>
                            @endforeach    
                @endunless
                <br class="clear"/>
 
                </article>
  
                <br class="clear"/>
 
                <h3>Designer</h3>
                <article class="designer-container">    
                    <div class="designer-info">
                        <a href="/{{ $designershort }}" class="designer-info-name">{{ $designer->name }}</a>
                        @if ($designer->locatie != "")
                            <p class="designer-info-locatie">{{$designer->locatie}}</p>
                        @endif
                        <a href="/{{ $designershort }}" class="designer-info-name">
                            @if ($designer->image != "")
                                <img src="{{URL::to('/')}}/uploads/users/{{ $designer->image }}" class="designer-info-img">
                            @else
                                <img src="http://jbryner.me/_images/placeholder_small.gif" alt="placeholder user picture" class="designer-info-img">
                            @endif
                        </a>
                    </div>

                    <br class="clear"/>
                    <br>
                    <div class="designer-info-more">
                        <p><b>More</b> from {{ $designer->name }}</p>
                        <div class="designer-info-more-gallery">
                            @foreach ($firstWork as $w)
                                <div class="designer-info-more-gallery-item">
                                    <a href="/projects/{{$w->id}}">
                                    <img src="{{URL::to('/')}}/uploads/projects/{{ $w->image }}" class="project-image"> 
                                    </a>
                                </div> 
                            @endforeach
                            <br class="clear"></br>
                            <a href="/{{ $designershort }}" class="btn btn-small">Visit profile</a>
                        </div>
                    </div>
                </article>
            </aside>
        </div>
    @stop

    @section('scripts')
        <script type="text/javascript">

            var myvote = {{$myvote}} + "";
            console.log(myvote);
            if(myvote == 1){
                console.log('voted');
                $('form.postlike').css('display','none');
            }
            else{
                console.log('not voted');
                $('form.deletelike').css('display','none');

            }

            $("form#project-vote.form-user.postlike").submit(function(e){
                    e.preventDefault();
                    var userId = {{$user->id}};
                    var projectId =  {{$project->id}};
                    var dataString = { userId: userId, projectId: projectId }; 
                    var num = parseInt($.trim($('label.btn-user-vote_count').html()));
                    $('label.btn-user-vote_count').html(++num);
                    $('form.postlike').css('display', 'none');
                    $('form.deletelike').css('display', 'block');
                    $.ajax({
                        type: "POST",
                        url : "vote/{{ $project->id }}",
                        data : dataString,
                        success : function(data){
                            console.log(data);
                        }
                    },"json");

                });

                $("form#project-vote.form-user.deletelike").submit(function(e){
                    e.preventDefault();
                    var userId = {{$user->id}};
                    var projectId =  {{$project->id}};
                    var dataString = { userId: userId, projectId: projectId }; 
                    var num = parseInt($.trim($('label.btn-user-vote_count').html()));
                    $('label.btn-user-vote_count').html(--num);
                    $('form.deletelike').css('display', 'none');
                    $('form.postlike').css('display', 'block');
                    $.ajax({
                        type: "POST",
                        url : "delete/{{ $project->id }}",
                        data : dataString,
                        success : function(data){
                            console.log(data);
                        }
                    },"json");
                });

                $("form#comment-form").submit(function(e){
                    e.preventDefault();
                    var userId = {{$user->id}};
                    var projectId =  {{$project->id}};
                    var body = $('input.form-control.comment_form-input').val();
                    var dataString = { user_id: userId, project_id: projectId, body: body };
                    var fakecomment = "<li class='comments-render-item'><article class='comment-div'><a href='/{{ strtolower(str_replace(' ', '-',  $user->name)) }}'><div class='comment-picture'><img src='{{URL::to('/')}}/uploads/users/thumbnails/{{ $user->image }}' class='comment-picture-img'></div><div class='comment-body'><span class='comment-body-name'>{{ $user->name }}</span></a><span class='comment-date'> Now</span><p class='comment-body-body'>"+body+"</p></div><br class='clear'/></article></li>"; 
                    $('ul.comments-render').prepend(fakecomment);
                    $('ul.comments-render').css('display', 'none');
                    $('ul.comments-render').slideDown('slow');
                    $('input.form-control.comment_form-input').val('');
                    $.ajax({
                        type: "POST",
                        url : "/projects/{{ $project->id }}",
                        data : dataString,
                        success : function(data){
                            console.log(data);
                        }
                    },"json");

                });
                
            $(document).ready(function(){
                if ($('.btn-user-vote_count').html()=="Count") {
                    $('.btn-user-vote_count').html("0");
                }

                $('.toggle-comments-container').click(function(event) {
                    event.preventDefault();
                    $('.comments-container').slideToggle();
                    $('a.subtle-link.center.toggle-comments-container').css('display', 'none');
                });

                $('article.showallvotes').slideUp('fast');

                $('article.showvotes').on('click', function(){
                    $('article.showvotes').slideUp("fast", function(){
                        $('article.showallvotes').slideDown('fast');
                    });
                    return false;
                });

            });
        
        
            
        </script>
    @stop

