@extends('app')

@section('banner')
    <div class="banner banner-contest">
        <div class="banner-darken">
            @include('includes.header')
            <p class="banner-quote">A LITTLE CONTEST NEVER HURT NOBODY.</p>
            @unless (Auth::check())
            <a class="btn btn-default  banner-registerbtn" href="auth/register">Sign up (it's free!)</a>
            @endunless
        </div>
    </div>
    @include('includes.nav')
@stop

@section('content')
	<div class="apply">
		<h1>Apply for our contest!</h1>

		<p>1. Upload your work <br>
		2. Voting starts <br>
		3. At the end of the month, the winner will be announced!
		</p>

        {!! Form::open(array('url' => 'applycontest', 'files' => true, 'class'=>'create-contest_form')) !!}
            {!! Form::label('title', 'Title:', ['class'=>'create-contest_form-label']) !!}
            {!! form::text('title', null, ['class' => 'form-control create-contest_form-input', 'autofocus']) !!}

            {!! Form::label('desc', 'Description: ', ['class'=>'create-contest_form-label']) !!}
            {!! form::textarea('desc', null, ['class' => 'form-control create-contest_form-input']) !!}

            {!! Form::label('tag_list', 'Tags:', ['class' => 'create-contest_form-label'])!!}
            {!! Form::select('tag_list[]', $tags,  null, ['class' => 'form-control create-contest_form-input selecttag', 'multiple', 'id' => 'tag_list'])!!}
            <br>
            {!! Form::label('image', 'Add an image (Please use 800x800 for the best effect.)', ['class'=>'create-contest_form-label']) !!}
            {!! form::file('image', null, ['class' => 'form-control create-contest_form-input'])!!}

            {!! Form::label('preview', 'Preview:', ['class' => 'create-project_form-label']) !!}
            <div id="preview" class="create-project_form-preview"></div>
            <br/>

            {!! Form::Submit('Add your work', ['class' => 'form-inline btn btn-big btn-center']) !!}

        {!! Form::close() !!}
	</div>
		<br>
		<div class="back">
			<a class="subtle-link center" href="contests">Go back</a>
            <br class="clear"></br>
		</div>
	
	@stop			

@section('scripts')
    <script>
        jQuery(document).ready(function() {
            jQuery("select.selecttag").select2();
        
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    console.log("changies!");

                    reader.onload = function (e) {
                        $('#preview').css('background', 'transparent url('+e.target.result +') center center no-repeat');
                         $('#preview').css('background-size', 'auto 100%');
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#image").change(function(){
                readURL(this);
            });
        });
    </script>
@stop