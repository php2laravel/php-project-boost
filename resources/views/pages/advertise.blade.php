@extends('app')

@section('banner')
	<div class="banner banner-advertise">
		<div class="banner-darken">
			@include('includes.header')
		    <p class="banner-quote">Give your company a Boost.</p>
		    @if (Auth::check())
		    <a class="btn btn-big banner-registerbtn" href="#start">Get started now</a>
		    @else
		    <a class="btn btn-default  banner-registerbtn" href="auth/register">Sign up (it's free!)</a>
		    @endif
		</div>
	</div>

	<a name="start"></a>
	@include('includes.nav')
@stop

@if (Auth::check())
	@section('content')
		<article class="adv-article">
		<br>
		    @if (Session::has('flash_notification.message'))
                <p class="alert alert-admin alert-{{ Session::get('flash_notification.level') }} alert-top alert-second">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
                </p>
            @endif 

			<h1>Get your advertentions on Boost, now!</h1>
			<p class="adv-more">By placing your ad here, hundreds of multimedia students will get to know your brand every day. You'll get an overview of the clicks and results.
			</p>

			<h3>It's easy</h3>
			<ol>
				<li>Send in your banner</li>
				<li>Advertisements will be visible on every page</li>
				<li>We work with 1 month slots</li>
				<li>You can see how successful your advertisements are!</li>
			</ol>
		</article>

	    <article class="big-sidebar panel-default shadow-box advertise-sidebar-second">
			@if (count($errors) > 0)
			    <div class="alert alert-danger">
			        <ul>
			            @foreach ($errors->all() as $error)
			                <li>{{ $error }}</li>
			            @endforeach
			        </ul>
			    </div>
			@endif
			@if ($countads == 3)

			<h3>We are sorry, we don't have place for more ads at the moment, please come back later.</h3>

			@else

				{!! Form::open(array('url' => 'advertise', 'files' => true, 'class'=>'create-ad_form')) !!}
	            {!! Form::label('companyname', 'Slogan for the advertisement:', ['class' => 'create-ad_form-label']) !!}

	            {!! form::text('companyname', null, ['class' => 'form-control create-ad_form-input', 'placeholder'=> 'ex: Facebook']) !!}

	            {!! Form::label('website', 'Your website: ', ['class' => 'create-ad_form-label']) !!}
	            {!! form::url('website', null, ['class' => 'form-control create-ad_form-input', 'placeholder'=> 'ex: http://www.facebook.com']) !!}
	            
	            {!! Form::label('image', 'Add an image (Use 600x250 images for the best effect.)', ['class' => 'create-ad_form-label']) !!}
	            {!! form::file('image', null, ['class' => 'form-control'])!!}
	            Mi
	            {!! Form::label('startdate', 'Start date:', ['class' => 'create-ad_form-label']) !!}
	            {!! form::text('startdate', null, ['class' => 'form-control create-ad_form-input', 'id'=>'datepicker', 'placeholder'=> 'ex. ' . date("d-m-Y") ]) !!}


	            {!! Form::label('price', 'Price for 1 month: € 150,00', ['class' => 'create-ad_form-label']) !!}
	            
	            <br />
		        <br />
				<script
				    src="https://checkout.stripe.com/checkout.js" class="stripe-button btn-small"
				    id="stripe"
				    data-key="pk_test_DFdyaj28NlvoR7ZoSkRJukJF"
				    data-image="{{URL::to('/')}}/uploads/users/{{ Auth::user()->image }}"
				    data-name="{{Auth::user()->name}}"
				    data-description="Thanks for your advertisement"
				    data-currency="eur"
				    data-amount="15000"
				    data-locale="auto">
				</script>
	        	{!! Form::close() !!}


			@endif
		</article>
			<br class="clear"></br>


	    <article class="big-sidebar panel-default shadow-box advertise-sidebar-second">
	    	@if(count($advertisements) > 0)
		    	<h2 class="center"> Your advertisements:</h2>


		    	@foreach($advertisements as $ad)
					    <a href="/advertise/{!! $ad->id !!}" title="Advertise page" target="_blank">
						    <h2>{{ $ad->companyname }}</h2>
						   	<img src="{{URL::to('/')}}/uploads/advs/{!! $ad->image !!}" alt="{{ $ad->companyname }}">
						   	<br>
						</a>
						   	<div>
						   		<span><i class="fa fa-eye"></i> {{ $ad->views }}</span>
						   		<br>
						   		<span><i class="fa fa-hand-pointer-o"></i> {{ $ad->clicks }}</span>
						   		<br>
						   		<span>I paid € {{ $ad->price }} for this advertisement and ends {{ $ad->enddate }}.</span>
							</div>
						
				@endforeach

			
			@else
				<h2 class="center">Create your first advertisement!</h2>
			
			@endif
		</article>
	@stop
@else
	@section('message')
		<article class="adv-article">
			<h1>Get your advertentions on Boost now</h1>
			<p class="adv-more">By placing your ad here, hundreds of multimedia students will get to know your brand every day. You'll get an overview of the clicks and results.
			</p>

			<h3>It's easy</h3>
			<ol>
				<li >Send in your banner</li>
				<li class="muted">Choose where your advertention will be shown</li>
				<li class="muted">Pick a date</li>
				<li class="muted">Ready to go!</li>
			</ol>
		</article>
        @include('includes.anonymous')
    @stop
@endif

@section('scripts')
	<script type="text/javascript">
		jQuery(document).ready(function(){
		    jQuery(".scroll").click(function(event){
		        //prevent the default action for the click event
		        event.preventDefault();

		        //get the full url - like mysitecom/index.htm#home
		        var full_url = this.href;

		        //split the url by # and get the anchor target name - home in mysitecom/index.htm#home
		        var parts = full_url.split("#");
		        var trgt = parts[1];

		        //get the top offset of the target anchor
		        var target_offset = $("#"+trgt).offset();
		        var target_top = target_offset.top;

		        //goto that anchor by setting the body scroll top to anchor top
		        $('html, body').animate({scrollTop:target_top}, 1500, 'easeInSine');
		    });

		  	jQuery('#datepicker').datepicker();

		  	jQuery('#datepicker2').datepicker();


		});
	</script>
@stop