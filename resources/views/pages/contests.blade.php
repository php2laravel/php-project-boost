@extends('app')

    @section('banner')
        <div class="banner banner-contest">
            <div class="banner-darken">
                @include('includes.header')
                <p class="banner-quote">A LITTLE CONTEST NEVER HURT NOBODY.</p>
                @if (Auth::check())
                <a class="btn btn-big  banner-registerbtn" href="applycontest">JOIN THE FUN!</a>
                @else
                <a class="btn btn-default  banner-registerbtn" href="auth/register">Sign up (it's free!)</a>
                @endif
            </div>
        </div>
        @include('includes.nav')
    @stop

        @section('message')
            @if (Session::has('flash_notification.message'))
                <p class="alert alert-admin alert-{{ Session::get('flash_notification.level') }} alert-top">
                 <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                {{ Session::get('flash_notification.message') }}
                </p>
            @endif 
        @stop
	@section('content')
		<div class="contest">
            <h1>Competition of the month</h1>
            <br>
            <p>Vote for your favourite competitors.
                <br> The one with the most votes, wins the contest!
                <br> The winner will be announced at the beginning of the nex month.
            </p>
            <div class="content-spotlight">
                @foreach($lastcontest as $l)
                    <h3 class="section-title">{{ $l->title}}</h3>
                    <div class="section-line"></div>

                    <p class="contest-explain">
                        <b>Challenge: </b> 
                        {{ $l->desc}}
                        <br>
                        <b>Deadline: </b>
                         <?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($l->enddate))->diffForHumans() ?> 
                         ({{ \Carbon\Carbon::createFromTimeStamp(strtotime($l->enddate))->format('d/m/Y') }})
                    </p>
                @endforeach
            <div>
		<a href="applycontest" class="btn btn-big">Join the contest!</a>
        </div>
        <div class="content-spotlight"

            <br class="clear"/>
            @foreach($submissionsnow as $work)
                <div class="content-spotlight-item">
                    <a class="content-spotlight-link" href="/contest/{!! $work->id !!}">
                        <img class="content-spotlight-item_image" src="{{URL::to('/')}}/uploads/projects/{!! $work->image !!}" alt="imageproject">
                        <div class="content-spotlight-item_info">
                            <p class="content-spotlight-item_info-namedesign">{!! $work->title !!}</p>
                    </a>
                    <?php $designerName = strtolower(str_replace(' ', '-',  $work->name)); ?>
                    <a class="content-spotlight-link" href="/contest/{!! $work->id !!}">
                        <p class="content-spotlight-item_info-author">{!! $work->name !!}</p>
                    </a>
                    <span class="comment-date"><?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($work->created_at))->toFormattedDateString() ?></span>
                            <ul class="content-spotlight-item_info-tools">
                                <li class="cmmnt">
                                    <img src="<?php echo asset('images/icons/comment.png')?>" alt="heart icon" class="icon-small">
                                    <span> {!! $work->countcomments !!} </span>
                                </li>
                                <li class="fav">
                                    <img src="<?php echo asset('images/icons/heart.png')?>" alt="heart icon" class="icon-small">
                                    <span> {!! $work->countlikes !!} </span>
                                </li>
                                <br class="clearfix"/>
                            </ul>
                        </div>
                </div>
            @endforeach
            <br>
        {!! $submissionsnow->render() !!}
        <div class="contests-gallery">
            <h1>Previous challenges</h1>
            <br class="clear"></br>
            @foreach($contests as $c)
                <h3 class="section-title">{{$c->title}}</h3>
                <div class="section-line"></div>
                <p><b>Ended </b><?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($c->enddate))->diffForHumans() ?></p>
                <p class="contest-explain"><b>Challenge: </b>{{$c->desc}}</p>
                
                @foreach($submissionsprevious as $work)
                    @if ($work->competition_id == $c->id)
                        <div class="content-spotlight-item">
                            <a class="content-spotlight-link" href="/contest/{!! $work->id !!}">
                                <img class="content-spotlight-item_image" src="{{URL::to('/')}}/uploads/projects/{!! $work->image !!}" alt="imageproject">
                                <div class="content-spotlight-item_info">
                                    <p class="content-spotlight-item_info-namedesign">{!! $work->title !!}</p>
                            </a>
                            <?php $designerName = strtolower(str_replace(' ', '-',  $work->name)); ?>
                            <a class="content-spotlight-link" href="/contest/{!! $work->id !!}">
                                <p class="content-spotlight-item_info-author">{!! $work->name !!}</p>
                            </a>
                            <span class="comment-date"><?php echo \Carbon\Carbon::createFromTimeStamp(strtotime($work->created_at))->toFormattedDateString() ?></span>
                                    <ul class="content-spotlight-item_info-tools">
                                        <li class="cmmnt">
                                            <img src="<?php echo asset('images/icons/comment.png')?>" alt="heart icon" class="icon-small">
                                            <span> {!! $work->countcomments !!} </span>
                                        </li>
                                        <li class="fav">
                                            <img src="<?php echo asset('images/icons/heart.png')?>" alt="heart icon" class="icon-small">
                                            <span> {!! $work->countlikes !!} </span>
                                        </li>
                                        <br class="clearfix"/>
                                    </ul>
                                </div>
                        </div>
                     @endif
                @endforeach
                <br class="clear"></br>
                 {!! $submissionsprevious->render() !!}
                <br class="clear"></br>
                
            @endforeach
        
            <br class="clear"></br>
        {!! $contests->render() !!}
        </div>

	@stop

