@extends('app')
<?php 
    if (Auth::check()) {
        $user = Auth::user();
    }
?>
@section('content')
<h1>Edit project</h1>
<div class="project-info_edit">
     <div class="project-info_form">
        {!! Form::model($project, ['method' => 'POST', 'url' => ['projects/edit', $project->id], 'files' => true, 'class'=>'edit-project_form' ]) !!}
            <br>
                {!! Form::label('title', 'Title:', ['class' => 'edit-project_form-label'])!!}
                {!! Form::text('title', null, ['class' => 'form-control edit-project_form-input'])!!}

                {!! Form::label('desc', 'Description:', ['class' => 'edit-project_form-label'])!!}
                {!! Form::textarea('desc', null, ['class' => 'form-control edit-project_form-input'])!!}

                {!! Form::label('tag_list', 'Tags:')!!}
                {!! Form::select('tag_list[]', $tags,  null, ['class' => 'form-control selecttag', 'multiple', 'id' => 'tag_list'])!!}

            {!! Form::label('image', 'Choose a new image', ['class' => 'edit-project_form-label']) !!}
            {!! form::file('image', null, ['class' => 'form-control'])!!}

            {!! Form::label('preview', 'Preview:', ['class' => 'create-contest_form-label']) !!}
            @if ($project->image != "")
                <img id="preview-old" src="{{URL::to('/')}}/uploads/projects/{{ $project->image }}" alt="preview image" class="create-contest_form-preview -old">
            @endif
            
            <div id="preview" class="create-project_form-preview"></div>
            <br/>
            
            <br class="clear" />
            {!! Form::submit('Save changes', ["class" => 'btn form btn-big btn-center profile-info_edit-btn']) !!}
        {!! Form::close() !!}
        <a href="{{URL::previous()}}" class="subtle-link center">Go back without saving</a>
    <br class="clear" />
    </div>
</div>

@stop

@section('scripts')
    <script>
        jQuery(document).ready(function() {
            jQuery("select.selecttag").select2();
            $('#preview').hide();
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    console.log("changies!");

                    reader.onload = function (e) {
                        $('#preview-old').slideUp();
                        $('#preview').slideDown();
                        $('#preview').css('background', 'transparent url('+e.target.result +') center center no-repeat');
                         $('#preview').css('background-size', 'contain');     
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#image").change(function(){
                readURL(this);
            });
        });
    </script>
@stop
