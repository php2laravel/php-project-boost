@extends('app')

@section('banner')
    <div class="banner banner-api">
        <div class="banner-darken">
            @include('includes.header')
            <p class="banner-quote">AN API TO BOOST YOUR PROJECT.</p>
            @unless (Auth::check())
            <a class="btn btn-default  banner-registerbtn" href="auth/register">Sign up (it's free!)</a>
            @endunless
        </div>
    </div>
    @include('includes.nav')
@stop

@section('content')
    <h3>API overview</h3>

    <div class="explain">

        <p class="steps">1. POPULAR PROJECTS</p>

            <p class="des">This endpoint returns all popular projects.<br>
                You can change the amount per page like this: '&perpage=10'.
            </p>

            <p class="url">http://boost.weareimd.be/api/v1/items/popular?page=1&perpage=10</p>

        <p class="steps">2. PER PROJECT</p>

            <p class="des">This endpoint returns details about one project item.<br>
            </p>


            <p class="url">http://boost.weareimd.be/api/v1/projects/1</p>
        </p>

    </div>



        <br>

    
@stop       