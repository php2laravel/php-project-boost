@extends('app')
    @section('content')

    <h1>Create a new project</h1>

    @if ($errors->any())

        <ul class="alert alert-danger list-unstyled">
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>

    @endif

        {!! Form::open(array('url' => 'projects', 'files' => true, 'class'=>'create-project_form')) !!}
            {!! Form::label('title', 'Title:', ['class' => 'create-project_form-label']) !!}
            {!! form::text('title', null, ['class' => 'form-control create-project_form-input', 'autofocus']) !!}

            {!! Form::label('desc', 'Description: ', ['class' => 'create-project_form-label']) !!}
            {!! form::textarea('desc', null, ['class' => 'form-control create-project_form-input']) !!}
            
            {!! Form::label('tag_list', 'Tags:', ['class' => 'create-project_form-label'])!!}
            {!! Form::select('tag_list[]', $tags,  null, ['class' => 'form-control selecttag', 'multiple', 'id' => 'tag_list'])!!}

            

            {!! Form::label('image', 'Add an image (Please use 800x800 for the best effect.)', ['class' => 'create-project_form-label']) !!}
            {!! form::file('image', null, ['class' => 'form-control'])!!}
            {!! Form::label('preview', 'Preview:', ['class' => 'create-project_form-label']) !!}
            <div id="preview" class="create-project_form-preview"></div>
            <br/>

            {!! Form::Submit('Add Project', ['class' => 'form-inline btn btn-big btn-center']) !!}

        {!! Form::close() !!}

    @stop

@section('scripts')
    <script>
        jQuery(document).ready(function() {
            jQuery("select.selecttag").select2();
        
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    console.log("changies!");

                    reader.onload = function (e) {
                        $('#preview').css('background', 'transparent url('+e.target.result +') center center no-repeat');
                         $('#preview').css('background-size', 'auto 100%');
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#image").change(function(){
                readURL(this);
            });
        });
    </script>
@stop