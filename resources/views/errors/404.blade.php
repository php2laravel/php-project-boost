@extends('app')
<?PHP
    if (Auth::check()) {
        // The user is logged in...
        $user = Auth::user();
    }
?>
@section('banner')
    <div class="banner">
        <div class="banner-darken">
            @include('includes.header')
            <p class="banner-quote">OOPS. This page doesn't exist</p>
            <button class="btn btn-big btn-center"><a style="color: white" href="/">Go Back</a></button>
        </div>
    </div>
    @include('includes.nav')
@stop

@section('newsletter')
    @include('includes.newsletter')
@stop