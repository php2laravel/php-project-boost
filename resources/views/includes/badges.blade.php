<div class="container-show-badges">
	<ul class="profile-info-badges">
		@if ($badges != null)
			@foreach ($badges as $badge)

				<h3>Badges</h3>

				<p>Besides gaining experience with your posts and feedback, you receive badges for being especially helpful and for being involved in the Boost community.
					You will earn more of the secret badges by continuing to use the Boost platform.</p>

				<li class="profile-info-badges_badge">
					<img src="{{ $badge->image }}" alt="{{ $badge->name . "icon" }}" class="profile-info-badges_img">
					<span class="profile-info-badges_info">{{ $badge->name }}</span>
				</li>
			@endforeach
		@else
			@if (!isset($designer))
				<p class="text-center">No badges available.</p>
				<p class="text-center">Why don't you <a href="projects/create">upload</a> some work to earn your first one? When you earn more badges and gain more experience, your work will get featured on the homepage.</p>
			@else 
				<p class="text-center">{{$designer->name}} has no badges at the moment.</p>
			@endif
		@endif
	</ul>
</div>