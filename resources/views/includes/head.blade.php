<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="csrf-token" 		  content="{{ csrf_token() }}" />
	<meta property="og:url"           content="http://boost.weareimd.be" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="boost" />
    <meta property="og:description"   content="Online design platform" />
    <meta property="og:image"         content="<?php echo asset('images/logos/logo.png')?>" />
    <meta name="viewport" 			  content="width=device-width, initial-scale=1.0">
	
	<title>Boost</title>

	<link rel="shortcut icon" href="<?php echo asset('favicon.ico')?>" type="image/x-icon">
	<link rel="icon" href="<?php echo asset('favicon.ico')?>" type="image/x-icon">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo asset('css/datepicker.css')?>" type="text/css">
	<link rel="stylesheet" href="<?php echo asset('sass/modules/fonts/webfontkit/stylesheet.css')?>" type="text/css">
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet" integrity="sha256-MfvZlkHCEqatNoGiOXveE8FIwMzZg4W85qfrfIFBfYc= sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.css">
	<link rel="stylesheet" href="<?php echo asset('css/style.css')?>" type="text/css">
	
	<script src="https://code.jquery.com/jquery-2.1.4.js"></script>
	<script src="<?php echo asset('js/jquery.konami.js')?>"></script>
	<script src="<?php echo asset('js/bootstrap-datepicker.js')?>"></script>
	<script src="https://cdn.jsdelivr.net/select2/4.0.1-rc.1/js/select2.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
	<script src="<?php echo asset('js/base.js')?>"></script>
</head>