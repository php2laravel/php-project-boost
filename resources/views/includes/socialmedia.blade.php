<section class="socialcontainer">
    <div class="socialcontainer-darken">
        <h3 class="socialmedia-title">Connect Boost with the networks you love</h3>
        <ul class="socialmedia-icons">
            <li class="instagram"><a href="https://www.instagram.com/boost.app/" class="icon instagram"></a></li>
            <li class="twitter"><a href="https://twitter.com/IMDBoost" class="icon twitter"></a></li>
            <li class="fb"><a href="https://www.facebook.com/Boost-442777632584715/" class="icon facebook"></a></li>
            <br class="clearfix"/>
        </ul>
    </div>
</section>