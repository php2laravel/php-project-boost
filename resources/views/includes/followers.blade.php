<div class="container-show-followers">
	<div class="followers-container">
		@if(count($followers) === 0)
			@if (!isset($designer))
				<img src="<?php echo asset('images/icons/iris.png')?>" alt="heart icon" class="icon-small">
				<span>You haven't got any followers yet. If you {!!link_to('../projects/create', 'add more projects', ['class'=>'gallery-link']);!!} to your portfolio, more students will notice you. Good luck!</span>
			@else
				<img src="<?php echo asset('images/icons/iris.png')?>" alt="heart icon" class="icon-small">
				<span>Be the first to follow {{$designer->name}}.</span>
			@endif
		@elseif(count($followers) >= 1)
			@foreach ($followers as $f)
			    <?php 
			        $name = $f->name;
			        $name = preg_replace('/\s+/', '-', $name);

			    ?>
			        <div class="content-spotlight-item profiles-tile">
			            <a href="{{ action('DesignerController@profile', [$name]) }}">
			                @if ($f->image != "")
			                    <img src="{{URL::to('/')}}/uploads/users/{{ $f->image }}" class="content-spotlight-item_image">
			                @else
			                    <img src="http://jbryner.me/_images/placeholder_small.gif" alt="placeholder user picture" class="content-spotlight-item_image">
			                @endif
			                <div class="content-spotlight-item_info">
			                    <p class="content-spotlight-item_info-namedesign">{{ $f->name }}</p>
				        </a>
				                <p class="content-spotlight-item_info-author">{!! $f->functie !!}</p>
			                </div>
		        </div>
		    @endforeach
		@endif
	</div>
</div>