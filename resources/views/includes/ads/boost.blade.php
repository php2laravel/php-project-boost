<div class="ads-container">
	@foreach($a as $ad)
		<a href="/{!! $ad->id !!}" title="" class="advts-link" target="_blank">
			<div class="adv-container-home_item" style="background-image:url('{{URL::to('/')}}/uploads/a/{!! $ad->image !!}');">
				<h4>{{$ad->companyname}}</h4>
			</div>
		</a>
	@endforeach
</div>