<div class="banner">
	<div class="banner-darken">
		@include('includes.header')
	    <p class="banner-quote">IT'S ABOUT PROGRESS, NOT PERFECTION.</p>
	    <p class="banner-underquote">Create an account. | Upload your work. | Get feedback from your peers. | Get better each day!</p>
	    @unless (Auth::check())
	    <a class="btn btn-default  banner-registerbtn" href="auth/register">Sign up (it's free!)</a>
	    @endunless
	</div>
</div>
@include('includes.nav')