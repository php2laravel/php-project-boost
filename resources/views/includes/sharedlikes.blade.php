@if(isset($sharedLikes) && count($sharedLikes) > 0)
	<p>You and {{$designer->name}} both favourited</p>
	<div class="sharedlikes-container">
		@foreach($sharedLikes as $work)

			<div class="content-spotlight-item">
	            <a class="content-spotlight-link" href="/projects/{!! $work->id !!}">
	                <img class="content-spotlight-item_image" src="{{URL::to('/')}}/uploads/projects/{!! $work->image !!}" alt="imageproject">
	                <div class="content-spotlight-item_info">
	                    <p class="content-spotlight-item_info-namedesign">{!! $work->title !!}</p>
	            </a>
	            <?php $designerName = strtolower(str_replace(' ', '-',  $work->name)); ?>
	            <a class="content-spotlight-link" href="/<?php echo $designerName ?>">
	                <p class="content-spotlight-item_info-author">{!! $work->name !!}</p>
	            </a>
	                    <ul class="content-spotlight-item_info-tools">
                        <li class="cmmnt">
                            <img src="<?php echo asset('images/icons/comment.png')?>" alt="heart icon" class="icon-small">
                            <span> {!! $work->countcomments !!} </span>
                        </li>
                        <li class="fav">
                            <img src="<?php echo asset('images/icons/vote.png')?>" alt="heart icon" class="icon-small">
                            <span> {!! $work->countlikes !!} </span>
                        </li>
                        <br class="clearfix"/>
                    </ul>
	                </div>
	        </div>
	    @endforeach
	</div>
@endif