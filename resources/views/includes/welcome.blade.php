<div class="welcome-block">
    <h1 class="welcome-title">This is <span class="welcome-logo">Boost</span></h1>
    <p class="welcome-msg">
    <br>
    <b>It's about progress, not perfection.
    </b>
    <br>
Create an account. | Upload your work. | Get feedback from your peers. | Get better each day!</p>
    <br class="clear"/>
    <div class="welcome-btns">
        <div class="welcome-btns-left">
            <a class="btn-thin welcome-btn" href="auth/login">Log in</a>
            <br>
            <p><a class="subtle-link welcome-reset" href="password/email">reset password</a></p>
        </div>
        <div class="welcome-btns-right">
         <span class="welcome-span">or</span> <a class="welcome-btn btn-thin" href="auth/register">Sign up</a>
        </div>
    </div>
    <br class="clear"/>
</div>