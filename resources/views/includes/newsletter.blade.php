<section class="newsletter">

	<!-- Begin MailChimp Signup Form -->
	<h4 class="newsletter-title">Do you want to stay up-to-date? Subscribe for our newsletter!</h4>

	<div id="mc_embed_signup">
	<form action="//jorenpolfliet.us9.list-manage.com/subscribe/post?u=7766a8e8c4829477634898501&amp;id=84d704484c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="form-inline newsletter_form" target="_blank" novalidate>
	   	<div id="mc_embed_signup_scroll">
			<div class="mc-field-group">
		    	<input placeholder='email address' type="email" value="" name="EMAIL" class="form-inline newsletter_form-input form-control" id="mce-EMAIL">
		    	<input type="submit" value="Subscribe now" name="subscribe" id="mc-embedded-subscribe" class="btn-default btn-big">
		    	<br>
			</div>
		</div>
	    <div id="mce-responses" class="">
	        <div class="response" id="mce-error-response" style="display:none"></div>
	        <div class="response" id="mce-success-response" style="display:none"></div>
	    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
	   <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_7766a8e8c4829477634898501_84d704484c" tabindex="-1" value=""></div>
	   
	   </div>
	</form>
	</div>
	<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';fnames[2]='LNAME';ftypes[2]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
	<br class="clear"></br>
</section>