<div class="container-show-favourites">
	<div class="favourites-container">
		@if(count($likes) === 0)
			@if (!isset($designer))
				<img src="<?php echo asset('images/icons/vote.png')?>" alt="heart icon" class="icon-small">
				<span>You haven't yet liked a project. You can go to the {!!link_to('../projects', 'gallery', ['class'=>'gallery-link']);!!} to give a like to the projects you find interesting!</span>
			@else 
				<img src="<?php echo asset('images/icons/vote.png')?>" alt="heart icon" class="icon-small">
				<span>{{$designer->name}} has no favourited projects.</span>
			@endif
		@elseif (count($likes) >= 1)
			<ul class="favourites">
				@foreach($likes as $work)
			        <div class="content-spotlight-item">
			            <a class="content-spotlight-link" href="/projects/{!! $work->id !!}">
			                <img class="content-spotlight-item_image" src="{{URL::to('/')}}/uploads/projects/{!! $work->image !!}" alt="imageproject">
			                <div class="content-spotlight-item_info">
			                    <p class="content-spotlight-item_info-namedesign">{!! $work->title !!}</p>
			            </a>
			            <?php $designerName = strtolower(str_replace(' ', '-',  $work->name)); ?>
			            <a class="content-spotlight-link" href="/<?php echo $designerName ?>">
			                <p class="content-spotlight-item_info-author">{!! $work->name !!}</p>
			            </a>
			                    <ul class="content-spotlight-item_info-tools">
	                            <li class="cmmnt">
	                                <img src="<?php echo asset('images/icons/comment.png')?>" alt="heart icon" class="icon-small">
	                                <span> {!! $work->countcomments !!} </span>
	                            </li>
	                            <li class="fav">
	                                <img src="<?php echo asset('images/icons/vote.png')?>" alt="heart icon" class="icon-small">
	                                <span> {!! $work->countlikes !!} </span>
	                            </li>
	                            <br class="clearfix"/>
	                        </ul>
			                </div>
			        </div>
			    @endforeach
				@if ($totallikes > 5)
					<!--
					<a href="#" class="favourites-item_next"> > </a>
					-->
				@endif
			</ul>
		@endif
	</div>
</div>