<div class="container-show-follows">
	<div class="followings-container">
		@if(count($follows) === 0)
			@if (!isset($designer))
				<img src="<?php echo asset('images/icons/iris.png')?>" alt="heart icon" class="icon-small">
				<span>You aren't following anyone now. You can go to the {!!link_to('../designers', 'designers gallery', ['class'=>'gallery-link']);!!} to follow the profiles you find interesting!</span>
			@else
				<img src="<?php echo asset('images/icons/iris.png')?>" alt="heart icon" class="icon-small">
				<span>{{$designer->name}} isn't following another profile.</span>
			@endif
		@elseif(count($follows) >= 1)
			@foreach ($follows as $d)
			    <?php 
			        $name = $d->name;
			        $name = preg_replace('/\s+/', '-', $name);

			    ?>
			        <div class="content-spotlight-item profiles-tile">
			            <a href="{{ action('DesignerController@profile', [$name]) }}">
			                @if ($d->image != "")
			                    <img src="{{URL::to('/')}}/uploads/users/{{ $d->image }}" class="content-spotlight-item_image">
			                @else
			                    <img src="http://jbryner.me/_images/placeholder_small.gif" alt="placeholder user picture" class="content-spotlight-item_image">
			                @endif
			                <div class="content-spotlight-item_info">
			                    <p class="content-spotlight-item_info-namedesign">{{ $d->name }}</p>
				        </a>
				                <p class="content-spotlight-item_info-author">{!! $d->functie !!}</p>
			                </div>
		        </div>
		    @endforeach
		@endif
	</div>
</div>