<footer class="footer">
	<div class="container">
		<p class="footer-credits"><span class="footer-logo">Boost </span><span class="footer-credits_names"> | &nbsp; Jens - Joren - Niels - Thais - Veerle</span></p>
	</div>
</footer>
<script type="text/javascript">
	jQuery.ajaxSetup({
	   headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content') }
	});
</script>
<script>
	$.konami(function() {
		$(document).unbind('keydown',arguments.callee);
		$.getScript('http://www.cornify.com/js/cornify.js',function(){
			cornify_add();
			$(document).keydown(cornify_add);
		});
	});
</script>
