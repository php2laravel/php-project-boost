<div class="container-show-activity">
    <h2>Recent Activity</h2>

    <div class="likes-overview">
        <h4>
            Likes ({{count($activityLikes)}})
        </h4>
         @if (count($activityComments) ==0)
            @if (!isset($designer))
                You didn't recently give feedback to another designer.
            @else
                {{ $designer->name }} didn't recently give feedback to another designer.
            @endif 
        @else 
            @foreach ($activityLikes as $like)
        		<div class="row">
                    <div class="col-md-4">
                        <img src="<?php echo asset('images/icons/voted.png')?>" alt="heart icon" class="icon-small">
                        {{ \Carbon\Carbon::createFromTimeStamp(strtotime($like->created_at))->diffForHumans() }}
                    </div>
                    <div class="col-md-8">
                        @if (!isset($designer))
                            You liked the project <a href="{{ action('ProjectsController@show', [$like->projectId]) }}">{{ $like->title }}</a>
                        @else
                            {{ $designer->name }} liked the project <a href="{{ action('ProjectsController@show', [$like->projectId]) }}">{{ $like->title }}</a>
                        @endif
                    </div>
        		</div>
            @endforeach
            {!! $activityLikes->render() !!}
        @endif
    </div>
    <div class="comments-overview">
        <h4>
            Comments ({{count($activityComments)}})
        </h4>
        @if (count($activityComments) ==0)
            @if (!isset($designer))
                You haven't recently liked a project.
            @else
                {{ $designer->name }} hasn't recently liked a project.
            @endif 
        @else 
            @foreach ($activityComments as $comment)
                <div class="row">
                    <div class="col-md-4">
                        <img src="<?php echo asset('images/icons/comment.png')?>" alt="heart icon" class="icon-small">
                        {{ \Carbon\Carbon::createFromTimeStamp(strtotime($comment->created_at))->diffForHumans() }}
                    </div>
                    <div class="col-md-8">
                        @if (!isset($designer))
                            You commented on the project <a href="{{ action('ProjectsController@show', [$comment->project_id]) }}">{{ $comment->title }}</a>: <span class="muted"> {{ $comment->body}}</span>
                        @else 
                            {{ $designer->name }} commented on the project <a href="{{ action('ProjectsController@show', [$comment->project_id]) }}">{{ $comment->title }}</a>: <span class="muted"> {{ $comment->body}}</span>
                        @endif
                    </div>
                </div>
            @endforeach
            {!! $activityComments->render() !!}
        @endif
    </div>
</div>