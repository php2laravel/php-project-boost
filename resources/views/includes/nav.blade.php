<nav>
	<ul class="nav nav-tabs nav-toggle-me nav-justified">
		<li role="presentation" class="nav-item">
				<a href="#" title="Menu" class='nav-item-link nav-toggle-icon'>Menu</a>
		</li>
	</ul>
	<ul class="nav nav-tabs nav-justified nav-toggle">
		<li role="presentation" class="nav-item">
			{!!
				link_to('../', 'Home', ['class' => 'nav-item-link'])
			!!}
		</li>
		<li role="presentation" class="nav-item">
			{!!
				link_to('../projects', 'Boosts', ['class' => 'nav-item-link'])
			!!}
		</li>
		<li role="presentation" class="nav-item">
			{!!
				link_to('../designers', 'Designers', ['class' => 'nav-item-link'])
			!!}
		</li>
		<li role="presentation" class="nav-item">
			{!!
				link_to('./contests', 'Contests', ['class' => 'nav-item-link'])
			!!}
		</li>
		<li role="presentation" class="nav-item">
			{!!
				link_to('../advertise', 'Advertise', ['class' => 'nav-item-link'])
			!!}
		</li>
		<li role="presentation" class="nav-item">
			{!!
				link_to('../apipage', 'Api', ['class' => 'nav-item-link'])
			!!}
		</li>
	</ul>
</nav>

<script type="text/javascript">
jQuery(document).ready(function($) {
	jQuery(".nav-toggle-icon").click(function() {
		event.preventDefault();
		jQuery(".nav-toggle").slideToggle()();
});;
	
});
</script>