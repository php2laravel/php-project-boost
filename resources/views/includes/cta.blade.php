<div class="content-spotlight-section cta-btns">
	<a href="/projects/create" class="btn-cta btn btn-big">Add new project</a>
    <span class="spacebetween"></span>
    <a href="/contests" class="btn-cta btn btn-big">Join a contest</a>
    <br class="clear"/>
</div>