<?php 
	if (Auth::check()) {
	    $user = Auth::user();
	}
	if (!isset($notificationLikes)) {
		$notificationLikes = 0;
	}
	if (!isset($notificationComments)) {
		$notificationComments = 0;
	}
?>
<div class="header">
	{!!
		link_to('../', 'Boost', ['class'=>'header-logo'])
	!!}
	<ul class="header-menu">
		@if (Auth::check())
			<li class="header-menu-item header-menu_form-toggle">
				{!! Form::open( array('class'=>'form-inline header-menu_form', 'role' => 'search', 'id'=>'search-form', 'method'=>'GET',  'url' => 'search')) !!}
					<div class="input-group header-menu_form-search">
						<span class="input-group-btn">
					        <button type="submit" class="btn header-menu_form-addon" type="button">
					        	<img src="<?php echo asset('images/icons/search-white.png')?>" alt="search icon" class="header-menu_icon">
					        </button>
					    </span>
					    {!! form::text('search',null, ['required', 'class' => 'form-control header-menu_form-input', 'placeholder' => 'search for...', 'id'=>'search-field']) !!}
	                </div>
	                <br class="clear"/>
	            {!! Form::close() !!}
			</li>

			<li class="header-menu-item header-menu_form-toggle-icon">
				<img src="<?php echo asset('images/icons/search-white.png')?>" alt="search icon" class="header-menu_icon">
			</li>
		@endif

		<li class="header-menu-item">
			@if (Auth::check())
			<div class="header-menu_my-profile">
				<a href="../my-profile" class="header-menu_my-profile_link">
					@if ($user->image != "")
						<img src="{{URL::to('/')}}/uploads/users/thumbnails/{{ $user->image }}" class="profile-picture-thumbnail">
					@else
						<img src="http://jbryner.me/_images/placeholder_small.gif" alt="placeholder user picture" class="profile-picture-thumbnail">
					@endif
					<span>
						Hi {{$user->name}} 
					<!--
					</a>
						@if (count($notificationLikes) + count($notificationComments) != 0)
							<a href="../notifications" class="notifications-count-header">({{count($notificationLikes) + count($notificationComments)}})</a>
						@endif
					<a href="../my-profile" class="header-menu_my-profile_link">
					-->
					</span> 
					<span> </span>
				</a> | 
				{!!
					link_to_route('logout', 'Logout', '', ["class" => "header-menu_my-profile_logout"])
				!!} 
			</div>
			@else
			<div class="header-menu_auth">
				{!!
					link_to_route('register', 'Register', '', ["class" => "header-menu_auth-link"])
				!!} 
				<span> | </span> 
				{!!
					link_to_route('login', 'Login', '', ["class" => "header-menu_auth-link"])
				!!}
			</div>
			@endif			
		</li>
	</ul>
</div>
<br class="clear"/>

<script type="text/javascript">
jQuery(document).ready(function($) {
	jQuery(".header-menu_form-toggle-icon").click(function() {
		jQuery(".header-menu_form-toggle").slideToggle()();
	});;
	
});
</script>