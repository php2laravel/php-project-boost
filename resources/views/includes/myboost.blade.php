<?php 
	if (isset($designer)) 
	{
		$u = $designer;
	} else {
		$u = $user;
	}
?>
<div class="container-show-myboosts">
	@if(count($u->projects) < 1)
		@if ($u == $user)
			<p>Make your first boost, it's easy!</p>
			<a href='/projects/create' class='btn btn-default'>Create a new project</a>
		@else
			<p>{{$u->name}} doens't have any projects yet.</p>
		@endif
	@else
		<ul class='myboost'>
		    @foreach ($u->projects as $project)
		    	<div class='content-spotlight-item'>
				    @if ($project->competition)
		        		<a href='../contest/{{ $project->id }}'>
		        	@else 
		        		<a href='../projects/{{ $project->id }}'>
		        	@endif
				    <img class='content-spotlight-item_image' src='{{URL::to('/')}}/uploads/projects/{!! $project->image !!}' alt='imageproject'>
				    <div class='content-spotlight-item_info'>
				        <p class='content-spotlight-item_info-namedesign myboost_title'>{!! $project->title !!}</p>
				        </a>
			                <ul class='content-spotlight-item_info-tools'>
		                        <li class='cmmnt'>
		                            <img src='<?php echo asset('images/icons/comment.png')?>' alt='heart icon' class='icon-small'>
		                            <span> {!! $project->countcomments !!} </span>
		                        </li>
		                        <li class='fav'>
		                            <img src='<?php echo asset('images/icons/vote.png')?>' alt='heart icon' class='icon-small'>
		                            <span> {!! $project->countlikes !!} </span>
		                        </li>
		                        @if ($u->id === Auth::user()->id)
			                        <li>
								    	<a href='../projects/edit/{{ $project->id }}' class='btn btn-adm btn-adm-small btn-adm-edit'>edit</a>
								    </li>
								    <li>
								    	<button class='btn btn-adm btn-adm-small btn-adm-delete' type='button' data-toggle='collapse' data-target='#collapseDeleteForm-{{$project->id}}' aria-expanded='false' aria-controls='collapseExample'>
				                      delete
				                    	</button>
				                    	<br class='clear'></br>
								    </li>    
				                    </ul>
						                <div class='collapse collapse-delete-form' id='collapseDeleteForm-{{$project->id}}'>
						                  <p>Are you sure you want to delete this project?</p>
						                    <a href='../projects/delete/{{ $project->id }}' class='btn btn-small btn-center btn-delete-confirm'>Yes, delete it.</a>
						                    <span class='spacebetween'></span>
						                    <button  class='btn btn-default btn-center btn-primary' data-toggle='collapse' data-target='#collapseDeleteForm-{{$project->id}}' aria-controls='collapseExample'>No</button>
						                </div>
				            @else
				        		</ul>
				            @endif
		            </div>
				<br class='clear'/>
				</div>
		    @endforeach
		</ul>
	@endif
</div>