@extends('app')

@if (Auth::user()->admin == 1)

    @section('content')
        <h1>Create a new tag</h1>
        @if ($errors->any())
            <ul class="alert alert-danger list-unstyled">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        <h3>Taglist</h3>
        <ul>
            @foreach ($taglist as $t)
                <li>{{$t->name}}</li>
            @endforeach
        </ul>
        
        {!! Form::open(array('url' => 'admin/new-tag', 'class'=>'create-tag_form')) !!}
            {!! Form::label('name', 'New tag:', ['class' => 'create-tag_form-label']) !!}
            {!! form::text('name', null, ['class' => 'form-control create-tag_form-input']) !!}

            <br class="clear"/>
            {!! Form::Submit('Add Tag', ['class' => 'form-inline btn btn-big btn-center']) !!}

        {!! Form::close() !!}
        <a href="{{URL::previous()}}" class="subtle-link center">Go back without saving</a>
        <br class="clear"/>
    @stop

@else
    @section('banner')
        <div class="banner banner-admin">
            <div class="banner-darken">
                @include('includes.header')
                <p class="banner-quote">404</p>
                <p class="banner-underquote">We're sorry dude, but you don't have access to the admin panel.</p>
                <a class="btn btn-big  banner-registerbtn" href="{{ URL::to('/') }}">Go back to safety</a>
            </div>
        </div>
        @include('includes.nav')
    @stop
    @section('content')
        @include('includes.newsletter')
    @stop
@endif
