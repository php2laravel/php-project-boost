@extends('app')

@if (Auth::user()->admin == 1)

    @section('banner')
        <div class="banner banner-admin">
            <div class="banner-darken">
                @include('includes.header')
                <p class="banner-quote">THIS IS BOOST.</p>
                <p class="banner-underquote">Welcome admin</p>
            </div>
        </div>
        @include('includes.nav')
    @stop

    @section('message')
        @if (Session::has('flash_notification.message'))
            <p class="alert alert-admin alert-{{ Session::get('flash_notification.level') }}">
             <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            {{ Session::get('flash_notification.message') }}
            </p>
            <br class="clear"/>
        @endif 
    @stop

@section('content')
<?php $designer=Auth::user(); ?>
    <div class="content-spotlight-section cta-btns">
        <a class="btn-cta btn btn-big" href="{{ URL::to('/admin/new-contest') }}">Create a new contest</a><span class="spacebetween"></span>
        <a class="btn-cta btn btn-big" href="{{ URL::to('/admin/new-tag') }}">Add a new tag</a>
        <br class="clear"/>
    </div>

    <h1>Take a look at the Boost stats</h1>
@stop

@section('second-nav')
    <nav class="second-nav">
        <ul class="second-navbar">
            <li class="second-navbar-item">
                <a href="#" title="" id="projects" class="second-navbar-item-link">
                    Projects
                        <ul class="adm-stats">
                            <li>
                                <i class="fa fa-picture-o"></i>
                                <span>{{ count($projects) }}</span>
                            </li>
                            <li>
                               <i class="fa fa-heart"></i>
                               <span>{{  count($likes) }}</span> 
                            </li>
                            <li>
                                <i class="fa fa-comments"></i>
                                <span>{{ count($comments) }}</span>
                            </li>
                        </ul>
                </a>
            </li>
            <li class="second-navbar-item">
                <a href="#" title="" id="contests" class="second-navbar-item-link">
                   Contests
                    <ul class="adm-stats">
                        <li>
                            <i class="fa fa-picture-o"></i>
                            <span>{{ count($contests) }}</span>
                        </li>
                        <li>
                            <i class="fa fa-picture-o"></i>
                            <span>{{ count($contests) }}</span>
                        </li>
                    </ul>
                </a>
            </li>
            <li class="second-navbar-item">
                <a href="#" title="" id="users" class="second-navbar-item-link">
                   Users  
                    <ul class="adm-stats">
                        <li>
                            <i class="fa fa-users"></i>
                            <span>{{ count($users) }}</span>
                        </li>
                    </ul>
                </a>
            </li>
            <li class="second-navbar-item">
                <a href="#" title="" id="ads" class="second-navbar-item-link">
                   Advertisements
                    <ul class="adm-stats">
                        <li>
                            <i class="fa fa-cart-plus"></i>
                            <span>{{ count($ads) }}</span>
                        </li>
                        <li>
                            <i class="fa fa-credit-card"></i>
                            <span>€ {{ $adsmoney }}</span>
                        </li>
                    </ul>
                </a>
            </li>
        </ul>
    </nav>
@stop

        
@section('second-content')
    <div class="sec-container">
        <main class="container">
            <section id="sec-content">
                <div class="admin-container">

                    <article class="adm-stats-details details-projects">

                        <div class="adm-list-projects">
                            <h4 class="show-projects">Projects ({{count($projects)}})</h4>
                            <ul class="adm-project-list list-projects">
                                @foreach ($projects as $p)
                                <?php $pshort = strtolower(str_replace(' ', '-',  $p->name)) ?>
                                <li class="adm-project">
                                    <ul class="adm-project-list">
                                        <li class="adm-project-list-item">
                                            <a href="../projects/{{ $p->id }}" title="go to project page  {{$p->title}}">{{$p->id}}</a>
                                        </li>
                                        <li class="adm-project-list-item">
                                            <a href="../projects/{{ $p->id }}" title="go to project page  {{$p->title}}">{{$p->title}}</a>
                                        </li>
                                        <li class="adm-project-list-item">
                                            <a href="../{{ $pshort }}" title="go to profile page {{$p->name}}">{{$p->name}}</a>
                                        </li>
                                        <li class="adm-project-list-item">
                                            <a href='../projects/edit/{{ $p->id }}' class='btn btn-adm btn-adm-small btn-adm-edit'>edit</a>
                                        </li>
                                        <li class="adm-project-list-item">
                                            <button class='btn btn-adm btn-adm-small btn-adm-delete' type='button' data-toggle='collapse' data-target='#collapseDeleteForm-{{$p->id}}' aria-expanded='false' aria-controls='collapseExample'>delete</button>
                                        </li>
                                    </ul>
                                       
                                    <div class='collapse collapse-delete-form' id='collapseDeleteForm-{{$p->id}}'>
                                      <p>Are you sure you want to delete this project?</p>
                                        <a href='../projects/delete/{{ $p->id }}' class='btn btn-small btn-center btn-delete-confirm'>Yes, delete it.</a>
                                        <span class='spacebetween'></span>
                                        <button  class='btn btn-default btn-center btn-primary' data-toggle='collapse' data-target='#collapseDeleteForm-{{$p->id}}' aria-controls='collapseExample'>No</button>
                                    </div>
                                    <br class="clear"/>
                                </li>
                                @endforeach
                                {!! $projects->render() !!}
                                <br class="clear"></br>
                            </ul>
                        </div>

                        <div class="adm-list-likes">
                            <h4 class="show-likes">Likes ({{count($likes)}})</h4>
                            <ul class="adm-project-list list-likes">
                                @foreach ($likes as $l)
                                <?php $lshort = strtolower(str_replace(' ', '-',  $l->name)) ?>
                                <li class="adm-project">
                                    <ul class="adm-project-list">
                                        <li class="adm-project-list-item-33">
                                            {{$l->created_at}}
                                        </li>
                                        <li class="adm-project-list-item-33">
                                            <a href="../{{ $lshort }}" title="go to profile page {{$l->name}}">{{$l->name}}</a>
                                        </li>
                                        <li class="adm-project-list-item-33">
                                            <a href="../contest/{{ $l->projectId }}" title="go to project page  {{$l->title}}">
                                                <img src="<?php echo asset('images/icons/heart.png')?>" alt="heart icon" class="icon-small">
                                                 {{$l->title}}
                                            </a>
                                        </li>
                                        <li class="adm-project-list-item">
                                            
                                        </li>
                                    </ul>
                                       
                                    <br class="clear"/>
                                </li>
                                @endforeach
                                {!! $likes->render() !!}
                                <br class="clear"></br>
                            </ul>
                        </div>    


                        <div class="adm-list-comments">
                            <h4 class="show-comments">Comments ({{count($comments)}})</h4>
                            <ul class="adm-project-list list-comments">
                                @foreach ($comments as $c)
                                <?php $cshort = strtolower(str_replace(' ', '-',  $c->name)) ?>
                                <li class="adm-project">
                                    <ul class="adm-project-list">
                                        <li class="adm-project-list-item-25">
                                            {{$c->created_at}}
                                        </li>
                                        <li class="adm-project-list-item-20">
                                            <a href="../{{ $cshort }}" title="go to profile page {{$c->name}}">{{$c->name}}</a>
                                        </li>
                                        <li class="adm-project-list-item-50">
                                            <a href="../contest/{{ $c->project_id }}" title="go to project page  {{$c->title}}">
                                                 {{$c->title}}
                                            </a>
                                            <span class="spacebetween"></span>
                                            <img src="<?php echo asset('images/icons/comment.png')?>" alt="comment icon" class="icon-small">
                                            <span class="spacebetween"></span>
                                            <span>"{{$c->body}}"</span>
                                        </li>
                                        <li class="adm-project-list-item-5">
                                            <button class='btn btn-adm btn-adm-small btn-adm-delete' type='button' data-toggle='collapse' data-target='#collapseDeleteForm-comment-{{$c->thisid}}' aria-expanded='false' aria-controls='collapseExample'>delete</button>
                                        </li>
                                    </ul>
                                    <br class="clear"/>
                                       
                                    <div class='collapse collapse-delete-form' id='collapseDeleteForm-comment-{{$c->thisid}}'>
                                      <p>Are you sure you want to delete this comment?</p>
                                        <a href='../comment/delete/{{$c->thisid}}' class='btn btn-small btn-center btn-delete-confirm'>Yes, delete it.</a>
                                        <span class='spacebetween'></span>
                                        <button  class='btn btn-default btn-center btn-primary' data-toggle='collapse' data-target='#collapseDeleteForm-comment-{{$c->thisid}}' aria-controls='collapseExample'>No</button>
                                    </div>
                                    <br class="clear"/>

                                </li>
                                @endforeach
                                {!! $comments->render() !!}
                                <br class="clear"></br>
                            </ul>
                        </div>
                        <br class="clear"></br>    
                    </article>

                    <article class="adm-stats-container stats-contests">
                        <h4>Contests</h4>
                        <ul class="adm-stats">
                            <li>
                                <i class="fa fa-picture-o"></i>
                                <span>{{ count($contests) }}</span>
                            </li>
                            <li>
                                <i class="fa fa-picture-o"></i>
                                <span>{{ count($contests) }}</span>
                            </li>

                        </ul>
                    </article>

                    <article class="adm-stats-details details-contests">

                        <div class="adm-list-contests">

                            <h4 class="show-contests">Contest Challenges ({{count($challenges)}})</h4>
                            <ul class="adm-project-list list-contests">
                                @foreach ($challenges as $c)
                                <li class="adm-project">
                                    <ul class="adm-project-list">
                                        <li class="adm-project-list-item-5">
                                            {{$c->id}}
                                        </li>
                                        <li class="adm-project-list-item-20">
                                            {{$c->title}}
                                        </li>
                                        <li class="adm-project-list-item-33">
                                            {{$c->desc}}
                                        </li>
                                        <li class="adm-project-list-item-33">
                                            {{$c->startdate}}
                                            &#8594;
                                            {{$c->enddate}}
                                        </li>
                                    </ul>
                                <br class="clear"></br>
                                </li>
                                @endforeach
                                {!! $challenges->render() !!}
                                <br class="clear"></br>
                            </ul>       

                            <h4 class="show-contests">Contest Submissions ({{count($contests)}})</h4>
                            <ul class="adm-project-list list-contests">
                                @foreach ($contests as $c)
                                <?php $cshort = strtolower(str_replace(' ', '-',  $c->name)) ?>
                                <li class="adm-project">
                                    <ul class="adm-project-list">
                                        <li class="adm-project-list-item">
                                            <a href="../contest/{{ $c->id }}" title="go to contest page  {{$c->title}}">{{$c->id}}</a>
                                        </li>
                                        <li class="adm-project-list-item">
                                            <a href="../contest/{{ $c->id }}" title="go to project page  {{$c->title}}">{{$c->title}}</a>
                                        </li>
                                        <li class="adm-project-list-item">
                                            <a href="../{{ $cshort }}" title="go to profile page {{$c->name}}">{{$c->name}}</a>
                                        </li>
                                        <li class="adm-project-list-item">
                                            <a href='../projects/edit/{{ $c->id }}' class='btn btn-adm btn-adm-small btn-adm-edit'>edit</a>
                                        </li>
                                        <li class="adm-project-list-item">
                                            <button class='btn btn-adm btn-adm-small btn-adm-delete' type='button' data-toggle='collapse' data-target='#collapseDeleteForm-{{$c->id}}' aria-expanded='false' aria-controls='collapseExample'>delete</button>
                                        </li>
                                    </ul>
                                       
                                    <div class='collapse collapse-delete-form' id='collapseDeleteForm-{{$c->id}}'>
                                      <p>Are you sure you want to delete this project?</p>
                                        <a href='../projects/delete/{{ $c->id }}' class='btn btn-small btn-center btn-delete-confirm'>Yes, delete it.</a>
                                        <span class='spacebetween'></span>
                                        <button  class='btn btn-default btn-center btn-primary' data-toggle='collapse' data-target='#collapseDeleteForm-{{$c->id}}' aria-controls='collapseExample'>No</button>
                                    </div>
                                    <br class="clear"/>
                                </li>
                                @endforeach
                                {!! $contests->render() !!}
                                <br class="clear"></br>
                            </ul>   
                        </div>    
                    </article>

                     <article class="adm-stats-container stats-users">
                            <h4>Users</h4>  
                            <ul class="adm-stats">
                                <li>
                                    <i class="fa fa-users"></i>
                                    <span>{{ count($users) }}</span>
                                </li>
                            </ul>
                        </article>
                    <article class="adm-stats-details details-users">

                        <div class="adm-list-users">
                            <h4>Users ({{count($users)}})</h4>
                            <ul class="adm-project-list">
                                @foreach ($users as $u)
                                <?php $ushort = strtolower(str_replace(' ', '-',  $u->name)) ?>
                                <li class="adm-project">
                                    <ul class="adm-project-list">
                                        <li class="adm-project-list-item-5">
                                            <a href="../{{ $u->id }}" title="go to profile page  {{$p->title}}">{{$u->id}}</a>
                                        </li>
                                        <li class="adm-project-list-item-33">
                                             <a href="../{{ $u->id }}" title="go to profile page  {{$p->title}}">
                                                {{$u->name}}
                                                @if ($u->admin)
                                                     | Admin
                                                @endif
                                            </a>
                                        </li>
                                        <li class="adm-project-list-item-50">
                                            {{$u->email}}
                                        </li>
                                        <li class="adm-project-list-item-5">
                                            <button class='btn btn-adm btn-adm-small btn-adm-delete' type='button' data-toggle='collapse' data-target='#collapseDeleteForm-{{$ushort}}' aria-expanded='false' aria-controls='collapseExample'>delete</button>
                                        </li>
                                    </ul>
                                    <br class="clear"/> 
                                    <div class='collapse collapse-delete-form' id='collapseDeleteForm-{{$ushort}}'>
                                      <p>Are you sure you want to delete this user?</p>
                                        <a href='../user/delete/{{ $u->id }}' class='btn btn-small btn-center btn-delete-confirm'>Yes, delete {{$u->name}}</a>
                                        <span class='spacebetween'></span>
                                        <button  class='btn btn-default btn-center btn-primary' data-toggle='collapse' data-target='#collapseDeleteForm-{{$ushort}}' aria-controls='collapseExample'>No, cancel</button>
                                    </div>
                                    <br class="clear"/>
                                </li>
                                @endforeach
                                {!! $users->render() !!}
                                <br class="clear"></br>
                            </ul>    
                        </div>
                    
                    </article>


                    <article class="adm-stats-container stats-ads">
                        <h4>Advertisements</h4>
                        <ul class="adm-stats">
                            <li>
                                <i class="fa fa-cart-plus"></i>
                                <span>{{ count($ads) }}</span>
                            </li>
                            <li>
                                <i class="fa fa-credit-card"></i>
                                <span>€ {{ $adsmoney }}</span>
                            </li>
                        </ul>
                    </article>
                    <article class="adm-stats-details details-ads">

                        <div class="adm-list-ads">
                            <h4>Advertisement </h4>
                            <ul class="adm-project-list">
                                This information isn't yet available.
                                <br class="clear"></br>
                            </ul>
                        </div>

                    </article>

                </div>
            </section>
        </main>
    </div>
@stop

@else
    @section('banner')
        <div class="banner banner-admin">
            <div class="banner-darken">
                @include('includes.header')
                <p class="banner-quote">404</p>
                <p class="banner-underquote">We're sorry dude, but you don't have access to the admin panel.</p>
                <a class="btn btn-big  banner-registerbtn" href="{{ URL::to('/') }}">Go back to safety</a>
            </div>
        </div>
        @include('includes.nav')
    @stop
    @section('content')
        @include('includes.newsletter')
    @stop
@endif


@section('scripts')
    <script>
        $(document).ready(function() {

            $(".stats-users").hide();
            $(".stats-contests").hide();
            $(".stats-ads").hide();
            $(".details-users").hide();
            $(".details-contests").hide();
            $(".details-ads").hide();
            $("#projects").addClass('bubble-tip');

            $("#projects").click(function() {
                event.preventDefault();
                $('.second-navbar-item-link').removeClass('bubble-tip');
                $(this).addClass('bubble-tip');
                $(".details-users").slideUp();
                $(".details-contests").slideUp();
                $(".details-ads").slideUp();
                $(".details-projects").slideDown();
            });

            $("#contests").click(function() {
                event.preventDefault();
                $('.second-navbar-item-link').removeClass('bubble-tip');
                $(this).addClass('bubble-tip');
                $(".details-users").slideUp();
                $(".details-contests").slideDown();
                $(".details-ads").slideUp();
                $(".details-projects").slideUp();
            });

            $("#users").click(function() {
                event.preventDefault();
                $('.second-navbar-item-link').removeClass('bubble-tip');
                $(this).addClass('bubble-tip');
                 $(".details-users").slideDown();
                $(".details-contests").slideUp();
                $(".details-ads").slideUp();
                $(".details-projects").slideUp();
            });

            $("#ads").click(function() {
                event.preventDefault();
                $('.second-navbar-item-link').removeClass('bubble-tip');
                $(this).addClass('bubble-tip');
                $(".details-users").slideUp();
                $(".details-contests").slideUp();
                $(".details-ads").slideDown();
                $(".details-projects").slideUp();
            });

        });
    </script>
@stop 