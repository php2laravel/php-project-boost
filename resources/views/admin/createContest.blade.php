@extends('app')

@if (Auth::user()->admin == 1)

    @section('content')
        <h1>Create a new contest</h1>
        @if ($errors->any())
            <ul class="alert alert-danger list-unstyled">
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        {!! Form::open(array('url' => 'admin/new-contest', 'class'=>'create-contest_form')) !!}
            {!! Form::label('title', 'Title:', ['class' => 'create-contest_form-label']) !!}
            {!! form::text('title', null, ['class' => 'form-control create-contest_form-input']) !!}

            {!! Form::label('desc', 'Description: ', ['class' => 'create-contest_form-label']) !!}
            {!! form::textarea('desc', null, ['class' => 'form-control create-contest_form-input']) !!}

            {!! Form::label('startdate', 'Start date:', ['class' => 'create-contest_form-label']) !!}
            
            {!! form::text('startdate', null, ['class' => 'form-control create-contest_form-input', 'id'=>'datepicker', 'placeholder'=> 'ex. ' . date("d-m-Y") ]) !!}
            <br class="clear"/>
            {!! Form::Submit('Add contest', ['class' => 'form-inline btn btn-big btn-center']) !!}

        {!! Form::close() !!}
        <a href="{{URL::previous()}}" class="subtle-link center">Go back without saving</a>
        <br class="clear"/>
    @stop

    @section('scripts')
        <script>
            jQuery(document).ready(function() {
                jQuery('#datepicker').datepicker();
            });
        </script>
    @stop

@else
    @section('banner')
        <div class="banner banner-admin">
            <div class="banner-darken">
                @include('includes.header')
                <p class="banner-quote">404</p>
                <p class="banner-underquote">We're sorry dude, but you don't have access to the admin panel.</p>
                <a class="btn btn-big  banner-registerbtn" href="{{ URL::to('/') }}">Go back to safety</a>
            </div>
        </div>
        @include('includes.nav')
    @stop
    @section('content')
        @include('includes.newsletter')
    @stop
@endif
