@extends('app')
<?PHP
if (Auth::check()) {
    // The user is logged in...
    $user = Auth::user();
}

?>
@if (Auth::check()==false)
    @section('banner')
        @include('includes.banner')
    @stop
@else
    @section('banner')
        <div class="banner banner-home">
            <div class="banner-darken">
                @include('includes.header')
                <div class="welcome">
                    <div class="welcome-block">
                        <h1 class="welcome-title">This is <span class="welcome-logo">Boost</span></h1>
                        <p class="welcome-msg">
                            Welcome {{$user->name}}
                        </p>
                        <div class="content-spotlight-section">
                            @include('includes.cta')
                        </div>
                    </div>
                </div>                      
            </div>
        </div>  
        @include('includes.nav')      
    @stop        
@endif
@section('content')
    <div class="contesthightlight">
        <h3 class="section-title">Winners contest November</h3>
        <div class="section-line"></div>
        <?PHP $colors = ['gold', 'silver', 'bronze']; $r = 0; ?>
        @foreach($winner as $win)
        <div class="contesthightlight-winner">
            <div class="contesthightlight-winner-{{ $colors[$r] }}"></div>
            <a class="contesthightlight-winner-link" href="/projects/{!! $win->id !!}">
            <img class="contesthightlight-winner-image" src="{{URL::to('/')}}/uploads/projects/{!! $win->image !!}" alt="imageproject">
            <div class="contesthightlight-winner-textbox">
                <p class="contesthightlight-winner-textbox-namedesign">{!! $win->title !!}</p>
            </a>
                <?php $designerName = strtolower(str_replace(' ', '-',  $win->name)); ?>
                <a class="contesthightlight-winner-textbox-author" href="/<?php echo $designerName ?>">{!! $win->name !!}</a>  <br><br><br>
                    <ul class="contesthightlight-winner-textbox-tools">
                        <li class="contesthightlight-winner-textbox-tools-cmmnt">
                                <img src="<?php echo asset('images/icons/comment.png')?>" alt="heart icon" class="icon-small">
                                <span class="contesthightlight-winner-textbox-tools-cmmnt-countcomments"> {!! $win->countcomments !!} </span>
                        </li>
                        <li class="contesthightlight-winner-textbox-tools-fav">
                            <img src="<?php echo asset('images/icons/heart.png')?>" alt="heart icon" class="icon-small">
                            <span> {!! $win->countlikes !!} </span>
                        </li>
                        <br class="clearfix"/>
                    </ul>              
            </div>
            <br class="clearfix"/>
        </div>
        <?PHP $r++ ?>
        @endforeach
        <br class="clear"/>
    </div>
    <br class="clearfix"/>
    <div class="content-spotlight">
        <h3 class="section-title">In the spotlight</h3>
        <div class="section-line"></div>
        <br class="clear"/>
        @foreach($spotlight as $work)
            <div class="content-spotlight-item">
                <a class="content-spotlight-link" href="/projects/{!! $work->id !!}">
                    <img class="content-spotlight-item_image" src="{{URL::to('/')}}/uploads/projects/{!! $work->image !!}" alt="imageproject">
                    <div class="content-spotlight-item_info">
                        <p class="content-spotlight-item_info-namedesign">{!! $work->title !!}</p>
                </a>
                <?php $designerName = strtolower(str_replace(' ', '-',  $work->name)); ?>
                <a class="content-spotlight-link" href="/<?php echo $designerName ?>">
                    <p class="content-spotlight-item_info-author">{!! $work->name !!}</p>
                
                        <ul class="content-spotlight-item_info-tools">
                            <li class="cmmnt">
                                <a class="content-spotlight-link" href="/projects/{!! $work->id !!}">
                                    <img src="<?php echo asset('images/icons/comment.png')?>" alt="heart icon" class="icon-small">
                                    <span class="content-spotlight-item_info-author"> {!! $work->countcomments !!} </span>
                                </a>
                            </li>
                            <li class="fav">
                                <img src="<?php echo asset('images/icons/heart.png')?>" alt="heart icon" class="icon-small">
                                <span> {!! $work->countlikes !!} </span>
                            </li>
                            <br class="clearfix"/>
                        </ul>
                    </div>
            </div>
        @endforeach
         <br class="clear"/>
    </div>
    <br class="clear"/>
        @section('socialmedia')
            @include('includes.socialmedia')
        @stop
        @section('newsletter')
            @include('includes.newsletter')
        @stop
        @section('advertentions')
            @include('includes.ads.home')
        @stop
@stop