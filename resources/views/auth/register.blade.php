<!-- resources/views/auth/register.blade.php -->
@extends('app')
@section('box')
    <div class="container-img">
        <div class="box">
            <div class="box-small">
                <img class="box-logo" src="/images/logos/logo.jpg" alt="logo Boost">
                <p class="box-quote">It's about progress, not perfection.</p>
                <ul class="box-list">
                    <li> Create an account.</li>
                    <li> Upload your work.</li>
                    <li> Get feedback from your peers.</li>
                    <li> Get better each day!</li>
                </ul>
            </div>
            <div class="box-big">
                <h3>SIGN UP, it's free!</h3>

                @if ($errors->any())

                    <ul class="alert alert-danger list-unstyled">
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>

                @endif

                <form class="register-form" method="POST" action="/auth/register">
                    {!! csrf_field() !!}

                    <div class="form-group">
                        
                        <label class="register-form-label" for="name">full name</label>
                        <div class="input-group register-form-group">
                            <span class="input-group-addon register-form-addon">
                                <img class="box-icon" src="/images/icons/user.png" alt="icon user">
                            </span>
                            <input placeholder="John Doe" type="text" class="form-control register-form-input" name="name" value="{{ old('name') }}">
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="register-form-label" for="email">email</label>
                        <div class="input-group register-form-group">
                            <span class="input-group-addon register-form-addon">
                                <img class="box-icon" src="/images/icons/mail.png" alt="icon mail">
                            </span>
                            <input placeholder="john.doe&#64;student.thomasmore.be" type="email" class="form-control register-form-input" name="email" id ="email" value="{{ old('email') }}">
                        </div>

                    </div>

                    <div class="form-group">

                        <label class="register-form-label" for="password">password</label>
                        <div class="input-group register-form-group">
                            <span class="input-group-addon register-form-addon">
                                <img class="box-icon" src="/images/icons/password.png" alt="icon lock password">
                            </span>
                            <input type="password" class="form-control register-form-input" id="password" name="password">
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="register-form-label" for="confirm">confirm password</label>
                        <div class="input-group register-form-group">
                            <span class="input-group-addon register-form-addon">
                                <img class="box-icon" src="/images/icons/password.png" alt="icon lock password">
                            </span>
                            <input type="password" class="form-control register-form-input" id="confirm" name="password_confirmation">
                        </div>
                    </div>

                    <div class="form-group">

                        <label class="register-form-label" for="refer">refer a student, get rewards</label>
                        <div class="input-group register-form-group">
                            <span class="input-group-addon register-form-addon">
                                <img class="box-icon" src="/images/icons/user.png" alt="icon lock password">
                            </span>
                            <input placeholder="john.doe&#64;student.thomasmore.be" type="email" class="form-control register-form-input" id="refer" name="refer">
                        </div>
                    </div>

                    <div class="btn-register-wrapper">
                        <button type="submit" class="btn btn-default btn-register">Continue</button>
                    </div>
                    <p class="register-form-link subtle-link">
                    {!! link_to_route('login', 'already have an account?', '', ['class'=>'subtle-link'])!!}
                    </p>
                </form>
            </div>
        </div>
    </div>
@stop
