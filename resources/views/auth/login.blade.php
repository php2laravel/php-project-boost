<!-- resources/views/auth/login.blade.php -->
@extends('app')
    @section('box')
    <div class="container-img">
        <aside class="big-sidebar box-side">
            <h3>Log in</h3>

            @if ($errors->any())

                <ul class="alert alert-danger list-unstyled">
                    @foreach($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>

            @endif

            <form class="register-form" method="POST" action="/auth/login">
                {!! csrf_field() !!}

                <div class="form-group">

                    <label class="register-form-label" for="email">email</label>
                    <div class="input-group register-form-group">
                        <span class="input-group-addon register-form-addon">
                            <img class="box-icon" src="/images/icons/mail.png" alt="icon mail">
                        </span>
                        <input placeholer="email" type="email" class="form-control register-form-input" name="email" id ="email" value="{{ old('email') }}">
                    </div>

                </div>

                <div class="form-group">

                    <label class="register-form-label" for="password">password</label>
                    <div class="input-group register-form-group">
                        <span class="input-group-addon register-form-addon">
                            <img class="box-icon" src="/images/icons/password.png" alt="icon lock password">
                        </span>
                        <input type="password" class="form-control register-form-input" id="password" name="password">
                    </div>
                </div>


                <div class="checkbox">
                    <label>
                        <input type="checkbox" name="remember"> Remember Me
                    </label>
                </div>

                <div class="btn-register-wrapper">
                    <button type="submit" class="btn btn-default btn-register">Log in</button>
                </div>
                <p class="register-form-link">
                {!! link_to_route('register', 'want to create an account?', '', ['class'=>'subtle-link'])!!}
                <a href="/password/email" class="btn btn-link subtle-link">reset password</a>
                </p>

            </form>
        </aside>
    </div>
    @stop
