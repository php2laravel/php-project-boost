<!-- resources/views/auth/password.blade.php -->

@extends('app')

@section('content')

    <h1>Reset password</h1>
    <hr>
    <form method="POST" action="/password/email">
        {!! csrf_field() !!}

        @if (count($errors) > 0)
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        <div class="form-group">

            <label for="email">Email:</label>
            <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">

        </div>

        <div>
            <button type="submit" class="btn btn-default">Send password reset link</button>
        </div>
    </form>

@stop