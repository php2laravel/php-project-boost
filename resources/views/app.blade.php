@if (array_key_exists('head', View::getSections()))
    @yield('head')
    <body>
                <!-- Load Facebook SDK for JavaScript -->
                <div id="fb-root"></div>
                <script>(function(d, s, id) {
                  var js, fjs = d.getElementsByTagName(s)[0];
                  if (d.getElementById(id)) return;
                  js = d.createElement(s); js.id = id;
                  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
                  fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
@else
    @include('includes.head')
    <body>
@endif

    <section class="top-section">
        @if (array_key_exists('banner', View::getSections()))
            @yield('banner')
        @else
            @include('includes.header')
            @include('includes.nav')
        @endif
    </section>
    @yield('box')
    @yield('message')
	<main class="container">
		@yield('left-sidebar')
		@yield('second-sidebar')
		@yield('content')
    </main>
    @yield('second-nav')
    @yield('second-message')
    @yield('second-content')
    
    @yield('socialmedia')
    @yield('newsletter')
    @yield('advertentions')
	@include('includes.footer')
</body>
@yield('scripts')
</html>