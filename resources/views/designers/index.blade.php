@extends('app')
@section('content')
<h1>Designers</h1>
    @foreach ($designers as $designer)
    <?php 
        $name = $designer->name;
        $name = preg_replace('/\s+/', '-', $name);

    ?>
        <div class="content-spotlight-item profiles-tile">
            <a href="{{ action('DesignerController@profile', [$name]) }}">
                @if ($designer->image != "")
                    <img src="{{URL::to('/')}}/uploads/users/{{ $designer->image }}" class="content-spotlight-item_image">
                @else
                    <img src="http://jbryner.me/_images/placeholder_small.gif" alt="placeholder user picture" class="content-spotlight-item_image">
                @endif
                <div class="content-spotlight-item_info">
                    <p class="content-spotlight-item_info-namedesign">{{ $designer->name }}</p>
            </a>
                    <p>
                        @if ($designer->functie != "")
                            <span class="content-spotlight-item_info-author">{{ $designer->functie }}</span>
                        @endif
                        @if ($designer->functie != "" && $designer->locatie != "")
                            <span> | </span>
                        @endif
                        @if ($designer->locatie != "")
                            <span class="content-spotlight-item_info-author">{{ $designer->locatie }}</span>
                        @endif
                    </p>
                </div>
                <br class="clear"/>
        </div>
    @endforeach
    {!! $designers->render() !!}
@stop
