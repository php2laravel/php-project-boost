@extends('app')
<?php 
	if (Auth::check()) {
	    $user = Auth::user();
	}
?>

@section('message')
	@if ($user->id == $designer->id)
			<div class="panel panel-default switch-profiles_panel">
				<img class="switch-profiles_img" src="{{URL::to('/')}}/images/icons/iris.png" alt="icon eye">
				<p class="switch-profiles_msg">This is your public profile. To view your private profile, click <a href="../my-profile">here</a>.</p>
				<br class="clear">
			</div>
	@endif
	@if (Session::has('flash_notification.message'))
        <p class="alert alert-admin alert-{{ Session::get('flash_notification.level') }} alert-top">
         <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        {{ Session::get('flash_notification.message') }}
        </p>
    @endif 
@stop


@section('left-sidebar')
	<aside class="profile-top">
		<div class="profile-top-left">
				@if ($designer->image != "")
					<img src="{{URL::to('/')}}/uploads/users/{{ $designer->image }}" class="profile-picture">
				@else
					<img src="http://jbryner.me/_images/placeholder_small.gif" alt="placeholder user picture" class="profile-picture">
				@endif
			<ul class="profile-info_statistics">
				<li>
	                <img src="<?php echo asset('images/icons/vote.png')?>" alt="heart icon" class="icon-normal">
	                <br>
	                <span> {{ $totallikes }} </span>
				</li>
				<li>
					<img src="<?php echo asset('images/icons/iris.png')?>" alt="heart icon" class="icon-normal">
					<br>
					<span> {{ $designer->views }} </span>
				</li>
			</ul>
			@if ($designer->id != Auth::id() && $follower==false )
		        {!! Form::open( array('class'=>'form-follow', 'id'=>'designer-follow', 'action' => 'FollowerController@store')) !!}
		                {!! Form::hidden('userId', Auth::id()) !!}
		                {!! Form::hidden('designer_id', $designer->id) !!}
		                {!! Form::submit('Follow ', array('class'=>'btn btn-small'))!!}
		        {!! Form::close() !!}
	   		@elseif($designer->id != Auth::id() && $follower==true)
		        <br class="clear"></br>
		        <img src="<?php echo asset('images/icons/iris.png')?>" alt="follower icon" class="icon-small">
		        <span> You follow {{ $designer->name }}</span>
		        {!! Form::open( array('class'=>'form-follow', 'id'=>'designer-follow', 'action' => 'FollowerController@delete')) !!}
		                {!! Form::hidden('userId', Auth::id()) !!}
		                {!! Form::hidden('designer_id', $designer->id) !!}
		                {!! Form::submit('(Unfollow) ', array('class'=>'btn-none subtle-link'))!!}
		        {!! Form::close() !!}
	   		@endif 

		</div>
		<div class="profile-top-right">
			<h3>
				{{ $designer->name }} <span class="profile-info-exp"> {{ $designer->xp()[0] }} | {{ $designer->xp()[1] }} </span>		
			</h3>
			@if($designer->admin == 1)
				<b>BOOST ADMIN | <span>{{$designer->email}}</span></b>
			@endif
			<div class="profile-text">
			<div>
				@if($designer->bio != "")
					<p class="profile-info_bio">
						{{ $designer->bio }}
					</p>
				@endif
			</div>
			<div>
				<ul class="profile-info-basic">
					<li>
						@if($designer->locatie != "")
							<p class="profile-info-basic-item">{{$designer->locatie}}</p>
						@endif
					</li>
					<li>
						@if($designer->opleiding != "")
							<p class="profile-info-basic-item">{{ $designer->opleiding}}</p>
						@endif	
					</li>
					<li>
						@if($designer->functie != "")
							<p class="profile-info-basic-item">{{ $designer->functie}}</p>
						@endif	
					</li>
				</ul>
			</div>


				<div class="profile-info_list-links">
					<p class="profile-info_list-links-item">
						@if($designer->portfolio != "")
							<a href="http://{{ $designer->portfolio }}" target="_blank"><i class="fa glyphicon glyphicon-link"></i></a>
						@endif
					</p>	
					<p class="profile-info_list-links-item">
						@if($designer->twitter != "")
							<a href="http://{{ $designer->twitter }}" target="_blank"><i class="fa fa-twitter"></i></a>
						@endif
					</p>
					<p class="profile-info_list-links-item">
						@if($designer->facebook != "")
							<a href="http://{{ $designer->facebook }}" target="_blank"><i class="fa fa-facebook-official"></i></a>
						@endif
					</p>
				</div>
			</div>
		</div>		
	</aside>
@stop

@section('second-nav')
	<nav class="second-nav">
		<ul class="second-navbar">
			<li class="second-navbar-item">
				<a href="#" title="" id="boosts" class="second-navbar-item-link">
					Boosts ({{count($designer->projects)}})
				</a>
			</li>
			<li class="second-navbar-item">
				<a href="#" title="" id="likes" class="second-navbar-item-link">
					Favourites ({{count($likes)}})
				</a>
			</li>
			<li class="second-navbar-item">
				<a href="#" title="" id="followers" class="second-navbar-item-link">
					Followers ({{count($followers)}})
				</a>
			</li>
			<li class="second-navbar-item">
				<a href="#" title="" id="follows" class="second-navbar-item-link">
					Following ({{count($follows)}})
				</a>
			</li>
			<li class="second-navbar-item">
				<a href="#" title="" id="activity" class="second-navbar-item-link">
					Activity ({{count($activityComments) + count($activityLikes) }})
				</a>
			</li>
			<li class="second-navbar-item">
				<a href="#" title="" id="badges" class="second-navbar-item-link">
					Badges ({{count($badges)}}/{{count($totalBadges)}})
				</a>
			</li>
		</ul>
	</nav>
@stop

        
@section('second-content')
	<div class="sec-container">
	    <main class="container">
			<section id="sec-content">
				@include('includes.myboost')
				@include('includes.favourites')
				@include('includes.follows')
				@include('includes.followers')
				@include('includes.badges')
				@include('includes.activity')
				<div class="spacebetween"></div>
			</section>
		</main>
    </div>
@stop



@section('advertentions')
    @include('includes.ads.profile')
@stop

@section('scripts')
    <script>
		$(document).ready(function() {

			$(".container-show-badges").hide();
			$(".container-show-favourites").hide();
			$(".container-show-follows").hide();
			$(".container-show-followers").hide();
			$(".container-show-activity").hide();
			$("#boosts").addClass('bubble-tip');

			$("#badges").click(function() {
				event.preventDefault();
				$('.second-navbar-item-link').removeClass('bubble-tip');
				$(this).addClass('bubble-tip');
				$(".container-show-myboosts").slideUp();
				$(".container-show-followers").slideUp();
				$(".container-show-favourites").slideUp();
				$(".container-show-follows").slideUp();
				$(".container-show-activity").slideUp();
				$(".container-show-badges").slideDown();
			});

			$("#activity").click(function() {
				event.preventDefault();
				$('.second-navbar-item-link').removeClass('bubble-tip');
				$(this).addClass('bubble-tip');
				$(".container-show-myboosts").slideUp();
				$(".container-show-followers").slideUp();
				$(".container-show-favourites").slideUp();
				$(".container-show-follows").slideUp();
				$(".container-show-badges").slideUp();
				$(".container-show-activity").slideDown();
			});

			$("#likes").click(function() {
				event.preventDefault();
				$('.second-navbar-item-link').removeClass('bubble-tip');
				$(this).addClass('bubble-tip');
				$(".container-show-badges").slideUp();
				$(".container-show-myboosts").slideUp();
				$(".container-show-follows").slideUp();
				$(".container-show-followers").slideUp();
				$(".container-show-activity").slideUp();
			    $(".container-show-favourites").slideDown();
			});

			$("#follows").click(function() {
				event.preventDefault();
				$('.second-navbar-item-link').removeClass('bubble-tip');
				$(this).addClass('bubble-tip');
				$(".container-show-badges").slideUp();
				$(".container-show-favourites").slideUp();
				$(".container-show-myboosts").slideUp();
				$(".container-show-followers").slideUp();
				$(".container-show-activity").slideUp();
			    $(".container-show-follows").slideDown();
			});

			$("#boosts").click(function() {
				event.preventDefault();
				$('.second-navbar-item-link').removeClass('bubble-tip');
				$(this).addClass('bubble-tip');
				$(".container-show-badges").slideUp();
				$(".container-show-favourites").slideUp();
				$(".container-show-followers").slideUp();
			    $(".container-show-follows").slideUp();
				$(".container-show-activity").slideUp();
				$(".container-show-myboosts").slideDown();
			});

			$("#followers").click(function() {
				event.preventDefault();
				$('.second-navbar-item-link').removeClass('bubble-tip');
				$(this).addClass('bubble-tip');
				$(".container-show-myboosts").slideUp();
				$(".container-show-badges").slideUp();
				$(".container-show-favourites").slideUp();
				$(".container-show-follows").slideUp();
				$(".container-show-activity").slideUp();
			    $(".container-show-followers").slideDown();
			});
		});
    </script>
@stop 