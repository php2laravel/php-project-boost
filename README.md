# Project PHP2

Laravel project voor PHP2. 

### Members
- Joren Polfliet
- Thais Lenglez
- Veerle Devos
- Niels Meulders
- Jens Ivens



### Installation

```sh
$ git clone git@bitbucket.org:php2laravel/project-php2.git
$ cd project-php2
$ vagrant up
```