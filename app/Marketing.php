<?php

namespace App;

use Weblee\Mandrill\Mail;
use Illuminate\Http\Request;
use MandrillMail;
use Auth;

class Marketing
{

    public function commentMail($projectTitle, $projectOwner, $ownerEmail, $link, $adone, $adtwo, $adthree, $adonelink, $adtwolink, $adthreelink){
        $template_content = [];
        $message = array(
            'subject' => 'Someone has left you a comment!',
            'from_email' => 'boostman@boost.weareimd.be',
            'from_name' => 'Boostman',
            'to' => array(
                array(
                    'email' => $ownerEmail,
                    'name' => $projectOwner,
                    'type' => 'to'
                )
            ),
            'merge_vars' => array(
                array(
                    'rcpt' => $ownerEmail,
                    'vars' => array(
                        array(
                            'name' => 'NAME',
                            'content' => $projectOwner
                        ),
                        array(
                            'name' => 'SPONSERONE',
                            'content' => $adone
                        ),
                        array(
                            'name' => 'SPONSERTWO',
                            'content' => $adtwo
                        ),
                        array(
                            'name' => 'SPONSERTHREE',
                            'content' => $adthree
                        ),
                        array(
                            'name' => 'SPONSORLINKONE',
                            'content' => $adonelink
                        ),
                        array(
                            'name' => 'SPONSORLINKTWO',
                            'content' => $adtwolink
                        ),
                        array(
                            'name' => 'SPONSORLINKTHREE',
                            'content' => $adthreelink
                        ),
                        array(
                            'name' => 'LINK',
                            'content' => $link
                        ),
                        array(
                            'name' => 'PROJECT',
                            'content' => $projectTitle
                        )
                    )
                )
            )
        );

        MandrillMail::messages()->sendTemplate('BOOST - Comment', $template_content, $message);
    }

    public function likeEmail($projectTitle, $projectOwner, $ownerEmail, $adone, $adtwo, $adthree, $adonelink, $adtwolink, $adthreelink)
    {
        $template_content = [];
        $message = array(
            'subject' => 'People are liking your project!',
            'from_email' => 'boostman@boost.weareimd.be',
            'from_name' => 'Boostman',
            'to' => array(
                array(
                    'email' => $ownerEmail,
                    'name' => $projectOwner,
                    'type' => 'to'
                )
            ),
            'merge_vars' => array(
                array(
                    'rcpt' => $ownerEmail,
                    'vars' => array(
                        array(
                            'name' => 'NAME',
                            'content' => $projectOwner
                        ),
                        array(
                            'name' => 'SPONSERONE',
                            'content' => $adone
                        ),
                        array(
                            'name' => 'SPONSERTWO',
                            'content' => $adtwo
                        ),
                        array(
                            'name' => 'SPONSERTHREE',
                            'content' => $adthree
                        ),
                        array(
                            'name' => 'SPONSORLINKONE',
                            'content' => $adonelink
                        ),
                        array(
                            'name' => 'SPONSORLINKTWO',
                            'content' => $adtwolink
                        ),
                        array(
                            'name' => 'SPONSORLINKTHREE',
                            'content' => $adthreelink
                        ),
                        array(
                            'name' => 'PROJECT',
                            'content' => $projectTitle
                        )
                    )
                )
            )
        );

        MandrillMail::messages()->sendTemplate('BOOST - Likes', $template_content, $message);
    }


    public function adMail($adCompany, $adWebsite, $startdate, $enddate, $price, $adEmail, $adone, $adtwo, $adthree, $adonelink, $adtwolink, $adthreelink)
    {
        $template_content = [];
        $message = array(
            'subject' => 'Your advertisement has been added succesfully.',
            'from_email' => 'boostman@boost.weareimd.be',
            'from_name' => 'Boostman',
            'to' => array(
                array(
                    'email' => $adEmail,
                    'name' => $adCompany,
                    'type' => 'to'
                )
            ),
            'merge_vars' => array(
                array(
                    'rcpt' => $adEmail,
                    'vars' => array(
                        array(
                            'name' => 'COMPANY',
                            'content' => $adCompany
                        ),
                        array(
                            'name' => 'WEBSITE',
                            'content' => $adWebsite
                        ),
                        array(
                            'name' => 'START',
                            'content' => $startdate
                        ),
                        array(
                            'name' => 'END',
                            'content' => $enddate
                        ),
                        array(
                            'name' => 'PRICE',
                            'content' => $price
                        ),
                        array(
                            'name' => 'SPONSERONE',
                            'content' => $adone
                        ),
                        array(
                            'name' => 'SPONSERTWO',
                            'content' => $adtwo
                        ),
                        array(
                            'name' => 'SPONSERTHREE',
                            'content' => $adthree
                        ),
                        array(
                            'name' => 'SPONSORLINKONE',
                            'content' => $adonelink
                        ),
                        array(
                            'name' => 'SPONSORLINKTWO',
                            'content' => $adtwolink
                        ),
                        array(
                            'name' => 'SPONSORLINKTHREE',
                            'content' => $adthreelink
                        ),

                        array(
                            'name' => 'EMAIL',
                            'content' => $adEmail
                        )
                    )
                )
            )
        );

        MandrillMail::messages()->sendTemplate('BOOST - Ad', $template_content, $message);
    }


    public function weeklyMail($name, $email, $topLikes, $mostComments, $newest, $adone, $adtwo, $adthree, $adonelink, $adtwolink, $adthreelink)
    {
        $template_content = [];
        $message = array(
            'subject' => 'Weekly Boost overview.',
            'from_email' => 'boostman@boost.weareimd.be',
            'from_name' => 'Boostman',
            'to' => array(
                array(
                    'email' => $email,
                    'name' => $name,
                    'type' => 'to'
                )
            ),
            'merge_vars' => array(
                array(
                    'rcpt' => $email,
                    'vars' => array(
                        array(
                            'name' => 'NAME',
                            'content' => $name
                        ),
                        array(
                            'name' => 'EMAIL',
                            'content' => $email
                        ),
                        array(
                            'name' => 'COMMENTS',
                            'content' => $mostComments
                        ),
                        array(
                            'name' => 'NEWEST',
                            'content' => $newest
                        ),
                        array(
                            'name' => 'SPONSERONE',
                            'content' => $adone
                        ),
                        array(
                            'name' => 'SPONSERTWO',
                            'content' => $adtwo
                        ),
                        array(
                            'name' => 'SPONSERTHREE',
                            'content' => $adthree
                        ),
                        array(
                            'name' => 'SPONSORLINKONE',
                            'content' => $adonelink
                        ),
                        array(
                            'name' => 'SPONSORLINKTWO',
                            'content' => $adtwolink
                        ),
                        array(
                            'name' => 'SPONSORLINKTHREE',
                            'content' => $adthreelink
                        ),
                        array(
                            'name' => 'TOPLIKES',
                            'content' => $topLikes
                        )
                    )
                )
            )
        );

        MandrillMail::messages()->sendTemplate('BOOST - OverviewW', $template_content, $message);
    }

    public function monthlyMail($name, $email, $topLikes, $mostComments, $newest, $adone, $adtwo, $adthree, $adonelink, $adtwolink, $adthreelink)
    {
        $template_content = [];
        $message = array(
            'subject' => 'Monthly Boost Overview.',
            'from_email' => 'boostman@boost.weareimd.be',
            'from_name' => 'Boostman',
            'to' => array(
                array(
                    'email' => $email,
                    'name' => $name,
                    'type' => 'to'
                )
            ),
            'merge_vars' => array(
                array(
                    'rcpt' => $email,
                    'vars' => array(
                        array(
                            'name' => 'NAME',
                            'content' => $name
                        ),
                        array(
                            'name' => 'EMAIL',
                            'content' => $email
                        ),
                        array(
                            'name' => 'COMMENTS',
                            'content' => $mostComments
                        ),
                        array(
                            'name' => 'NEWEST',
                            'content' => $newest
                        ),
                        array(
                            'name' => 'SPONSERONE',
                            'content' => $adone
                        ),
                        array(
                            'name' => 'SPONSERTWO',
                            'content' => $adtwo
                        ),
                        array(
                            'name' => 'SPONSERTHREE',
                            'content' => $adthree
                        ),
                        array(
                            'name' => 'SPONSORLINKONE',
                            'content' => $adonelink
                        ),
                        array(
                            'name' => 'SPONSORLINKTWO',
                            'content' => $adtwolink
                        ),
                        array(
                            'name' => 'SPONSORLINKTHREE',
                            'content' => $adthreelink
                        ),
                        array(
                            'name' => 'TOPLIKES',
                            'content' => $topLikes
                        )
                    )
                )
            )
        );

        MandrillMail::messages()->sendTemplate('BOOST - OverviewM', $template_content, $message);
    }
}