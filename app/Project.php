<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
   protected $fillable = [
       'title',
       'user_id',
       'desc',
       'image',
       'competition',
   ];
  public function User()
   {
        return $this->belongsTo('App\User');
    }
  // public $timestamps = false;

  public function votes()
	{
	    return $this->belongsToMany('App\User', 'votes', 'projectId', 'userId');
	}

  public function likes() {
      return $this->hasMany('App\Vote', 'projectId');
  }

  public function comments() {
      return $this->hasMany('App\Comment');
  }
  
  public function tags()
  {
      return $this->belongsToMany('App\Tag')->withTimeStamps();
  }
  public function getTagListAttribute() // $project->tagList or $project->tag_list
  {
      return $this->tags->lists('id'); // get array of tag id's associated with this project
  }
}