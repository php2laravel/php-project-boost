<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use DB;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Contracts\Billable as BillableContract;
use Auth;

class User extends Model implements AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract, BillableContract
{
    use Authenticatable, Authorizable, CanResetPassword, Billable;

    protected $dates = ['trial_ends_at', 'subscription_ends_at'];
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'opleiding',
        'locatie',
        'bio',
        'functie',
        'portfolio',
        'twitter',
        'facebook',
        'instagram',
        'image',
        'refer',
        'emails',
        'mailsTime',
        'experience',
        'confirmation_code',
        'confirmed'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public function projects()
    {
        return $this->hasMany('App\Project');
    }

    public function badges()
    {
        return $this->hasMany('App\BadgeUser');
    }

    public function contests()
    {
        return $this->hasMany('App\Contest');

    }

    public function xp()
    {
        $exp = $this->experience;
        if ($exp == 0)
        {
            $xp = "No ranking available";
            $rank = "";
        }
        elseif ($exp < config('app.ranking.rookie'))
        {
            $xp = $exp . "/" .  config('app.ranking.rookie') . "xp";
            $rank = "newbie";
        }
        elseif ($exp < config('app.ranking.beginner'))
        {
            $xp = $exp . "/" .  config('app.ranking.beginner') . "xp";
            $rank = "rookie";
        }
        elseif ($exp < config('app.ranking.semi_pro'))
        {
            $xp = $exp . "/" .  config('app.ranking.semi_pro') . "xp";
            $rank = "beginner";
        }
        elseif ($exp < config('app.ranking.professional'))
        {
            $xp = $exp . "/" .  config('app.ranking.professional') . "xp";
            $rank = "semi professional";
        }
        else
        {
            $xp = $exp . "/" .  config('app.ranking.expert') . "xp";
            $rank = "expert";
        }
        return [$xp,$rank];
    }

    public function update_xp($id,$xp)
    {
        //find user
        $user = User::find($id);
        $user->experience = $user->experience + $xp;
        $user->save();
    }

    public function remove_xp($id,$xp)
    {
        //find user
        $user = User::find($id);
        $user->experience = $user->experience - $xp;
        $user->save();
    }

    public function add_badge($id, $name)
    {
        //find user
        $user = User::find($id);
        if (!$user->hasBadge($id, $name))
        {
            $badge = Badge::where('name',"=",$name)->firstOrFail();
            $badge_id = $badge->id;
            DB::table('badges_users')
                ->insert(array('badge_id' => $badge_id, 'user_id' =>$id));

        }
    }

    public function comments()
    {
        return $this->hasMany('App\Comment');
    }

    public function votes()
    {
        return $this->belongsToMany('App\Project', 'votes', 'userId', 'projectId');
    }

    public function hasBadge($id, $badge_name)
    {
        $ret = false;
        $badges = DB::table('badges_users')
            ->join('badges', 'badges_users.badge_id', '=', 'badges.id')
            ->select('*')
            ->where('badges_users.user_id', '=', $id)
            ->where('badges.name', '=', $badge_name)
            ->get();
        if (count($badges)>0){$ret = true;}
        return $ret;
    }
}


