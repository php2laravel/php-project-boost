<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//HOME
Route::get('/', 'indexController@index');

//ADMIN
//panel
Route::get('admin', ['middleware' => 'auth', 'uses'=>'AdminController@index']);
//new contest
Route::get('admin/new-contest', ['middleware' => 'auth', 'uses'=>'ContestsController@create']);
Route::post('admin/new-contest', ['middleware' => 'auth', 'uses'=>'ContestsController@newcontest']);
//new tag
Route::get('admin/new-tag', ['middleware' => 'auth', 'uses'=>'TagController@create']);
Route::post('admin/new-tag', ['middleware' => 'auth', 'uses'=>'TagController@store']);



//BOOSTS
//boosts
Route::get('projects', ['as' => 'projects.index', 'uses' => 'projectsController@index']);
//create & store
Route::get('projects/create', ['middleware' => 'auth', 'uses'=>'ProjectsController@create']);
Route::post('projects', 'ProjectsController@store');
//edit, save & delete
Route::get('projects/edit/{id}', ['middleware' => 'auth', 'uses'=>'ProjectsController@edit']);
Route::post('projects/edit/{id}', 'ProjectsController@update');
Route::get('projects/delete/{id}', 'ProjectsController@delete');
//vote 
Route::post('projects/vote/{id}','VoteController@store');
Route::post('projects/delete/{id}','VoteController@delete');

Route::post('contest/vote/{id}','VoteController@store');
Route::post('contest/delete/{id}','VoteController@delete');
//Route::post('projects/vote/{id}', 'VoteController@store');
/*
			Route::get('ajaxtest', 'ProjectsController@ajax');
			Route::get('ajax/{id}', 'ProjectsController@vote');
			Route::get('projects/vote', function(){
				if (Request::ajax()) {
					return Response::json(Request::all());
				}
			});
			Route::get('projects/my-vote/{id}', function(){
				//$voteexist = Vote::where('userId', '=', Auth::id())->first();
				if (1 === 1) {
				   return true;
				} else {
					return false;
				}
				/*
			    if (Vote::whereUserId(Auth::id())->whereProjectId($projectId)->exists()){
			        return 'true';
			    }
			    else {
			    	return 'false';
			    }
			});
			//ajax test
			Route::get('vote', function(){
				if (Request::ajax()) {
					return Response::json(Request::all());
				}
			});

			Route::get('projects/islikedbyme/{id}', 'ProjectController@isLikedByMe');
			Route::post('projects/like/{id}', 'ProjectController@like');
*/

// CONTESTS
Route::get('contests', ['middleware' => 'auth', 'uses'=>'ContestsController@index']);
Route::get('applycontest', ['middleware' => 'auth', 'uses'=>'ApplyContestController@index']);
// ->post
Route::post('applycontest', 'ContestsController@store');
//detail contest detail project
Route::get('contest/{id}', ['middleware' => 'auth', 'uses'=>'ContestsController@show']);

//detail page project
Route::get('projects/{id}', ['middleware' => 'auth', 'uses'=>'ProjectsController@show']);
//follow designer
Route::post('projects/follow/{id}','FollowerController@store');
Route::post('projects/unfollow/{id}','FollowerController@delete');

// add or remove comment
Route::post('projects/{id}','CommentController@store');
Route::post('contest/{id}','CommentController@store');

//search
Route::get('search', ['middleware' => 'auth', 'uses'=>'QueryController@search']);
Route::get('tag', ['middleware' => 'auth', 'uses'=>'QueryController@tag']);
Route::get('colour', ['middleware' => 'auth', 'uses'=>'QueryController@colour']);

//USERS
//create, delete & store
Route::get('user/create', 'UserController@create');
Route::post('user', 'UserController@store');
Route::get('user/activity', 'UserController@activity');
Route::get('user/delete/{id}', 'UserController@delete');

// Email verification
Route::get('register/verify/{confirmationCode}', 'UserController@confirm');

// Authentication routes...
Route::get('auth/login', 
		['as' => 'login', 'uses' =>'Auth\AuthController@getLogin']);
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 
		['as' => 'logout', 'uses' =>'Auth\AuthController@getLogout']);

// Registration routes...
Route::get('auth/register',
		['as' => 'register', 'uses' => 'Auth\AuthController@getRegister']);
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

// User profile routes
Route::get('my-profile', 
			['as' => 'my-profile', 'middleware' => 'auth', 'uses' => 'UserController@show']);
Route::get('my-profile/edit', ['middleware' => 'auth', 'uses'=>'UserController@edit']);
Route::get('my-profile/activity', ['middleware' => 'auth', 'uses'=>'UserController@activity']);

Route::post('my-profile/edit/{id}', 'UserController@update');

//ads
Route::get('advertise', 'AdsController@index');
Route::post('advertise', 'AdsController@store');

Route::get('advertise/{id}', 'AdsController@redirect');

// API 
Route::get('apipage', 'ApiPageController@index');
Route::group(array('prefix' => 'api/v1'), function() {
    Route::get('/items/popular', 'ApiController@popular');
    Route::get('projects/{id}', ['middleware' => 'api', 'uses'=>'ApiController@projects']);
});

// notifications 
Route::get('notifications', 'UserController@notifications');

//designers
Route::get('designers', ['as'=>'designers', 'uses'=>'DesignerController@show']);
Route::get('{name}', ['as'=>'public-profile', 'middleware' => 'auth', 'uses'=>'DesignerController@profile']);
