<?php

namespace App\Http\Controllers;

use App\Comment;
use Request;
use Input;
use Validator;
use Redirect;
use Session;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use App\Marketing;
use DB;
use Flash;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {

        $input = Request::all();
        $Marketing = new Marketing();

        $projectOwnerID = DB::table('users')
            ->select('users.id')
            ->where('projects.id', '=', $input['project_id'])
            ->join('projects', 'users.id', '=', 'projects.user_id')
            ->value('users.id');

        $mails = DB::table('users')
            ->select('emails')
            ->where('id', '=', $projectOwnerID)
            ->value('emails');

        $projectTitle = DB::table('projects')
            ->select('title')
            ->where('id', '=', $input['project_id'])
            ->value('title');

        $projectOwner = DB::table('users')
            ->select('name')
            ->where('projects.id', '=', $input['project_id'])
            ->join('projects', 'users.id', '=', 'projects.user_id')
            ->value('name');

        $ownerEmail = DB::table('users')
            ->select('email')
            ->where('projects.id', '=', $input['project_id'])
            ->join('projects', 'users.id', '=', 'projects.user_id')
            ->value('email');

        // Advertisements in mailing footer
        $today = date("m-d-Y");

        $advertisements = DB::table('ads')
            ->where('enddate', '>', $today)
            ->take(3)
            ->get();

        $ad_array = ['Add your advertisement here','Add your advertisement here','Add your advertisement here'];
        $ad_link_array = ['http://boost.weareimd.be/advertise','http://boost.weareimd.be/advertise','http://boost.weareimd.be/advertise'];
        $teller = 0;

        foreach($advertisements as $ad)
        {
            $ad_array[$teller] = $ad->companyname;
            $ad_link_array[$teller] = $ad->website;
            $teller++;
        }

        $adone = $ad_array[0];
        $adtwo = $ad_array[1];
        $adthree = $ad_array[2];

        $adonelink = $ad_link_array[0];
        $adtwolink = $ad_link_array[1];
        $adthreelink = $ad_link_array[2];
        if(Request::ajax()) {
                $data = Input::all();
                $comment = new Comment;
                $comment->fill(\Input::all());
                $comment->save();
                die;
            }
        if ($input['body'] == '') {
            Session::flash('noresult', 'Je reactie is leeg.');

        } else {

            $comment = new Comment;
            $comment->body = $input['body'];
            $comment->project_id = $input['project_id'];
            $comment->user_id = Auth::user()->id;
            $comment->created_at = Carbon::now();
            $link = "boost.weareimd.be/projects/" . $input['project_id'];
            $comment->save();
            if ($mails == "1") {
                $Marketing->commentMail($projectTitle, $projectOwner, $ownerEmail, $link, $adone, $adtwo, $adthree, $adonelink, $adtwolink, $adthreelink);
            }

        $user = Auth::user();
        $user->update_xp($user->id, config('app.experience.comment'));

        DB::table('projects')->where('projects.id', '=', $input['project_id'])->increment('countcomments');

        return redirect('projects/' . $input['project_id']);

        }
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }
}
