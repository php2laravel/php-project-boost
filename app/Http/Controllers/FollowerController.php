<?php

namespace App\Http\Controllers;

use App\Project;
use App\Follower;
use App\User;
use Auth;
use Request;
use Redirect;
use Input;
use DB;
use Flash;
use Session;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Marketing;

class FollowerController extends Controller
{
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   /* public function store($id)
    {
        if(Request::ajax()) {
            $data = Input::all();
            //$vote = new Vote;
            print_r($data);
            die;
        }
        $.post('/employees/'+$(this).val(), function(response){
    }  
    */


    public function store($id){

        //user becomes fan of the designer
        $input = Request::all();
        $user=$input['userId'];
        $designer=$input['designer_id'];

        $follower = new Follower;
        $follower->designer_id = $designer;
        $follower->fan_id = $user;
        
        $follower->save();
        $designername = User::where('id', '=' , $designer)->firstOrFail(); 
        
        Flash::success("From now on, you're following ".$designername->name.". ");
        return Redirect::back();
    }

    public function delete($id){
        $input = Request::all();
        $user=$input['userId'];
        $designer=$input['designer_id'];
        $follower = DB::table('followers')
            ->where('designer_id', '=', $designer)
            ->where('fan_id', '=', $user)
            ->delete();
        $designername = User::where('id', '=' , $designer)->firstOrFail(); 
        Flash::message("You've stopped following ".$designername->name.". ");
        return Redirect::back();
    }
}
