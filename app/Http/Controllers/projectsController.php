<?php

namespace App\Http\Controllers;

use Flash;
use App\Project;
use App\Vote;
use App\User;
use App\Tag;
use Auth;
//use Illuminate\Http\Request;
use Request;
use Input;
use File;
use Validator;
use Redirect;
use Session;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Image;
use DB;
use League\ColorExtractor\Client as ColorExtractor;

class projectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = DB::table('projects')
            ->join('users', 'users.id', '=', 'projects.user_id')
            ->where('competition', '=', false)
            ->select('users.name','projects.*')
            ->orderBy('projects.created_at', 'desc')
            ->paginate(12);


        return view('pages.projects', compact('projects', 'designershort'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(/*Request $request*/)
    {
        //$this->validate($request, ['title'=>'required|min:3']);
        $input = Request::all();
        $lastproject = DB::table('projects')->max('id');
        $newid = $lastproject+1;
        
        $image = Input::file('image');
        $imageN = $image->getClientOriginalName();
        $filename  = $newid. '.' . $image->getClientOriginalExtension();
        $path = public_path('uploads/projects/' . $filename);

        Image::make($image->getRealPath())->fit(800, 800)->save($path);

        $project = Auth::user()->projects()->create($input);
        $project->user_id = Auth::id();
        $project->title = $input['title'];
        if (isset($input['tag_list'])) {
            $this->syncTags($project, $input['tag_list']);
        }
        $project->desc = $input['desc'];
        $project->image = $filename;
        $project->competition = false;

        $project->save();
        Flash::success('Your project is added to the gallery');

        // add exp to the user for uploading a project
        $user = Auth::user();
        $user->update_xp($user->id, config('app.experience.upload'));

        // add badge if user hasn't got one
        $user->add_badge($user->id, config('app.badges.first_upload'));

        return redirect('/projects');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $project = Project::findOrFail($id)->load("User");
        //$input = Request::all();
        //-----------------------COLORS--------------------------//
        $client = new ColorExtractor;
        $info = pathinfo('uploads/projects/'.$project -> image);
        if ($info["extension"] == "jpg") {
            $image = $client->loadJpeg('uploads/projects/'.$project->image);
        }else if($info["extension"] == "png")
        {
            $image = $client->loadPng('uploads/projects/'.$project->image);
        }else if($info["extension"] == "gif") {
            $image = $client->loadGif('uploads/projects/'.$project->image);
        }else{
            echo 'Wrong image extension';
        }
        $palette = $image->extract(3);
        $palette = str_replace("#", "", $palette);

        $project->colors = implode(', ', $palette);
        $project->save();

        //-----------------------COLORS--------------------------//

        //-----------------------DB-----------------------------//
        $project_comments = DB::table('comments')
            ->select('comments.id', 'body', 'name', 'image', 'comments.created_at')
            ->where('project_id', '=', $id)
            ->join('users', 'users.id', '=', 'user_id')
            ->orderBy('created_at', 'desc')
            //->get());
            ->simplePaginate(10);

        //-----------------------DB-----------------------------//
        //return view('pages.showProject', compact('project', 'palette'));
        // votes
        $votes = DB::table('votes')
            ->where('votes.projectId', '=' , $id)
            ->join('users', 'users.id', '=', 'votes.userId')
            ->select('users.name','votes.*')
            ->get();
        $topVotes = DB::table('votes')
            ->where('votes.projectId', '=' , $id)
            ->join('users', 'users.id', '=', 'votes.userId')
            ->select('users.name','votes.*')
            ->limit(3)
            ->get();
        $project = Project::findOrFail($id);
        $designer = User::findOrFail($project->user_id);
        $firstWork = DB::table('projects')
            ->where('user_id', '=' , $designer->id)
            ->where('id', '<>', $id)
            ->select('id','image')
            ->orderBy('countlikes','desc')
            ->limit(4)
            ->get();
        $designershort = strtolower(str_replace(' ', '-',  $designer->name));
        $firstvote = Vote::where('projectId', '=' , $id)->first();
        $myvote = DB::table('votes')
            ->where('projectId', '=' , $id)
            ->where('userId', '=', Auth::id())
            ->first();
        if ($myvote != null) {
            $myvote = true;
        } else {
            $myvote = false;
        }
        $countVotes = Vote::where('projectId', '=' , $project->id)->count();

        $follower = DB::table('followers')
            ->where('fan_id', '=' , Auth::id())
            ->where('designer_Id', '=', $designer->id)
            ->first();
        if ($follower != null) {
            $follower = true;
        } else {
            $follower = false;
        }
        
        $today = date("m-d-Y");

        $a = DB::table('ads')
            ->where('enddate', '>', $today)
            ->take(3)
            ->get();

        return view('pages.showProject', ['follower'=>$follower, 'project' => Project::findOrFail($id), 'comments' => $project_comments, 'palette' => $palette, 'designer' => $designer, 'countVotes' => $countVotes, 'votes' => $votes, 'topVotes' => $topVotes, 'designershort' => $designershort, 'myvote' =>$myvote, 'firstWork' => $firstWork, 'a' => $a]);
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = Project::findOrFail($id);
        $tags = Tag::lists('name', 'id');
        return view('pages.projects.edit', compact('project', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id)

    {
        //get project
        $project = Project::find($id);
        $input = Request::all();
        //auto prepopulate form
        $project->fill(\Input::all());
        //upload and rename new image
        if (Input::hasFile('image'))
        {
            $image = Input::file('image');
            $imageN = $image->getClientOriginalName();
            $filename  = $id. '.' . $image->getClientOriginalExtension();
            $path = public_path('uploads/projects/' . $filename);
            $oldimage = $project->image;
            File::delete('uploads/projects/'.$oldimage);

            Image::make($image->getRealPath())->fit(800, 800)->save($path);
            $project->image = $filename;
        }

        //save project and return to project page
        if (isset($input['tag_list'])) {
            $this->syncTags($project, $input['tag_list']);
        } else {
            $project->tags()->detach();
        }


        $project->save();// no validation implemented
        Flash::message('Changes saved!');
        if ($project->competition == true) {
            return redirect('/contests'); 
        }
        else {
            return redirect('/projects'); 
        }   
    }

    private function syncTags(Project $project, array $tags) {
        $project->tags()->sync($tags);
    }

    public function delete($id)
    {
        //get project
        $project = Project::find($id);
        //get images and delete from uploads folder
        $filename = $project->image;
        //delete project
        if ($project->competition == true) {
            $project->delete();
            File::delete('uploads/projects/'.$filename);
            //back to gallery
            Flash::message('Your submission is deleted from the gallery');
            return redirect('/contests');
        } else {
            $project->delete();
            File::delete('uploads/projects/'.$filename);
            //back to gallery
            Flash::message('Your project is deleted from the gallery');
            return redirect('/projects');
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function projects()
    {
        return view('pages.projects');
    }

    public function create()
    {
        // all tags (name collumn, with value set as key) from the DB
        $tags = Tag::lists('name', 'id');
        return view('pages.projectsCreate', compact('tags'));

    }


    public function ajax()
    {
        return view('ajax');
    }

    /*
    public function post_isLikedByMe($id)
    {
        $project = Project::findOrFail($id)->first();
        if (Vote::whereUserId(Auth::id())->whereProjectId($project->id)->exists()){
            return 'true';
        }
        return 'false';
    }

    public function like()
    {
        // Get the Project
        $project = Project::findOrFail($id)->first();
        // If the user already like this project, we delete the like
        $existing_vote = Vote::whereProjectId($project->id)->whereUserId(Auth::id())->first();
        if (!is_null($existing_vote)) {
            $existing_vote->delete();
        }
        else { // The user don't like this project yet

            // We check if the user already liked this project once, but unliked it.
            $existing_vote = Vote::onlyTrashed()->whereProjectId($project->id)->whereUserId(Auth::id())->first();

            if (!is_null($existing_vote)) { // The user has liked the project before, but deleted his like later
                // Then, we restore this like
                $existing_vote->restore();
            }
            else { // The user never liked this project before, so we create the like, and we can trigger some events (notifications, etc)
                $vote = new Vote;
                $vote->userId = Auth::id();
                $vote->projectId = 5;
                $vote->save();
            }
        }
    }
    */
}
