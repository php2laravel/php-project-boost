<?php

namespace App\Http\Controllers;

use Hash;
use Mockery\Exception;
use View;
use Input;
use Auth;
use App\User;
use Request;
use Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Image;
use Flash;

class DesignerController extends Controller
{
    public function show()
    {
        $designers = DB::table('users')->select('*')->paginate(12);
        return view('designers.index', compact('designers'));
    }
    
    public function profile($name)
    {
        $name = str_replace('-', ' ', $name);
        $designer = User::where('name', '=' , $name)->firstOrFail();
        $user = Auth::user();
        $likes = DB::table('projects')
            ->join('users', 'users.id', '=', 'projects.user_id')
            ->where('votes.userId', '=', $designer->id)
            ->join('votes', 'projectId', '=', 'projects.id')
            ->select('projects.*', 'users.name')
            ->orderBy('votes.created_at', 'desc')
            ->get();

        $myLikes = DB::table('projects')
            ->join('users', 'users.id', '=', 'projects.user_id')
            ->join('votes', 'projectId', '=', 'projects.id')
            ->where('votes.userId', '=', $user->id)
            ->select('projects.*', 'users.name')
            ->orderBy('votes.created_at', 'desc')
            ->get();

        $sharedLikes = DB::table('projects')
            ->join('users', 'users.id', '=', 'projects.user_id')
            ->join('votes', 'projectId', '=', 'projects.id')
            ->where('votes.userId', '=', $user->id)
            ->orWhere('votes.userId', '=', $designer->id)
            ->select('projects.*', 'users.name')
            ->get();
        
        $list = array(); // new array to check if a project is liked by both users 
        $match = array(); // new array that contains all the projects that are liked by both users 
        if(count($sharedLikes) > 1):
            foreach($sharedLikes as $work): 
                $new = $work->id;
                if (in_array($new, $list)):
                    array_push($match, $new);   // if the projectID is a duplicate: push it to the match array
                endif; 
                array_push($list, $new);
            endforeach;
        endif;

        if (count($match) < 1) {
            $sharedLikes = array();
        } else {
            $sharedLikes = DB::table('projects')
            ->join('users', 'users.id', '=', 'projects.user_id')
            ->whereIn('projects.id', $match)
            ->select('projects.*', 'users.name')
            ->get();
        }

        $follower = DB::table('followers')
            ->where('fan_id', '=' , Auth::id())
            ->where('designer_Id', '=', $designer->id)
            ->first();
        if ($follower != null) {
            $follower = true;
        } else {
            $follower = false;
        }

        $follows = DB::table('followers')
            ->where('fan_id', '=', $designer->id)
            ->join('users', 'users.id', '=', 'designer_id')
            ->select('users.image', 'users.name', 'users.functie')
            ->get();

        $followers = DB::table('followers')
            ->where('designer_id', '=', $designer->id)
            ->join('users', 'users.id', '=', 'fan_id')
            ->select('users.image', 'users.name', 'users.functie')
            ->get();

        if ($designer === null) {
            return view('errors.404');
        }


        $badges = DB::table('badges_users')
            ->join('badges', 'badges_users.badge_id', '=', 'badges.id')
            ->select('badges.*')
            ->where('badges_users.user_id', '=', $designer->id)
            ->orderBy('created_at', 'desc')
            ->get();

        $today = date("m-d-Y");

        $totalBadges = DB::table('badges')
            ->select('*')
            ->get();

        $a = DB::table('ads')
            ->where('enddate', '>', $today)
            ->take(3)
            ->get();

        $totallikes = DB::table('projects')
        // alle countlikes van alle projecten van deze user
            ->join('users', 'projects.user_id', '=', 'users.id')
            ->where('user_id', '=', $designer->id)
            ->sum('countlikes');

        DB::table('users')
            ->where('id', "=", $designer->id)
            ->increment('views');

        $activityComments = DB::table('comments')
            ->select('*')
            ->join('users', 'comments.user_id', '=', 'users.id')
            ->join('projects', 'comments.project_id', '=', 'projects.id')
            ->where('comments.user_id', '=',  $designer->id)
            ->orderBy('comments.created_at', 'desc')
            ->take(40)
            ->simplePaginate(5);

        //comments & likes you made
        $activityLikes = DB::table('votes')
            ->select('*')
            ->join('users', 'votes.userId', '=', 'users.id')
            ->join('projects', 'votes.projectId', '=', 'projects.id')
            ->where('userId', '=',  $designer->id)
            ->orderBy('votes.created_at', 'desc')
            ->take(40)
            ->simplePaginate(5);


        return view('designers.profile', compact('designer', 'follower', 'followers', 'likes', 'totallikes', 'sharedLikes', 'myLikes', 'follows', 'badges','totalBadges', 'a', 'views', 'activityComments', 'activityLikes'));
    }
}