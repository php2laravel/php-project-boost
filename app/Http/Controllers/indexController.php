<?php

namespace App\Http\Controllers;

use App\Project;
use Auth;
use Request;
use Input;
use File;
use Validator;
use Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use Image;
use Flash;
use App\Vote;

class indexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $spotlight = DB::table('projects')
            ->join('users', 'users.id', '=', 'projects.user_id')
            ->where('competition', '=', false)
            ->select('users.name','projects.*')
            ->orderBy('users.experience', 'desc')
            ->orderBy('projects.countlikes', 'desc')
            ->groupBy('users.id')
            ->take(9)
            ->get();

        //print_r($spotlight);

        $winner = DB::table('projects')
            ->join('users', 'users.id', '=', 'projects.user_id')
            // ->where('countlikes', DB::raw("(select max('countlikes') from projects)"))
            ->where('competition', '=', true)
            // ->select(DB::raw('max(countlikes) as countlikes, projects'))
            ->select('users.name','projects.*')
            ->orderBy('countlikes', 'desc')
            // ->where('countlikes', DB::raw("(select max('countlikes'))"))       
            ->take(3)
            ->get();


        $today = date("m-d-Y");

        $a = DB::table('ads')
            ->where('enddate', '>', $today)
            ->take(3)
            ->get();

      return view('index', compact('spotlight', 'winner', "a"));

    }

    public function advertise()
    {
      return view('pages.advertise');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
