<?php

namespace App\Http\Controllers;

use Flash;
use App\Project;
use App\Vote;
use App\User;
use App\Tag;
use App\Contest;
use Auth;
use Request;
use Input;
use App\Comment;
use App\Advertisement;
use File;
use Validator;
use Redirect;
use Session;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Image;
use DB;


class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $likes = Vote::select('votes.*', 'projects.title', 'projects.id', 'users.id', 'users.name')
            ->join('users', 'users.id', '=', 'votes.userId')
            ->join('projects', 'projects.id', '=', 'votes.projectId')
            ->select('users.name','votes.*', 'projects.title')
            ->paginate(10);
        $projects = Project::select('projects.id', 'projects.title', 'projects.image', 'users.name')
            ->join('users', 'users.id', '=', 'projects.user_id')
            ->where('competition', '=', false)
            ->paginate(10);
        $contests = Project::select('projects.id', 'projects.title', 'projects.image', 'users.name')
            ->join('users', 'users.id', '=', 'projects.user_id')
            ->where('competition', '=', true)
            ->paginate(10);
        $challenges = Contest::select('*')
            ->paginate(10);
        $comments = Comment::select('comments.*', 'users.name', 'users.id', 'projects.id', 'projects.title')
            ->join('users', 'users.id', '=', 'comments.user_id')
            ->join('projects', 'projects.id', '=', 'comments.project_id')
            ->select(['comments.*', 'users.name', 'users.id', 'projects.id', 'projects.title', DB::raw('comments.id as thisid')])
            ->paginate(10);
        $ads = Advertisement::select('*')->paginate(10);
        $users = User::select('*')->paginate(10);
        $adsmoney = Advertisement::sum('price');
        $tags = Tag::select('name')->get();


        return view('admin.index', compact('likes', 'projects', 'contests', 'challenges', 'comments', 'users', 'ads', 'adsmoney', 'tags'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
