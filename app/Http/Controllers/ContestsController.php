<?php

namespace App\Http\Controllers;

use App\Contest;
use Flash;
use App\Project;
use App\Vote;
use App\User;
use App\Tag;
use Auth;
use Request;
use Input;
use File;
use Validator;
use Redirect;
use Session;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Image;
use DB;
use League\ColorExtractor\Client as ColorExtractor;

class ContestsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        // $deadline = DB::table('contests')
        //     ->select('*')
        //     ->whereDate('created_at', '<=', Carbon::now()->toDateString())
        //     ->orderBy('enddate', 'desc')
        //     ->paginate(12);

        $lastid = DB::table('contests')->max('id');
            
        $submissionsnow = DB::table('projects')
            ->join('users', 'users.id', '=', 'projects.user_id')
            ->join('contests', 'contests.id', '=', 'projects.competition_id')
            ->where('competition', '=', true)
            ->where('competition_id', '=', $lastid)   
            ->select('users.name','projects.*')
            ->orderBy('countlikes', 'desc')
            ->simplePaginate(12);

            // return $submissionsnow;   

        $submissionsprevious = DB::table('projects')
            ->join('users', 'users.id', '=', 'projects.user_id')
            ->where('competition', '=', true)
            ->select('users.name','projects.*')
            ->orderBy('countlikes', 'desc')
            ->simplePaginate(10);

        $today = Carbon::now();

        $contests = DB::table('contests')
            ->select('*')
            ->whereDate('created_at', '<=', Carbon::now()->toDateString())
            ->where('id', '<>', $lastid )
            ->orderBy('enddate', 'desc')
            ->simplePaginate(5);


        $lastcontest = DB::table('contests')
            ->select('id', 'title', 'desc', 'enddate')
            ->whereDate('created_at', '<=', Carbon::now()->toDateString())
            ->where('id', '=', $lastid )
            ->get();

        return view('pages.contests', compact('contests', 'lastcontest', 'submissionsnow', 'submissionsprevious'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function create()
    {
        return view('admin/createContest');
    }

    public function newcontest()
    {
        //$this->validate($request, ['title'=>'required|min:3']);
        $input = Request::all();
        $lastcontest = DB::table('contests')->max('id');
        $newid = $lastcontest+1;

        $contest = new Contest;
        $contest->fill(\Input::all());
        $contest->title = $input['title'];
        $contest->desc = $input['desc'];
        $contest->startdate = date("Y-m-d h:m:s", strtotime($input['startdate']));
        $contest->enddate = date("Y-m-d h:m:s", strtotime("+30 days"));
        
        $contest->save();
        Flash::success('You created a new contest');
        return redirect('/admin');
    }


    private function syncTags(Project $project, array $tags) {
        $project->tags()->sync($tags);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store()
    {
        $contest_id = DB::table('contests')->max('id');
        $input = Request::all();
        $lastproject = DB::table('projects')->max('id');
        $newid = $lastproject+1;
        
        $image = Input::file('image');
        $imageN = $image->getClientOriginalName();
        $filename  = $newid. '.' . $image->getClientOriginalExtension();
        $path = public_path('uploads/projects/' . $filename);
        Image::make($image->getRealPath())->fit(800, 800)->save($path);

        $contests = Auth::user()->projects()->create($input);
        $contests->user_id = Auth::id();
        $contests->title = $input['title'];
        if (isset($input['tag_list'])) {
           $this->syncTags($contests, $input['tag_list']);
        }
        $contests->desc = $input['desc'];
        $contests->image = $filename;
        $contests->competition = 1;
        $contests->competition_id = $contest_id;

        $contests->save();
        Flash::success('Your project is added to the contest! Good luck!');
        return redirect('contests');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $project = Project::findOrFail($id)->load("User");
        $input = Request::all();
        //-----------------------COLORS--------------------------//

        $client = new ColorExtractor;
        $info = pathinfo('uploads/projects/'.$project->image);
        if ($info["extension"] == "jpg") {

            $image = $client->loadJpeg('uploads/projects/'.$project->image);
        }else if($info["extension"] == "png")
        {
            $image = $client->loadPng('uploads/projects/'.$project->image);
        }else if($info["extension"] == "gif") {
            $image = $client->loadGif('uploads/projects/'.$project->image);
        }else{
            echo 'Wrong image extension';
        }
        $palette = $image->extract(3);

        $palette = str_replace("#", "", $palette);

        $project->colors = implode(', ', $palette);
        $project->save();
        //-----------------------COLORS--------------------------//


        //-----------------------DB-----------------------------//
        $project_comments = DB::table('comments')
            ->select('body', 'name', 'image', 'comments.created_at')
            ->where('project_id', '=', $id)
            ->join('users', 'users.id', '=', 'user_id')
            ->orderBy('created_at', 'desc')
            ->simplePaginate(10);

        $votes = DB::table('votes')
            ->where('votes.projectId', '=' , $id)
            ->join('users', 'users.id', '=', 'votes.userId')
            ->select('users.name','votes.*')
            ->get(); 
        $topVotes = DB::table('votes')
            ->where('votes.projectId', '=' , $id)
            ->join('users', 'users.id', '=', 'votes.userId')
            ->select('users.name','votes.*')
            ->limit(3)
            ->get();         
        $project = Project::findOrFail($id);
        $designer = User::findOrFail($project->user_id);
        $firstWork = array(); 
        $designershort = strtolower(str_replace(' ', '-',  $designer->name));
        $firstvote = Vote::where('projectId', '=' , $id)->first();
        $myvote = DB::table('votes')
            ->where('projectId', '=' , $id)
            ->where('userId', '=', Auth::id())
            ->first();
        if ($myvote != null) {
            $myvote = true;
        } else {
            $myvote = false;
        }
        $countVotes = Vote::where('projectId', '=' , $id)->count();
        $contest = true;

        $follower = DB::table('followers')
            ->where('fan_id', '=' , Auth::id())
            ->where('designer_Id', '=', $designer->id)
            ->first();
        if ($follower != null) {
            $follower = true;
        } else {
            $follower = false;
        }

        $today = date("m-d-Y");

        $advertisements = DB::table('ads')
            ->where('enddate', '>', $today)
            ->take(3)
            ->get();

        return view('pages.showProject', ['follower'=>$follower, 'advertisements'=>$advertisements, 'contest'=>$contest, 'project' => $project, 'comments' => $project_comments, 'palette' => $palette, 'designer' => $designer, 'countVotes' => $countVotes, 'votes' => $votes, 'topVotes' => $topVotes, 'designershort' => $designershort, 'myvote' =>$myvote, 'firstWork' => $firstWork]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
