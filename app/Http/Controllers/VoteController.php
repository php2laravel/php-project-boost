<?php

namespace App\Http\Controllers;

use App\Project;
use App\Vote;
use Auth;
use Request;
use Redirect;
use Input;
use DB;
use Flash;
use Session;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Marketing;

class VoteController extends Controller
{
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id){

        if(Request::ajax()) {
            $data = Input::all();
            $vote = new Vote;
            $vote->fill(\Input::all());
            $vote->save();
            die;
        }

        $input = Request::all();
        $Marketing = new Marketing();

        $projectOwnerID = DB::table('users')
            ->select('users.id')
            ->where('projects.id', '=', $input['projectId'])
            ->join('projects', 'users.id', '=', 'projects.user_id')
            ->value('users.id');

        $mails = DB::table('users')
            ->select('emails')
            ->where('id', '=', $projectOwnerID)
            ->value('emails');

        $projectTitle = DB::table('projects')
            ->select('title')
            ->where('id', '=', $input['projectId'])
            ->value('title');

        $projectOwner = DB::table('users')
            ->select('name')
            ->where('projects.id', '=', $input['projectId'])
            ->join('projects', 'users.id', '=', 'projects.user_id')
            ->value('name');

        $ownerEmail = DB::table('users')
            ->select('email')
            ->where('projects.id', '=', $input['projectId'])
            ->join('projects', 'users.id', '=', 'projects.user_id')
            ->value('email');

        $likes = DB::table('votes')
            ->where('projectId', '=', $input['projectId'])
            ->count();


        $today = date("m-d-Y");
        $advertisements = DB::table('ads')
            ->where('enddate', '>', $today)
            ->take(3)
            ->get();

        $ad_array = ['Add your advertisement here','Add your advertisement here','Add your advertisement here'];
        $ad_link_array = ['http://boost.weareimd.be/advertise','http://boost.weareimd.be/advertise','http://boost.weareimd.be/advertise'];
        $teller = 0;

        foreach($advertisements as $ad)
        {
            $ad_array[$teller] = $ad->companyname;
            $ad_link_array[$teller] = $ad->website;
            $teller++;
        }

        $adone = $ad_array[0];
        $adtwo = $ad_array[1];
        $adthree = $ad_array[2];

        $adonelink = $ad_link_array[0];
        $adtwolink = $ad_link_array[1];
        $adthreelink = $ad_link_array[2];

        $vote = new Vote;
        $vote->fill(\Input::all());
        print_r($vote);
        $vote->save();

        DB::table('projects')->where('projects.id', '=', $input['projectId'])->increment('countlikes');

        Flash::success('vote saved!');

        if($mails == "1")
        {
            if($likes == "9")
            {
                $Marketing->likeEmail($projectTitle, $projectOwner, $ownerEmail, $adone, $adtwo, $adthree, $adonelink, $adtwolink, $adthreelink);
            }
        }

        $user = Auth::user();
        $user->update_xp($user->id, config('app.experience.like'));

        return Redirect::back();

    }

    public function delete($id){

        if(Request::ajax()) {
            $input = Request::all();
            $userId = $input['userId']; 
            $projectId = $input['projectId']; 
            $vote = DB::table('votes')
            ->where('projectId', '=', $projectId)
            ->where('userId', '=', $userId)
            ->delete();
            die;
        }
      $input = Request::all();
      $userId = $input['userId']; 
      $projectId = $input['projectId']; 
      $vote = DB::table('votes')
            ->where('projectId', '=', $projectId)
            ->where('userId', '=', $userId)
            ->delete();
      Flash::message('vote undone');

      DB::table('projects')->where('projects.id', '=', $input['projectId'])->decrement('countlikes');

        //remove experience if like is removed
      $user = Auth::user();
      $user->remove_xp($user->id, config('app.experience.like'));
      return Redirect::back();
    }
}
