<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use MandrillMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use DB;
use Input;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    protected $redirectPath = '/';





    public function postRegister(Request $request)
    {
        $this->validate($request, [ 'name'=>'required|min:5|unique:users,name',
                                    'email'=>array('required', 'unique:users,email','email','regex:/(.*)student\.thomasmore\.be$/i'),
                                    'refer'=>array('unique:users,email','email','regex:/(.*)student\.thomasmore\.be$/i'),
                                    'password'=>'required|min:6|confirmed',
                                    'password_confirmation'=>'required|min:6']);
        $confirmation_code = str_random(30);
        $request['confirmation_code'] = $confirmation_code;
        $confirmation_link = "http://boost.weareimd.be/register/verify/" . $confirmation_code;
        $new_user = $this->create($request->all());
        $new_user->confirmed = true;
        $new_user->update();
        Auth::login($new_user);
        // Advertisements in mailing footer
        $today = date("m-d-Y");

        $advertisements = DB::table('ads')
            ->where('enddate', '>', $today)
            ->take(3)
            ->get();

        $ad_array = ['Add your advertisement here','Add your advertisement here','Add your advertisement here'];
        $ad_link_array = ['http://boost.weareimd.be/advertise','http://boost.weareimd.be/advertise','http://boost.weareimd.be/advertise'];
        $teller = 0;

        foreach($advertisements as $ad)
        {
            $ad_array[$teller] = $ad->companyname;
            $ad_link_array[$teller] = $ad->website;
            $teller++;
        }

        $adone = $ad_array[0];
        $adtwo = $ad_array[1];
        $adthree = $ad_array[2];

        $adonelink = $ad_link_array[0];
        $adtwolink = $ad_link_array[1];
        $adthreelink = $ad_link_array[2];
        $template_content = [];
        $message = array(
            'subject' => 'Welcome to Boost!',
            'from_email' => 'boostman@boost.weareimd.be',
            'from_name' => 'Boostman',
            'to' => array(
                array(
                    'email' => $request->input('email'),
                    'name' => $request->input('name'),
                    'type' => 'to'
                )
            ),
            'merge_vars' => array(
                array(
                    'rcpt' => $request->input('email'),
                    'vars' => array(
                        array(
                            'name' => 'NAME',
                            'content' => $request->input('name')
                        ),
                        array(
                            'name' => 'SPONSERONE',
                            'content' => $adone
                        ),
                        array(
                            'name' => 'SPONSERTWO',
                            'content' => $adtwo
                        ),
                        array(
                            'name' => 'SPONSERTHREE',
                            'content' => $adthree
                        ),
                        array(
                            'name' => 'SPONSORLINKONE',
                            'content' => $adonelink
                        ),
                        array(
                            'name' => 'SPONSORLINKTWO',
                            'content' => $adtwolink
                        ),
                        array(
                            'name' => 'SPONSORLINKTHREE',
                            'content' => $adthreelink
                        ),
                        array(
                            'name' => 'EMAIL',
                            'content' => $request->input('email')
                        ),
                        array(
                            'name' => 'CONFIRMATION',
                            'content' => $confirmation_link
                        )
                    )
                )
            )
        );

        $messageRef = array(
            'subject' => 'Boost - Refer a Friend',
            'from_email' => 'noreply@boost.com',
            'from_name' => 'Boostman',
            'to' => array(
                array(
                    'email' => $request->input('refer'),
                    'name' => 'Sir',
                    'type' => 'to'
                )
            ),
            'merge_vars' => array(
                array(
                    'rcpt' => $request->input('refer'),
                    'vars' => array(
                        array(
                            'name' => 'NAME',
                            'content' => $request->input('name')
                        ),
                        array(
                            'name' => 'EMAILREF',
                            'content' => $request->input('refer')
                        ),
                        array(
                            'name' => 'EMAIL',
                            'content' => $request->input('email')
                        )
                    )
                )
            )
        );

        $email_refer = $new_user->refer;
        $referBadge = DB::table('users')->select('id')->where('refer' ,'=' , $email_refer )->get();

        if($request->input('refer') != '')
        {
            MandrillMail::messages()->sendTemplate('BOOST - New Account', $template_content, $message);
            MandrillMail::messages()->sendTemplate('BOOST - Refer', $template_content, $messageRef);
            $new_user->update_xp($new_user->id, config('app.experience.refer'));
        }else{
            MandrillMail::messages()->sendTemplate('BOOST - New Account', $template_content, $message);
        }

        Session::flash('success', 'You have succesfully registered and you are automatically logged in.');

        if (User::all()->count() < config('app.badges.founding_member'))
        {
            $new_user->add_badge($new_user->id, config('app.badges.founding_member_name'));
        }

        /*if($referBadge != null)
        {
            $new_user->add_badge($new_user->id, config('app.badges.referance_badge_name'));
        }*/

        return redirect('/');
    }



    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        $userCheck = DB::table('users')
            ->where('email', '=', Input::get('refer'));

        if($userCheck  != null)
        {
            return User::create([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'refer' => $data['refer'],
                'experience' => 0,
                'confirmation_code' => $data['confirmation_code'],
            ]);
        }else{
            //user already has account
        }
    }
}
