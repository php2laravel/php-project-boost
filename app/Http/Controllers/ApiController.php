<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Project;
use Response;

class ApiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function popular(Request $request) 
    {

        $page = $request->get('page');
        $perpage = $request->get('perpage');
        $resp = [];
        
        if(isset($page) && isset($perpage)) {
            $projects = Project::paginate($perpage);

            foreach ($projects as $project) {
                $data = [
                    'id' => $project->id,
                    'title' => $project->title,
                    'url' => url() . '/projects/' . $project->id,
                    'image_url' => url() . '/uploads/' .$project->image,
                    'author_name' => $project->User->name,
                    'author_name_image_url' => url() . '/uploads/users/' .$project->User->image,
                    'comments' => $project->comments->implode('body', ', '),
                    'likes' => $project->likes->count(),
                ];

                array_push($resp, $data);
            }
        }


        return Response::json($resp, 200, ['Content-Type: application/json'], JSON_PRETTY_PRINT);
        // https 200 staat voor succesvol

    }

    public function project($id) 
    {
        $project = Project::find($id);

        $data = [
            'id' => $project->id,
            'title' => $project->title,
            'url' => url() . '/projects/' . $project->id,
            'image_url' => url() . '/uploads/' .$project->image,
            'author_name' => $project->User->name,
            'author_name_image_url' => url() . '/uploads/users/' .$project->User->image,
            'comments' => $project->comments->implode('body', ', '),
            'likes' => $project->likes->count(),
        ];

        return Response::json($data, 200, ['Content-Type: application/json'], JSON_PRETTY_PRINT);
    }    
}
