<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Input;
use App\User;
use File;
use Validator;
use Redirect;
use Session;
use Image;
use DB;
use Flash;
use Auth;
use App\Advertisement;
use App\HttpRequests;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Http\FormRequest;
use Response;
use DateTime;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Contracts\Billable as BillableContract;
use App\Marketing;


class AdsController extends Controller 
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $today = date("m-d-Y");
        $ads = DB::table('ads')
                ->where('enddate', '>', $today)
                ->count();

       if($ads >= 3)
       {
           $countads = 3;
       }
       else{
        $countads = 0;
       }

        if(Auth::user()){
            
            $userid=Auth::user()->id;

            DB::table('ads')
            ->where('user_id', "=", $userid)
            ->increment('views');

            $advertisements = DB::table('ads')
                ->join('users', 'users.id', '=', 'ads.user_id')
                ->select('users.id','ads.*')
                ->where('users.id', '=', Auth::user()->id)
                ->where('ads.enddate', '>', $today)
                ->get();

        }

        return view('pages.advertise', compact('advertisements', 'countads'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [ 'companyname'=>'required|min:5',
            'website'=>'required',
            'startdate'=>'required']);
        $Marketing = new Marketing();
        //
        $input = Request::all();

        $user_id = Auth::user()->id;
        
        $today = date("Y-m-d h:m:s");
        $startdate = date("Y-m-d h:m:s", strtotime($input['startdate']));
        $enddate = date("Y-m-d h:m:s", strtotime("+30 days"));
        $price = 150;

        // Advertisements in mailing footer
        $todaymail = date("m-d-Y");

        $advertisements = DB::table('ads')
            ->where('enddate', '>', $todaymail)
            ->take(3)
            ->get();

        $ad_array = ['Add your advertisement here','Add your advertisement here','Add your advertisement here'];
        $ad_link_array = ['http://boost.weareimd.be/advertise','http://boost.weareimd.be/advertise','http://boost.weareimd.be/advertise'];
        $teller = 0;

        foreach($advertisements as $ad)
        {
            $ad_array[$teller] = $ad->companyname;
            $ad_link_array[$teller] = $ad->website;
            $teller++;
        }

        $adone = $ad_array[0];
        $adtwo = $ad_array[1];
        $adthree = $ad_array[2];

        $adonelink = $ad_link_array[0];
        $adtwolink = $ad_link_array[1];
        $adthreelink = $ad_link_array[2];

        $adCompany = $input['companyname'];
        $adWebsite = $input['website'];

        $adEmail = Auth::user()->email;
        // Set your secret key: remember to change this to your live secret key in production
        // See your keys here https://dashboard.stripe.com/account/apikeys
        \Stripe\Stripe::setApiKey("sk_test_EPDklRsxVouu59CPhUUzbJiT");

        // Get the credit card details submitted by the form
        $token = $input['stripeToken'];

        // Create the charge on Stripe's servers - this will charge the user's card
        try {
          $charge = \Stripe\Charge::create(array(
            "amount" => 15000, // amount in cents, again
            "currency" => "eur",
            "source" => $token,
            "description" => "Advertisement charge"
            ));

          if($startdate >= $today)
            {
                $input = Request::all();
                $lastadd = DB::table('ads')->max('id');
                $newid = $lastadd+1;
        
                $image = Input::file('image');
                $imageN = $image->getClientOriginalName();
                $filename  = $newid. '.' . $image->getClientOriginalExtension();
                $path = public_path('uploads/a/' . $filename);

                Image::make($image->getRealPath())->fit(600, 250)->save($path);

                $ad= new Advertisement;
                
                $ad->user_id = Auth::id();
                $ad->companyname = $input['companyname'];
                $ad->website = $input['website'];
                $ad->image = $filename;
                $ad->startdate = $startdate;
                $ad->enddate = $enddate;
                $ad->price = $price;
                $ad->save();

                $Marketing->adMail($adCompany, $adWebsite, $startdate, $enddate, $price, $adEmail, $adone, $adtwo, $adthree, $adonelink, $adtwolink, $adthreelink);

                Flash::success('Your advertisment is added! You can find the stats at the bottom of this page');
            }
        } catch(\Stripe\Error\Card $e) {
          // The card has been declined

            Flash::error('Payment declined');
        }

        
        return redirect('advertise');
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function redirect($id)
    {
        //
        DB::table('ads')
            ->select("*")
            ->where("id", $id)
            ->increment('clicks');

        $ad = Advertisement::find($id);
        $link = "http://".$ad->website;

        return redirect($link);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
