<?php

namespace App\Http\Controllers;

use App\Project;
use App\User;
use DB;
use Request;
use App\Http\Requests;
use Flash;
use App\Http\Controllers\Controller;

class QueryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $query = Request::input('search');
        
        $projects = DB::table('projects')
                    ->where('title', 'LIKE', '%' . $query . '%')
                    ->paginate(10);

        $desc = DB::table('projects')
                ->where('desc', 'LIKE', '%' . $query . '%')
                ->where('title', 'NOT LIKE', '%' . $query . '%')
                ->paginate(10);

        $designers = DB::table('users')
                    ->where('name', 'LIKE', '%' . $query . '%')
                    ->paginate(10);

        $tag = DB::table('tags')
             ->where('name', 'LIKE', '%' . $query . '%')
             ->paginate(10);

        $palette = DB::table('projects')
                 ->where('colors', 'LIKE' , '%' . $query . '%')
                 ->value('colors');
        $palette = explode(", ", $palette);

        $colors = DB::table('projects')
                ->where('colors', 'LIKE' , '%' . $query . '%')
                ->paginate(10);

        $tags = Project::whereHas('tags', function ($q) {
                    $q->where('name', 'like', '%' .Request::input('search'). '%');  
            })->get(); 

        $options = true;
        
    return view('pages.search', compact('desc', 'tags', 'tag', 'projects', 'query', 'colors', 'palette', 'designers', 'options'));    
}

    public function searchtitle(Request $request)
    {
        $query = Request::input('search');
        $projects = DB::table('projects')->where('title', 'LIKE', '%' . $query . '%')->paginate(10);
        return view('pages.search', compact('projects', 'query'));
    }

    public function searchdescription(Request $request)
    {
        $query = Request::input('search');
        $desc = DB::table('projects')->where('desc', 'LIKE', '%' . $query . '%')->where('title', 'NOT LIKE', '%' . $query . '%')->paginate(10);
        return compact('desc', 'query');
    }

    public function colour(Request $request)
    {
        $query = Request::input('search');
        $colors = DB::table('projects')
                ->where('colors', 'LIKE', '%' . $query . '%')
                ->paginate(10);
        //$colors = unserialize($colors);

        $palette = DB::table('projects')
                 ->where('colors', 'LIKE' , '%' . $query . '%')
                 ->value('colors');
        $palette = explode(", ", $palette);

        $query = "#".Request::input('search');

        return view('pages.search', compact('colors', 'query', 'palette'));
    }

    public function tag(Request $request)
    {

        $query = Request::input('search');
        $tag = DB::table('tags')->where('name', 'LIKE', '%' . $query . '%')->paginate(10);
        $tags = Project::whereHas('tags', function ($q) {
                    $q->where('name', 'like', '%' .Request::input('search'). '%');  
            })->get(); 
        return view('pages.search', compact('tags', 'tag', 'query'));
    }

    public function searchdesigners(Request $request)
    {
        $query = Request::input('search');
        $designers = DB::table('users')->where('name', 'LIKE', '%' . $query . '%')->paginate(10);
        return compact('query', 'designers');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
