<?php

namespace App\Http\Controllers;

use Flash;
use App\Project;
use App\Vote;
use App\User;
use App\Tag;
use App\Contest;
use Auth;
use Request;
use Input;
use App\Comment;
use App\Advertisement;
use File;
use Validator;
use Redirect;
use Session;
use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Image;
use DB;


class TagController extends Controller
{
   
    public function create()
    {
        $taglist = DB::table('tags')->orderBy('name', 'ASC')->select('name')->get();
        return view('admin/createTag', compact('taglist'));
    }

    
    public function store(Request $request)
    {
        $input = Request::all();
        $lasttag = DB::table('tags')->max('id');
        $newid = $lasttag+1;
        $new = $input['name'];

        $tag = new Tag;
        $tag->name = $new;
        $tag->save();
        
        Flash::success('You added a new tag "'.$new.'" to the list');
        return redirect('/admin'); 
    }

    
    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }

    
    public function destroy($id)
    {
        //
    }
}
