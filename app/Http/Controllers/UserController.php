<?php

namespace App\Http\Controllers;

use Hash;
use Mockery\Exception;
use View;
use Input;
use Auth;
use App\User;
use Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use File;
use Image;
use Flash;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View::make('users.create-account', ['title' => '| new user']);
    }

    /**
     * Confirm the email address
     *
     */
    public function confirm($confirmationCode)
    {
        $user = User::whereconfirmation_code($confirmationCode)->first();

        if ($user->confirmed)
        {
            Session::flash('error', 'Your account is already confirmed');
        }
        else
        {
            $user->confirmed = true;
            $user->update();
            Auth::login($user);
            Session::flash('success', 'Your account is confirmed and you are automatically logged in.');
        }
        return redirect('/');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = Auth::user();
        $likes = DB::table('projects')
            ->join('votes', 'projectId', '=', 'projects.id')
            ->where('votes.userId', '=', $user->id)
            ->join('users', 'users.id', '=', 'projects.user_id')
            ->select('projects.*', 'users.name')
            ->orderBy('votes.created_at', 'desc')
            ->get();

        $totallikes = DB::table('projects')
        // alle countlikes van alle projecten van deze user
            ->join('users', 'projects.user_id', '=', 'users.id')
            ->where('user_id', '=', $user->id)
            ->sum('countlikes');

        $badges = DB::table('badges_users')
            ->join('badges', 'badges_users.badge_id', '=', 'badges.id')
            ->select('badges.*')
            ->where('badges_users.user_id', '=', $user->id)
            ->orderBy('created_at', 'desc')
            ->get();

        $totalBadges = DB::table('badges')
            ->select('*')
            ->get();

        $follows = DB::table('followers')
            ->where('fan_id', '=', $user->id)
            ->join('users', 'users.id', '=', 'designer_id')
            ->select('users.image', 'users.name', 'users.functie')
            ->get();

        $followers = DB::table('followers')
            ->where('designer_id', '=', $user->id)
            ->join('users', 'users.id', '=', 'fan_id')
            ->select('users.image', 'users.name', 'users.functie')
            ->get();

        DB::table('users')
            ->where('id', '=', $user->id)
            ->increment('views');

        $projects = DB::table('projects')
            ->select('projects.id')
            ->join('users', 'projects.user_id', '=', 'users.id')
            ->where('users.id', '=', $user->id)
            ->lists('id');


        $activityComments = DB::table('comments')
            ->select('*')
            ->join('users', 'comments.user_id', '=', 'users.id')
            ->join('projects', 'comments.project_id', '=', 'projects.id')
            ->where('comments.user_id', '=',  $user->id)
            ->orderBy('comments.created_at', 'desc')
            ->take(40)
            ->simplePaginate(5);

        //comments & likes you made
        $activityLikes = DB::table('votes')
            ->select('*')
            ->join('users', 'votes.userId', '=', 'users.id')
            ->join('projects', 'votes.projectId', '=', 'projects.id')
            ->where('userId', '=',  $user->id)
            ->orderBy('votes.created_at', 'desc')
            ->take(40)
            ->simplePaginate(5);

        //comments & likes on your work
        return view('user.profile',  compact('likes', 'totallikes', 'follows', 'followers', 'badges','totalBadges',  'activityComments', 'activityLikes'));
    }

    public function notifications()
    {
        $user = Auth::user();

        $projects = DB::table('projects')
            ->select('projects.id')
            ->join('users', 'projects.user_id', '=', 'users.id')
            ->where('users.id', '=', $user->id)
            ->lists('id');

        //comments & likes on your work
        $notificationComments = DB::table('comments')
            ->select('*')
            ->join('users', 'comments.user_id', '=', 'users.id')
            ->join('projects', 'comments.project_id', '=', 'projects.id')
            ->whereIn('project_id', $projects)
            ->orderBy('comments.created_at', 'desc')
            ->simplePaginate(5);

        $notificationLikes = DB::table('votes')
            ->select('*')
            ->join('users', 'votes.userId', '=', 'users.id')
            ->join('projects', 'votes.projectId', '=', 'projects.id')
            ->whereIn('projectId', $projects)
            ->orderBy('votes.created_at', 'desc')
            ->simplePaginate(5);

        return view('pages.notifications',  compact('notificationComments', 'notificationLikes'));
    }

    public function edit()
    {
        return view('user.edit');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    /*public function update(Request $request, $id)
    {
        //
    }*/

    public function update($id)
    {
        //find user
        $user = User::find($id);
        //auto prepopulate form
        $user->fill(\Input::all()); 
        //upload and move new profile picture
        $input = Request::all();
        if (Input::hasFile('image'))
        {
            $image = Input::file('image');
            $imageN = $image->getClientOriginalName();
            $filename  = $id. '.' . $image->getClientOriginalExtension();
            $path = public_path('uploads/users/' . $filename);
            $paththumbnail = public_path('uploads/users/thumbnails/' . $filename);
            Image::make($image->getRealPath())->fit(250, 250)->save($path);
            $user->image = $filename;
            Image::make($image->getRealPath())->fit(100, 100)->save($paththumbnail);;
        }
        //save updates and redirect to my-profile
        $user->save();// no validation implemented
        Flash::success('Your profile info is updated!');
        return redirect('my-profile');
    }


    public function activity()
    {
        $userid = Auth::user()->id;
        $likes = DB::table('votes')
            ->join('users', 'userId', '=', 'users.id')
            ->where('users.id', '=', $userid)
            ->join('projects', 'projectId', '=', 'projects.id')
            ->select('votes.*', 'users.name', 'projects.title')
            ->get();

        $comments = DB::table('comments')
            ->join('users', 'user_id', '=', 'users.id')
            ->where('users.id', '=', $userid)
            ->join('projects', 'project_id', '=', 'projects.id')
            ->select('comments.*', 'users.name', 'projects.title')
            ->get();

        return view('user.activity', ['likes' => $likes, 'comments' => $comments]);
    }


    //get badge if you post a comment within the hour after your first login
    public function commentBadge($id)
    {
        $userid = Auth::user()->id;
        //selecteer eerste comment
        $eerstecomment = DB::table('comments')
            ->where('user_id', '=', $userid)
            ->order_by('created_at', 'asc')
            ->firstorfail();
        
        $date1 = user()->created_at; //first login
        $date2 = $eerstecomment->created_at; //first comment

        $diff = abs(strtotime($date2) - strtotime($date1));
        $minutes = floor($diff / (60*60));
        $maxminutes = 60; // 1 uur = deadline badge

        if ($minutes <= $maxminutes )
        {
            // $user->commentBadge = true;
            //$user->save();// no validation implemented     
        }
        return redirect('my-profile');
    }

    public function delete($id)
    {
        //get user
        $user = User::find($id);
        $username = $user->name;
        //get profile picture and delete from uploads folder
        $filename = $user->image;
        //delete project
        $user->delete();
        File::delete('uploads/users/'.$filename);
        File::delete('uploads/users/thumbnails/'.$filename);
        //back to gallery
        Flash::message($username.' is deleted from Boost');
        return redirect('/admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}