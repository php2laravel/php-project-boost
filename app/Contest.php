<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contest extends Model
{
    protected $fillable = [
       'title',
       'desc',
       'startdate', 
       'enddate',
       'image',
    ];

}
