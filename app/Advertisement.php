<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;
use Laravel\Cashier\Contracts\Billable as BillableContract;

class Advertisement extends Model implements BillableContract
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ads';

    use Billable;

    protected $dates = ['trial_ends_at', 'subscription_ends_at'];
}
