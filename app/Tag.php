<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
	/**
	* Fillable fields for every new tag
	*/
	protected $fillable = [
		'name'
	];
	/**
	* Get the articles associated with the given tag
	*/
    public function projects()
    {
    	return $this->belongsToMany('App/Project');

    }
}
