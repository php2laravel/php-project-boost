<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\User;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        \App\Console\Commands\Inspire::class,
    ];

    protected function schedule(Schedule $schedule)
    {


        $schedule->command('SendNewsletterCommand')
            ->weekly()->sundays()->at('20:00');

        $schedule->command('SendNewsletterCommand')
            ->monthly();


    }
}
