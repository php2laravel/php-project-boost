<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Project;
use App\Auth;
use App\Marketing;
use App\User;

class referBadgeCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'badge:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check if user gets refer a student badge';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach(User::all() as $user)
        {
            $userid = Auth::user()->id;
            $useremail = Auth::user()->email;

            $checkUsers = DB::table('users')
                ->select('id')
                ->where(DB::raw('refer in (select ').$useremail.(' from users)'))
                ->firstorfail();

            if ($checkUsers != null)
            {
                $user->add_badge($userid, config('app.badges.referance_badge_name'));
            }
        }
    }
}
