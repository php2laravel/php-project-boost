<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Project;
use App\Auth;
use App\Marketing;
use App\User;

class SendNewsletterCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'newsletter:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send a newsletter with most popular projects every month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (User::all() as $user) {
            $name = $user->name;
            $email = $user->email;
            $acceptEmails = $user->emails;
            $mailTime = $user->mailsTime;

            // Advertisements in mailing footer
            $today = date("m-d-Y");

            $advertisements = DB::table('ads')
                ->where('enddate', '>', $today)
                ->take(3)
                ->get();

            $ad_array = ['Add your advertisement here','Add your advertisement here','Add your advertisement here'];
            $ad_link_array = ['http://boost.weareimd.be/advertise','http://boost.weareimd.be/advertise','http://boost.weareimd.be/advertise'];
            $teller = 0;

            foreach($advertisements as $ad)
            {
                $ad_array[$teller] = $ad->companyname;
                $ad_link_array[$teller] = $ad->website;
                $teller++;
            }

            $adone = $ad_array[0];
            $adtwo = $ad_array[1];
            $adthree = $ad_array[2];

            $adonelink = $ad_link_array[0];
            $adtwolink = $ad_link_array[1];
            $adthreelink = $ad_link_array[2];

            $topLikes = DB::table('projects')
                ->select('*')
                ->sort('countlikes', 'desc')
                ->take(5)
                ->get();

            $mostComments = DB::table('projects')
                ->select('*')
                ->sort('countcomments', 'desc')
                ->take(5)
                ->get();


            $newest = DB::table('projects')
                ->select('*')
                ->sort('created_at', 'desc')
                ->take(5)
                ->get();

            if ($acceptEmails == "1") {
                if ($mailTime == "1") {
                    $Marketing = new Marketing;
                    $Marketing->weeklyMail($name, $email, $topLikes, $mostComments, $newest, $adone, $adtwo, $adthree, $adonelink, $adtwolink, $adthreelink);
                } else {
                    $Marketing = new Marketing;
                    $Marketing->monthlyMail($name, $email, $topLikes, $mostComments, $newest, $adone, $adtwo, $adthree, $adonelink, $adtwolink, $adthreelink);
                }


            }
        }
    }
}
