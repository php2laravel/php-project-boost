<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
	protected $table = 'votes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'userId',
        'projectId'
    ];
    public function User()
	{
	    return $this->belongsTo('App\User');
	}
    public function Project()
    {
        return $this->belongsTo('App\Project', 'project_id');
    }

}
