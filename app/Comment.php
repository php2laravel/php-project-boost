<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    //comments table in database
    protected $guarded = [];
    protected $table = 'comments';

    // user who has commented
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    // returns post of any comment
    public function post()
    {
        return $this->belongsTo('App\Project','project_id');
    }
    
}
